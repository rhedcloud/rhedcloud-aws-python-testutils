.. RHEDcloud AWS Python Test Utilities documentation master file, created by
   sphinx-quickstart on Thu Dec  7 23:58:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=============================================================
Welcome to RHEDcloud AWS Python Test Utilities documentation!
=============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   appstream
   cleanup
   decorators
   directoryservice
   dynamodb
   ebs
   ec2
   efs
   elb
   glacier
   iot
   kms
   lib
   opsworks
   orgs
   pinpoint
   route53
   rds
   servicecatalog
   ses
   snowball
   ssm
   sts
   utils
   vpc
   workspaces

Custom Pytest Plugins
=====================

The following are plugins for pytest.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   plugin/accessdenied
   plugin/account_locking
   plugin/blocked_services
   plugin/org_shuffle
   plugin/script_runner


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
