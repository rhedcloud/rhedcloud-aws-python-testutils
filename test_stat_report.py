repos = [
    {'name':'rhedcloud-aws-rs-account-cfn', 'path':'/www/git/rhedcloud-aws-rs-account-cfn'},
    {'name':'rhedcloud-aws-vpc-type1-cfn', 'path':'/www/git/rhedcloud-aws-vpc-type1-cfn'}
    ]

import os
import json
import re
import subprocess
import datetime
import pprint

def count_source_lines_and_scan_for_testcount(repo,type,file_name):
    test_count = None
    p = None
    if type=='structural' or type=='simulation':
        test_count = 1
    elif type=='functional':
        p = re.compile('\\$\\{testcount:.*}')
    else:
        raise Exception('bad test type', type)
    
    fname = repo['path']+'/tests/'+type+'/'+file_name
    with open(fname) as f:
        for i, l in enumerate(f):
            if not(test_count):
                s = p.search(l)
                #print(l)
                if s:
                    try:
                        test_count  = int(s.group().split(':')[1].split('}')[0]) 
                    except ValueError:
                        test_count = '#NAN'
                          
    return {'lines':i + 1, 'tests':test_count}

def get_file_stats_from_git(repo,type,file_name):
    pretty = '--pretty=format:{"name":"%an","timestamp":"%at"}'
    git_dir = repo['path']+'/.git'
    work_tree = repo['path']
    file_path = repo['path']+"/tests/"+type+"/"+file_name
    c = subprocess.run(["git", '--git-dir='+git_dir, '--work-tree='+work_tree, "log", pretty, file_path], stdout=subprocess.PIPE)
    d = c.stdout.decode('utf-8') #d is a string
    e = d.replace('\n',',')
    history_list = json.loads('['+e+']')
    if len(history_list)==0:
        authors=set({"#filehistory_not_found"})
        commits = None
    else:
        authors = set()
        commits = {'first':None,'last':None}
        for event in history_list:
            authors.add(event['name'])
            if not(commits['first']) or event['timestamp']<commits['first']:
                commits['first']=event['timestamp']
            if not(commits['last']) or event['timestamp']>commits['last']:
                commits['last']=event['timestamp']
           
    counts=count_source_lines_and_scan_for_testcount(repo,type,file_name)

    return {
        'authors':list(authors),
        'type':type,
        'name':file_name,
        'count':counts['tests'],
        'linecount':counts['lines'],
        'commits':commits,
        'template':repo['name'],
        'sortkey':commits['last']
        }

def main():
    global repos
    
    file_stats_list = []
    for repo in repos:
        if 'ignore' in repo and repo['ignore']:
            print("Ignoring repo '",repo['name'],"'")
            continue
        
        print(repo['name']+': structural:')
        structural_list = os.listdir(repo['path']+"/tests/structural")
        for file_name in structural_list:
            if file_name.startswith('test_') and file_name.endswith('.py'):
                print(file_name)
                file_stats_list.append(get_file_stats_from_git(repo,'structural',file_name))
        
            
        print(repo['name']+': functional:')
        try:
            functional_list = os.listdir(repo['path']+"/tests/functional")
            for file_name in functional_list:
                if file_name.startswith('test_') and file_name.endswith('.py'):
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo,'functional',file_name))
        except FileNotFoundError as err:
            print('No functional tests in ',repo['name'])
    
        print(repo['name']+': simulation:')
        try:
            simulation_list = os.listdir(repo['path']+"/tests/simulation")
            for file_name in simulation_list:
                if file_name.startswith('test_') and file_name.endswith('.py'):
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo,'simulation',file_name))
        except FileNotFoundError as err:
            print('No simulation tests in ',repo['name'])
        
    
    #sort list of events
    file_stats_list = sorted(file_stats_list, key=lambda file_stat: file_stat['sortkey'])
        
    #create comma separated list and count lines
    total_lines = 0
    test_count = 0
    testers = {}
    "CSV-------------"
    csv_lines='"Repo","Type","Name","Count","Lines","First Commit","Last Commit","Author(s)","Running Total Tests","Running Total Lines","RT Lines / 100"\n'
    for fs in file_stats_list:
        csv_line = '"'+fs['template']+'","'+fs['type']+'","'+fs['name']+'","'
        if fs['count']:
            csv_line+=str(fs['count'])+'","'
        else:
            csv_line+='"null","'
            
        csv_line+=str(fs['linecount'])+'","'
        if fs['commits']:
            dtfirst = datetime.datetime.fromtimestamp(int(fs['commits']['first']),tz=None)
            dtlast = datetime.datetime.fromtimestamp(int(fs['commits']['last']))
            csv_line+=str(dtfirst)+'","'+str(dtlast)+'","'
        else:
            csv_line+='"","","'
        authors=''
        for author in fs['authors']:
            authors+=author+"," #TODO remove last ,
            if author in testers:
                testers[author]['testcount']+=fs['count']
                testers[author]['linecount']+=fs['linecount']
            else:
                testers[author]={'testcount':fs['count'],'linecount':fs['linecount']}

        csv_line+=authors+'",'            
        total_lines+=fs['linecount']
        test_count+=fs['count']
        csv_line+=str(test_count)+","
        csv_line+=str(total_lines)+","
        csv_line+=str(total_lines/100)+"\n"
        csv_lines+=csv_line
    "CSV-END-------------"
    # write csv file
    f = open('file_stats.csv','w')    
    f.write(csv_lines)
    # write json file
    file_stats_list_string = json.dumps(file_stats_list)
    f = open('file_stats.json','w')
    f.write(file_stats_list_string)

    print("Testers:")
    pprint.pprint(testers)
        
    print("Total tests: {:,.0f}".format(test_count))
    print ("Lines of code: {:,.0f}".format(total_lines))
        
if __name__ == "__main__":
    main()
