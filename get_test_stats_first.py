# drop table test_stats;
# CREATE TABLE test_stats (
#      id MEDIUMINT NOT NULL AUTO_INCREMENT,
#     repo char(200),
#     type char(100),
#     name char(200),
#     count integer NOT NULL,
#     lines_ integer,
#     authors char(200),
#     PRIMARY KEY (id)
# );

# drop table test_author_stats;
# CREATE TABLE test_author_stats (
#      id MEDIUMINT NOT NULL AUTO_INCREMENT,
#     author char(200),
#     tests integer,
#     lines_ integer,
#     PRIMARY KEY (id)
# );
#####this version will consider the first commit as the date of the completion and only consider the first author as the sole contributor
import os
pathDir=os.environ.get('IT_ARCH_HOME')
repoNames = ['rhedcloud-aws-rs-account-cfn', 'rhedcloud-aws-vpc-type1-cfn', 'rhedcloud-aws-org-standard-scp']
repos = [
    {'name': repoNames[0], 'path':pathDir+repoNames[0]},
    {'name':repoNames[1],  'path':pathDir+repoNames[1]},
    {'name': repoNames[2],  'path':pathDir+repoNames[2]}
    ]
print repos
# TODO make use and password args
# user = NONE
# password = NONE

import argparse
import pprint
import os
#import requests
import json
import re
import subprocess
import datetime
import mysql.connector


def count_source_lines_and_scan_for_testcount(repo, type, file_name):
    test_count = None
    p = None
    if type == 'structural' or type == 'simulation':
        test_count = 1
    elif type == 'functional':
        p = re.compile('\\$\\{testcount:.*}')
    else:
        raise Exception('bad test type', type)
    
    fname = repo['path'] + '/tests/' + type + '/' + file_name
    with open(fname) as f:
        for i, l in enumerate(f):
            if not(test_count):
                s = p.search(l)
                # print(l)
                if s:
                    try:
                        test_count = int(s.group().split(':')[1].split('}')[0]) 
                    except ValueError:
                        test_count = '#NAN'
                          
    return {'lines':i + 1, 'tests':test_count}
    

def get_file_stats(repo, type, file_name):
    global user, password
    r = requests.get('https://api.bitbucket.org/1.0/repositories/itarch/' + repo['name'] + '/filehistory/' + repo['node'] + '/tests/' + type + '/' + file_name + '?page=0',
                      auth=(user, password))
    if r.status_code != 200:
        return None
    
    history_list = r.json()
    # sometimes the filehistory is not found
    if len(history_list) == 0:
        authors = set()
        commits = {'first':'John', 'last':'Doe'}
        commits['first'] = '2040-01-01';
        commits['last'] = '2040-01-01';
    else:
        authors = set()
        commits = {'first':None, 'last':None}
        for event in history_list:
            authors.add(event['raw_author'])
            event_date = event
            if not(commits['first']) or event['utctimestamp'] < commits['first']:
                commits['first'] =    datetime.datetime.fromtimestamp(       event['utctimestamp']    ).strftime('%Y-%m-%d %H:%M:%S') 
            if not(commits['last']) or event['utctimestamp'] > commits['last']:
                commits['last'] =          datetime.datetime.fromtimestamp(       event['utctimestamp']    ).strftime('%Y-%m-%d %H:%M:%S') 
    counts = count_source_lines_and_scan_for_testcount(repo, type, file_name)
    
    return {
        'authors':list(authors),
        'type':type,
        'name':file_name,
        'count':counts['tests'],
        'linecount':counts['lines'],
        'commits':commits,
        'template':repo['name']
        }
def get_file_stats_from_git(repo,type,file_name):
    pretty = '--pretty=format:{"name":"%an","timestamp":"%at"}'
    git_dir = repo['path']+'/.git'
    work_tree = repo['path']
    file_path = repo['path']+"/tests/"+type+"/"+file_name
    c = subprocess.run(["git", '--git-dir='+git_dir, '--work-tree='+work_tree, "log", pretty, file_path], stdout=subprocess.PIPE)
    d = c.stdout.decode('utf-8') #d is a string
    e = d.replace('\n',',')
    history_list = json.loads('['+e+']')
    if len(history_list) == 0:
        authors = set()
        commits = {'first':'John', 'last':'Doe'}
        commits['first'] = '2040-01-01';
        commits['last'] = '2040-01-01';
    else:
        authors = set()
        commits = {'first':None,'last':None}
        for event in history_list:
            authors.add(event['name'])
            if not(commits['first']) or event['timestamp']<commits['first']:
                commits['first']=event['timestamp']
            if not(commits['last']) or event['timestamp']>commits['last']:
                commits['last']=event['timestamp']
           
    counts=count_source_lines_and_scan_for_testcount(repo,type,file_name)

    return {
        'authors':list(authors),
        'type':type,
        'name':file_name,
        'count':counts['tests'],
        'linecount':counts['lines'],
        'commits':commits,
        'template':repo['name']
        }


def main():
        
#     parser = argparse.ArgumentParser()
#     parser.add_argument("user", help="The user name of a bitbucket user with access to the repos.")
#     parser.add_argument("password", help="The password of the user.")
#     args = parser.parse_args()

    global repos
#     user = args.user
#     password = args.password
    file_stats_list = []
    for repo in repos:
        if 'ignore' in repo and repo['ignore']:
            print("Ignoring repo '", repo['name'], "'")
            continue
        
        print(repo['name'] + ': structural:')
        if (os.path.exists(repo['path'] + "/tests/structural")):
            structural_list = os.listdir(repo['path'] + "/tests/structural")
            for file_name in structural_list:
                if file_name.startswith('test_') and file_name.endswith('.py'):
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo, 'structural', file_name))
              
        print(repo['name'] + ': functional:')
        if (os.path.exists(repo['path'] + "/tests/functional")):
            functional_list = os.listdir(repo['path'] + "/tests/functional")
            for file_name in functional_list:
                if file_name.startswith('test_') and file_name.endswith('.py'):
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo, 'functional', file_name))
    
        print(repo['name'] + ': simulation:')
        if (os.path.exists(repo['path'] + "/tests/simulation")):
            simulation_list = os.listdir(repo['path'] + "/tests/simulation")
            for file_name in simulation_list:
                if file_name.startswith('test_') and file_name.endswith('.py'):
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo, 'simulation', file_name))
        
    # pprint.pprint(json.dumps(file_stats_list))
    print(json.dumps(file_stats_list))
    
    db = mysql.connector.connect(host="localhost", user="root", passwd="CredentialRedactedAndRotated", database="test")
    cur = db.cursor()
    cur.execute("delete from test_stats");
    
    # create comma separated list
    total_lines = 0
    print("CSV-------------")
    csv_lines='"Repo","Type","Name","Count","Lines","First Commit","Last Commit","Author(s)"\n'
    print('"Repo","Type","Name","Count","Lines","First Commit","Last Commit","Author(s)"')
    for fs in file_stats_list:
        dbCount = 0
        csv_line = '"' + fs['template'] + '","' + fs['type'] + '","' + fs['name'] + '","'
        if fs['count']:
            csv_line += str(fs['count']) + '","'
            dbCount = fs['count']
        else:
            csv_line += '0","'
            
        csv_line += str(fs['linecount']) + '","'
        dbLineCount = fs['linecount']
        if fs['commits']:
            csv_line += fs['commits']['first'] + '","' + fs['commits']['last'] + '","'
        else:
            csv_line += '"","","'
        authors = ''
        for author in fs['authors']:
            authors += author + ","  # TODO remove last ,
        total_lines+=fs['linecount']
        csv_line+=authors+'"\n'
        csv_lines+=csv_line
        print(csv_line)
        cur.execute("insert into test_stats(repo, type, name, count, lines_, authors) values('" +fs['template'] +"','"+ fs['type'] + "','"+ fs['name']+"',"+ str(dbCount) + ","+str(fs['linecount'])+ ",'" + authors + "')");
    
    print("CSV-END-------------")
        # write csv file
    now = datetime.datetime.now()
    todayStr =  now.strftime("%Y-%m-%d")
    fileName='aws_cfn_test_code_stats-'+todayStr+'.csv';
    f = open(fileName,'w')    
    f.write(csv_lines)
    # write json file
    file_stats_list_string = json.dumps(file_stats_list)
    f = open('file_stats.json','w')
    f.write(file_stats_list_string)

    print("Total tests: {:,.0f}".format(len(file_stats_list)))
    print ("Lines of code: {:,.0f}".format(total_lines))
   # print ("======================================================================================")
    cur.execute("select authors,sum(lines_),sum(count) from test_stats group by authors")
    print ("Authors--------------------------------------------------  Line Counts------Test Counts")
   # print ("======================================================================================")
    for row in cur.fetchall() :
        print  ("{0:-<60}  {1:-<10}  {2}".format(row[0],row[1],row[2]))
    cur.execute("select sum(count), sum(lines_) from test_stats")
    totalTestCount=''
    totalLineCount=''
    for row in cur.fetchall() :
        print  ('Total-tests=' ,str(row[0]) , ', total lines=', str(row[1]))
        totalTestCount=row[0]
        totalLineCount=row[1]
    print ("After split:")
    
    cur.execute("select sum(count),sum(lines_),authors from test_stats group by authors")
    for row in cur.fetchall() :
        authorList = [x.strip() for x in row[2].split(',')]
        authorList = list( filter(None,authorList) )
        authorList=authorList[0:1]
        authorNumber = len (authorList)
        for a in authorList :
            testsByAuthor=row[0]/authorNumber
            linesByAuthor=row[1]/authorNumber
            cur.execute("insert into test_author_stats(author,tests,lines_) values('"+a+"',"+str(testsByAuthor)+","+str(linesByAuthor)+")")       
    AuthorLineCountProperties = '';
    AuthorTestCountProperties = '';
    AuthorLineTestCountCsv = 'Author, Line Count, Test Count\n';
    
    ##writing directly into csv file
    now = datetime.datetime.now()
    todayStr =   now.strftime("%Y-%m-%d")
    LineCountByWriterCsv =todayStr
    TestCountByWriterCsv =  todayStr
    SummaryCsv = todayStr
   # print ("======================================================================================")
    cur.execute("select author, sum(lines_), sum(tests) from test_author_stats group by author")
  
    print ("Authors--------------------------------------------------  Line Counts------Test Counts")
    #print ("======================================================================================")
    summaryCsv='Author, Line Count, Test Count'
    rowCount=0
    for row in cur.fetchall() :
        rowCount=rowCount+1;
        summaryCsv += '\n'+str(row[0])+','+str(row[1])+','+str(row[2])
        print  ("{0:-<60}  {1:-<10}  {2}".format(row[0],row[1],row[2]))
        AuthorLineCountProperties += str(rowCount) + '=' + str(row[1]) + '\n'
        AuthorTestCountProperties +=  str(rowCount)  +'='+str(row[2])+'\n'
        AuthorLineTestCountCsv += str(row[0]) + ',' + str(row[1]) +','+str(row[2])+ '\n'
        LineCountByWriterCsv += ',' +str(row[1])
        TestCountByWriterCsv += ',' +str(row[2])
    print ('\nLineCountByWriterCsv===')

    print (LineCountByWriterCsv)
    print ('\nTestCountByWriterCsv===')
    print (TestCountByWriterCsv)
    #TODO: print to csv files
    
    summaryProperties = '';
    summaryCsv += '\n\nSummary, Number'; 
    cur.execute("select sum(tests),  sum(lines_) from test_author_stats")
    for row in cur.fetchall() :
        summaryProperties +=  '1=600'
        summaryProperties +=  '\n2=1100'
        summaryProperties +=  '\n3='+str(totalTestCount)
        summaryProperties +=  '\n4=' + str(600-totalTestCount)
        summaryProperties +=  '\n5='+ str(1100-totalTestCount)
        summaryProperties +=   '\n6='+str(100*round(totalTestCount/600,2))
        summaryProperties +=  '\n7='+str(100*round(totalTestCount/1100,2))
        
        
        summaryCsv +=  '\nEstimated tests for initial research service rollout,600'
        summaryCsv +=  '\nTotal number of tests completed for broad coverage,1100'
        summaryCsv +=  '\nNumber of tests completed,'+str(totalTestCount)
        summaryCsv +=  '\nTests remaining for rollout,' + str(600-totalTestCount)
        summaryCsv +=  '\nTotal tests remaining,'+ str(1100-totalTestCount)
        summaryCsv +=   '\nTests rollout complete %,'+str(100*round(totalTestCount/600,2))
        summaryCsv +=  '\nTotal tests complete %,'+str(round(100*totalTestCount/1100,2))
        
        SummaryCsv += ',600,1100,'+str(totalTestCount)+','+ str(600-totalTestCount)+','+str(1100-totalTestCount)+','+str(100*round(totalTestCount/600,2))+','+str(100*round(totalTestCount/1100,2))
   
    print ('\nsummaryProperties===')  
    print (summaryProperties)  
    print ('\nsummaryCsv===')  
    print (summaryCsv)
    print ('\nSummaryCsv===')                    
    print (SummaryCsv)  
    ##print to files
    AwsTestStatDir='/opt/openeai/dev/feedfile/AwsTestStat/'
    #AwsTestStatDir='';
    f = open(AwsTestStatDir+'TestCountByWriter.csv','a')
    f.write(TestCountByWriterCsv+'\n')
    f.close();
    f = open(AwsTestStatDir+'LineCountByWriter.csv','a')
    f.write(LineCountByWriterCsv+'\n')
    f.close();    
    f = open(AwsTestStatDir+'Summary.csv','a')
    f.write(SummaryCsv+'\n')
    f.close();   
    
    f = open('summary.properties','w')
    f.write(summaryProperties)
    f.close();   
    
    f = open('AuthorLineCount.properties','w')
    f.write(AuthorLineCountProperties)
    f.close();   
    
    f = open('AuthorTestCount.properties','w')
    f.write(AuthorTestCountProperties)
    f.close();   
    
    f = open('AuthorLineTestCount.csv','w')
    f.write(summaryCsv)
    f.close();            

           
if __name__ == "__main__":
    main()
