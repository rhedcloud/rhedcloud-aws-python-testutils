"""
=============================
AWS Elastic Container Service
=============================

Utilities for AWS ECS.

"""

from contextlib import contextmanager

from .decorators import aws_client, ignore_errors
from .utils import has_status, make_identifier, retry, unexpected


@contextmanager
@aws_client("ecs")
def new_ecs_cluster(prefix, *, ecs):
    """Context manager to create a ECS cluster and remove it when it's
    no longer used.

    :param str prefix:
        The prefix to attach to the randomized name of the cluster

    :yields:
        An ECS Cluster Name

    """

    name = make_identifier(prefix)
    res = None
    try:
        res = ecs.create_cluster(clusterName=name)
        print("Created ECS Cluster: {}".format(name))
        yield name
    finally:
        if res is not None:
            print("Removing ECS Cluster: {}".format(name))
            res = retry(
                ecs.delete_cluster,
                kwargs=dict(
                    cluster=name,
                ),
                msg="Deleting ECS cluster: {}".format(name),
            )
            assert has_status(res, 200), unexpected(res)


@contextmanager
@aws_client("ecs")
def new_ecs_task_definition(prefix, task_role, container_command, *, ecs):
    """Context manager to create a new ECS task definition and remove it when
    it's no longer used.

    :param str prefix:
        The prefix to attach to the randomized name of the task definition
    :param boto3.IAM.Role ecs_task_role:
        A Role permitted to run ECS tasks.
    :param str container_command:
        The command to run in the container created by this task

    :yields:
        An ECS Task Definition Name(with appended revision :1)

    """

    name = make_identifier(prefix)
    res = None
    try:
        res = ecs.register_task_definition(
            family=name,
            taskRoleArn=task_role.arn,
            executionRoleArn=task_role.arn,
            networkMode="awsvpc",
            containerDefinitions=[
                {
                    "name": "boto-sample-app",
                    "image": "httpd:2.4",
                    "memoryReservation": 512,
                    "portMappings": [{"containerPort": 80}],
                    "essential": True,
                    "entryPoint": ["sh", "-c"],
                    "command": [container_command],
                }
            ],
            requiresCompatibilities=["FARGATE"],
            cpu=".25 vCPU",
            memory="512",
        )
        versioned_name = name + ":1"
        print("Created ECS Task Definition: {}".format(versioned_name))
        yield versioned_name
    finally:
        if res is not None:
            try:
                print("Removing ECS Task Definition: {}".format(versioned_name))
                res = ecs.deregister_task_definition(taskDefinition=versioned_name)
                assert has_status(res, 200), unexpected(res)
            except ecs.exceptions.ClusterContainsServicesException:
                pass


@contextmanager
@aws_client("ecs")
def new_ecs_service(prefix, cluster_name, task_def_name, sg_id, subnet_id, *, ecs):
    """Context manager to create a new ECS service and remove it when it's
    no longer used.

    :param str prefix:
        The prefix to attach to the randomized name of the service
    :param str cluster_name:
        The name of the cluster to create this service within
    :param str task_def_name:
        The name of the task that this service will run
    :param str sg_id:
        The id of the security group that this service will run under
    :param str subnet_id:
        The id of the subnet this service will run in

    :yields:
        An ECS Service Name

    """

    name = make_identifier(prefix)
    res = None
    try:
        res = create_service(
            name,
            cluster_name,
            task_def_name,
            sg_id,
            subnet_id,
            ecs=ecs,
        )
        yield name
    finally:
        if res is not None:
            ignore_errors(delete_service)(
                name,
                cluster_name,
                ecs=ecs,
            )


@aws_client("ecs")
def create_service(name, cluster_name, task_def_name, sg_id, subnet_id, *, ecs):
    """Create a new ECS service.

    :param str name:
        Name of the service.
    :param str cluster_name:
        The name of the cluster to create this service within
    :param str task_def_name:
        The name of the task that this service will run
    :param str sg_id:
        The id of the security group that this service will run under
    :param str subnet_id:
        The id of the subnet this service will run in

    :returns:
        A dictionary.

    """

    res = ecs.create_service(
        cluster=cluster_name,
        serviceName=name,
        taskDefinition=task_def_name,
        desiredCount=1,
        launchType="FARGATE",
        networkConfiguration={
            "awsvpcConfiguration": {
                "subnets": [subnet_id],
                "securityGroups": [sg_id],
            }
        },
    )
    assert has_status(res, 200), unexpected(res)

    print("Created ECS Service: {}".format(name))
    return res


@aws_client("ecs")
def delete_service(name, cluster_name, *, ecs):
    """Delete an ECS service.

    :param str name:
        Name of the service.
    :param str cluster_name:
        The name of the cluster to create this service within

    """

    print("Removing ECS Service: {}".format(name))
    res = ecs.update_service(
        cluster=cluster_name,
        service=name,
        desiredCount=0,
    )
    res = ecs.delete_service(
        cluster=cluster_name,
        service=name,
    )
    assert has_status(res, 200), unexpected(res)
