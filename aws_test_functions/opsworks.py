"""
============
AWS OpsWorks
============

"""

from .decorators import aws_client, catch


@aws_client('opsworks')
def create_stack(name, vpc_id, subnet_id, role, profile, *, opsworks):
    """Create a new OpsWork stack.

    :param str name:
        Name of the stack to create.
    :param str vpc_id:
        ID of the VPC the stack should be attached to by default.
    :param str subnet_id:
        ID of the subnet the stack should be attached to by default.
    :param IAM.Role role:
        Service Role for OpsWorks to use when performing actions on behalf of
        the user.
    :param IAM.InstanceProfile profile:
        Default Instance Profile to assign instances created in the stack.

    :returns:
        A dictionary.

    """

    role.reload()

    res = opsworks.create_stack(
        Name=name,
        Region='us-east-1',
        VpcId=vpc_id,
        ServiceRoleArn=role.arn,
        DefaultInstanceProfileArn=profile.arn,
        DefaultSubnetId=subnet_id,
    )

    return res


@catch(Exception, msg='Failed to stop instance: {ex}')
@aws_client('opsworks')
def stop_instance(instance_id, *, wait=True, opsworks):
    """Stop an instance.

    :param str instance_id:
        ID of an Instance that should be stopped.
    :param bool wait: (optional)
        Whether to wait for the Instance to appear to be stopped.

    """

    print('Stopping instance: {}'.format(instance_id))
    opsworks.stop_instance(
        InstanceId=instance_id,
        Force=True
    )

    if wait:
        waiter = opsworks.get_waiter('instance_stopped')
        waiter.wait(InstanceIds=[instance_id])


@catch(Exception, msg='Failed to delete instance: {ex}')
@aws_client('opsworks')
def delete_instance(instance_id, *, wait=True, opsworks):
    """Delete an instance.

    :param str instance_id:
        ID of an Instance that should be stopped.
    :param bool wait: (optional)
        Whether to wait for the Instance to appear to be terminated.

    """

    stop_instance(instance_id, opsworks=opsworks)

    print('Deleting instance: {}'.format(instance_id))
    opsworks.delete_instance(
        InstanceId=instance_id,
        DeleteElasticIp=True,
        DeleteVolumes=True,
    )

    if wait:
        waiter = opsworks.get_waiter('instance_terminated')
        waiter.wait(InstanceIds=[instance_id])


@catch(Exception, msg='Failed to delete layer: {ex}')
@aws_client('opsworks')
def delete_layer(layer_id, *, aggressive=True, wait=True, opsworks):
    """Delete an OpsWorks Layer.

    :param str layer_id:
        ID of an Layer that should be removed.
    :param bool aggressive: (optional)
        Whether to delete Instances before deleting the layer.
    :param bool wait: (optional)
        Whether to wait for the instances within the layer to appear to be
        terminated.

    """

    if aggressive:
        res = opsworks.describe_instances(LayerId=layer_id)
        instance_ids = [i['InstanceId'] for i in res['Instances']]

        for inst_id in instance_ids:
            delete_instance(inst_id, wait=False, opsworks=opsworks)

        if wait and instance_ids:
            waiter = opsworks.get_waiter('instance_terminated')
            waiter.wait(InstanceIds=instance_ids)

    print('Deleting layer: {}'.format(layer_id))
    opsworks.delete_layer(LayerId=layer_id)


@catch(Exception, msg='Failed to delete stack: {ex}')
@aws_client('opsworks')
def delete_stack(stack_id, *, aggressive=True, wait=True, opsworks):
    """Delete an OpsWorks Stack.

    :param str stack_id:
        ID of an Stack that should be removed.
    :param bool aggressive: (optional)
        Whether to delete Layers (and Instances) before deleting the stack.
    :param bool wait: (optional)
        Whether to wait for the instances within the stack to appear to be
        terminated.

    """

    if aggressive:
        res = opsworks.describe_layers(StackId=stack_id)
        for layer in res['Layers']:
            delete_layer(layer['LayerId'], wait=wait, opsworks=opsworks)

    print('Deleting stack: {}'.format(stack_id))
    opsworks.delete_stack(StackId=stack_id)


@catch(Exception, msg='Failed to cleanup OpsWorks: {ex}')
@aws_client('opsworks')
def cleanup(*, opsworks):
    """Attempt to remove any unused OpsWorks stacks."""

    print('Deleting any OpsWorks stacks...')
    res = opsworks.describe_stacks()
    for stack in res['Stacks']:
        delete_stack(stack['StackId'], wait=False, opsworks=opsworks)
