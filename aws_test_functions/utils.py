"""
======================
Test Utility Functions
======================

"""

import functools
import inspect
import logging
import sys
import time
import uuid

from boto3.session import Session
import pytest


def retry(
    func,
    args=None,
    kwargs=None,
    *,
    msg=None,
    delay=5,
    max_attempts=12,
    backoff=0,
    pred=None,
    exceptions=Exception,
    show=False,
    msg_min_attempts=0,
    until=None
):
    """Reusable retry helper.

    :param callable func:
        The function to call, potentially ``max_attempts`` times.
    :param tuple args: (optional)
        Any positional arguments to pass to ``func`` when invoked.
    :param dict kwargs: (optional)
        Any keyword arguments to pass to ``func`` when invoked.
    :param str msg: (optional)
        An optional message to print on each attempt.
    :param int delay: (optional)
        Number of seconds to wait before retrying. Default is ``5``.
    :param int max_attempts: (optional)
        Maximum number of attempts to make before failing. Default is ``12``.
    :param int backoff: (optional)
        Number of seconds to add to the delay on each retry. Default is ``0``.
    :param callable pred: (optional)
        An optional predicate to satisfy before retrying.
    :param tuple exceptions: (optional)
        One or more exceptions to catch to enable the retry logic.
    :param bool show: (optional)
        Whether to display the exception that is caught.
    :param int msg_min_attempts: (optional)
        Minimum number of attempts before displaying the specified message, if
        any. Default behavior always prints ``msg`` on each attempt.
    :param callable until: (optional)
        A callable that determines whether the result is an acceptable value.
        Useful for checking the status of resources that take a while to
        provision.

    """

    if args is None:
        args = tuple()

    if kwargs is None:
        kwargs = dict()

    if not isinstance(exceptions, tuple):
        exceptions = (exceptions,)

    add = 0
    result = None
    for attempt in range(max_attempts):
        if msg and attempt >= msg_min_attempts:
            print('{} (attempt #{})'.format(msg, attempt + 1))

        try:
            result = func(*args, **kwargs)
            if callable(until):
                if until(result):
                    return result
            else:
                return result
        except exceptions as ex:
            # always raise if this is our last attempt
            if attempt == max_attempts - 1:
                raise

            if callable(pred):
                if not pred(ex):
                    raise

            if show:
                print(ex)

        time.sleep(delay)
        add += backoff
        delay += add

    return result


def dict_to_filters(key_values, key_name=None, values_name=None):
    """Turn the specified `key_values` dictionary into a list of dictionaries
    suitable for AWS filters.

    :param dict key_values:
        A dictionary.
    :param str key_name: (optional)
        Name of the key to use. Defaults to `Name`.
    :param str values_name: (optional)
        Name of the values key to use. Defaults to `Values`.

    :returns:
        A list of dictionaries.

    """

    if key_name is None:
        key_name = 'Name'

    if values_name is None:
        values_name = 'Values'

    return [{
        key_name: k,
        values_name: v if isinstance(v, list) else [v],
    } for k, v in key_values.items()]


def client_resource(client, service=None):
    """Create a handle to the AWS Service Resource APIs from an existing AWS
    Client, using the same credentials as the existing client.

    :param Client client:
        An existing client handle to AWS service APIs.
    :param str service: (optional)
        Name of the service to create the Service Resource handle for. Defaults
        to the same service as the specified client.

    :returns:
        A handle to the Service Resource APIs for the specified service.

    """

    if service is None:
        service = client.meta.service_model.service_name

    creds = client._request_signer._credentials
    session = Session(creds.access_key, creds.secret_key, creds.token)

    return session.resource(service)


def build_fixture(key, with_assertion=True, depth=1):
    """Create a new fixture using shared variables.

    :param str key:
        The name of the fixture to create.
    :param bool with_assertion: (optional)
        Whether to assert that the shared variable for the specified key is
        truthy before returning. Default is ``True``.

        This is useful to disable when testing fully-blocked services because
        you want to get an "Access Denied" error, from an API call rather than
        an assertion error about missing variables.
    :param int depth: (optional)
        Number of levels deep into the stack to inject the dynamic fixture.

    :returns:
        A pytest fixture.

    """

    # print('Creating dynamic fixture: {}'.format(key))

    @pytest.fixture(scope='function')
    def f(shared_vars):
        if with_assertion:
            assert shared_vars[key], 'shared variable {} not set'.format(key)

        return shared_vars[key]

    st = inspect.stack()
    frame = st[depth].frame
    frame.f_locals[key] = f

    f.__name__ = key
    f.__module__ = inspect.getmodule(frame).__name__

    return f


def get_uuid():
    """Creates a UUID for use in randomizing names.

    :returns:
        A string containing a UUID

    """

    return str(uuid.uuid4())


def make_identifier(prefix, separator='-', max_length=None):
    identifier = ''.join([prefix, separator, get_uuid()])

    if max_length is not None:
        identifier = identifier[:max_length]

    return identifier


def unexpected(res):
    return 'Unexpected response: {}'.format(res)


def has_status(res, *expected_codes):
    """Determine whether the specified API response has a specific status code.

    :param dict res:
        A response from an AWS API call.
    :param *int expected_codes:
        One or more HTTP response codes.

    :returns:
        A boolean.

    """

    expected_codes = [i for i in expected_codes if isinstance(i, int)]
    assert len(expected_codes), 'At least one numeric HTTP response code is required'

    return res['ResponseMetadata']['HTTPStatusCode'] in expected_codes


def _some_in(reducer, haystack, *needles, modifier=None):
    """Generic function to find one or more needles in a haystack.

    :param callable reducer:
        A function to reduce the results of filtering which needles are found
        in the haystack.
    :param object haystack:
        An object to stringify in order to find any of the needles.
    :param *str needles:
        One or more needles to find in the haystack.
    :param callable modifier: (optional)
        Function to modify the value of each needle prior to searching the
        haystack. This can be used for forcing all needles to be lowercase, for
        example.

    :returns:
        A boolean.

    Usage:

    >>> any_in('Testing', 'a', 'e', 'i', 'o', 'u')
    True
    >>> all_in('Testing', 'a', 'e', 'i', 'o', 'u')
    False
    >>> all_in('Racecar', 'R', 'A', 'C', 'E', modifier=str.lower)
    True
    >>> all_in('Racecar', 'r', 'a', 'c', 'e', modifier=str.upper)
    False
    >>> any_in('Racecar', 'r', 'a', 'c', 'e', modifier=str.upper)
    True

    """

    haystack = str(haystack)
    if callable(modifier):
        needles = (modifier(n) for n in needles)

    return reducer(n in haystack for n in needles)


# return True when ANY needle is found in the haystack
any_in = functools.partial(_some_in, any)

# return True when ALL needles are found in the haystack
all_in = functools.partial(_some_in, all)

# print informational messages to stderr
logging.addLevelName(1337, "RHEDCLOUD")
logging.basicConfig(level="RHEDCLOUD", format="%(msg)s")
log = logging.getLogger("rhedcloud")

error = log.error
info = log.info
log.debug = debug = functools.partial(log.log, 1337)

# debug = functools.partial(print, file=sys.stderr)


def ow_debug(msg, *args, **kwargs):
    """Debug function that allows itself to be overwritten."""

    kwargs.update({
        "file": sys.stderr,
        "end": "\r",
        "flush": True,
    })

    print("\033[K{}".format(msg), *args, **kwargs)
