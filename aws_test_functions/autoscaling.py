"""
===============
AWS Autoscaling
===============

Utilities for AWS Autoscaling.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import debug, has_status, retry, unexpected


@contextmanager
@aws_client('autoscaling')
def new_launch_configuration(prefix, *, autoscaling):
    """Context manager to create an Autoscaling launch configuration and remove it
    when it is no longer used

    :param str prefix:
        Prefix to give the launch configuration when generating a name

    :yields:
        A launch configuration name

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        res = autoscaling.create_launch_configuration(
            LaunchConfigurationName=name,
            InstanceType='t2.micro',
            ImageId='ami-55ef662f'
        )

        assert has_status(res, 200), unexpected(res)
        debug('Created Autoscaling launch configuration: {}'.format(name))

        yield name
    finally:
        if res is not None:
            debug('Removing Autoscaling launch configuration: {}'.format(name))
            res = autoscaling.delete_launch_configuration(LaunchConfigurationName=name)
            assert has_status(res, 200), unexpected(res)


@contextmanager
@aws_client('autoscaling')
def new_autoscaling_group(prefix, launch_config_name, subnet_id, *, autoscaling):
    """Context manager to create an Autoscaling group and remove it
    when it is no longer used

    :param str prefix:
        Prefix to give the autoscaling group when generating a name
    :param str launch_config_name:
        The name of the launch configuration to use for the group
    :param str subnet_id:
        The id of the subnet to launch the group in

    :yields:
        An autoscaling group name

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        res = autoscaling.create_auto_scaling_group(
            AutoScalingGroupName=name,
            LaunchConfigurationName=launch_config_name,
            MaxSize=3,
            MinSize=0,
            VPCZoneIdentifier=subnet_id,
        )

        assert has_status(res, 200), unexpected(res)
        debug('Created Autoscaling group: {}'.format(name))

        yield name
    finally:
        if res is not None:
            debug('Removing Autoscaling group: {}'.format(name))
            res = autoscaling.delete_auto_scaling_group(AutoScalingGroupName=name, ForceDelete=True)
            assert has_status(res, 200), unexpected(res)


@contextmanager
@aws_client('autoscaling')
def attach_instances_to_group(instance_ids, group_name, wait, *, autoscaling):
    """Context manager to attach instances to a group and detach them when they are no longer in scope

    :param [str] instance_ids:
        List of instance_ids to attach to the group
    :param str group_name:
        The name of the launch configuration to use for the group
    :param bool wait:
        Whether to wait for the instance to reach Standby or InService in the group

    """

    res = None
    try:
        res = autoscaling.attach_instances(InstanceIds=instance_ids, AutoScalingGroupName=group_name)
        assert has_status(res, 200), 'Failed to create auto scaling launch group'
        debug('Attached instances {} to Autoscaling group {}'.format(instance_ids, group_name))

        if wait:
            for instance_id in instance_ids:
                retry(
                    autoscaling.describe_auto_scaling_instances,
                    kwargs=dict(InstanceIds=[instance_id]),
                    msg='Waiting for autoscaling instances to become available...',
                    until=lambda s: s['AutoScalingInstances'][0]['LifecycleState'] in ('Standby', 'InService'),
                    delay=5,
                    max_attempts=12,
                )

        yield
    finally:
        if res is not None:
            debug('Removing instances {} from Autoscaling group {}'.format(instance_ids, group_name))

            res = autoscaling.detach_instances(
                InstanceIds=instance_ids,
                AutoScalingGroupName=group_name,
                ShouldDecrementDesiredCapacity=True,
            )

            assert has_status(res, 200), unexpected(res)
