"""
============================
Custom Environment Variables
============================

This module contains custom environment variables to alter the behavior of
helper functions.

"""

import os


# ``IAM_DELAY`` controls how many seconds to wait after creating a test user
# using the ``create_test_user`` utility function. By default, the delay is 5
# seconds. It can be set using the ``RHEDCLOUD_IAM_DELAY`` environment
# variable.
IAM_DELAY = 0

# Redis connection information
REDIS_HOST = os.getenv('REDIS_HOST')
REDIS_PORT = os.getenv('REDIS_PORT')
REDIS_DB = int(os.getenv('REDIS_DB', 0))
REDIS_PASSWORD = os.getenv('REDIS_PASSWORD')

# Determine the default name of the tag used to identify the type of VPC.
RHEDCLOUD_VPC_TYPE_TAG = os.getenv('RHEDCLOUD_VPC_TYPE_TAG') or 'RHEDcloudVpcType'

# Determine the default type of VPC to use for testing based on the
# :envvar:`RHEDCLOUD_VPC_TYPE` environment variable. By default, a Type 1 VPC
# is assumed.
RHEDCLOUD_VPC_TYPE = os.getenv('RHEDCLOUD_VPC_TYPE') or '1'

# Name of the subnet to use for "internal" script runners.
RHEDCLOUD_INTERNAL_SUBNET = os.getenv('RHEDCLOUD_INTERNAL_SUBNET') or 'Public Subnet 1'

# Name of the subnet to use for "private" purposes.
RHEDCLOUD_PRIVATE_SUBNET = os.getenv('RHEDCLOUD_PRIVATE_SUBNET') or 'Private Subnet 1'

# Name of the subnet to use for "management" purposes.
RHEDCLOUD_MGMT_SUBNET = os.getenv('RHEDCLOUD_MGMT_SUBNET') or 'MGMT1 - IT ONLY'

# One or more comma-separated default subnet names to use when creating VPC
# endpoints.
RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS = (
    os.getenv('RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS') or
    'Public Subnet 1,Public Subnet 2'
).split(',')


def _load_env():
    """Load all custom environment variables."""

    _load_iam_delay()


def _load_iam_delay():
    """Load the value for ``RHEDCLOUD_IAM_DELAY``. Only positive integers are
    permitted.

    """

    global IAM_DELAY

    IAM_DELAY = 5

    try:
        value = int(os.getenv('RHEDCLOUD_IAM_DELAY'))
    except (ValueError, TypeError):
        pass
    else:
        if value >= 0:
            IAM_DELAY = value


_load_env()
