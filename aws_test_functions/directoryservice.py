"""
=====================
AWS Directory Service
=====================

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_subnet_id
from .utils import debug, retry


@contextmanager
@aws_client('ds')
def new_ad(name, vpc_id, subnets, *, ds):
    """Context manager to create and automatically clean up a Microsoft AD
    service in the specified VPC.

    :param str name:
        Name of the directory. Must be a fully qualified name, such as
        ``foo.bar.com``.
    :param str vpc_id:
        ID of a VPC.
    :param list subnets:
        A list of subnet IDs where the service will be setup.

    :yields:
        A string. The ID of the created directory.

    """

    directory_id = None

    try:
        res = ds.create_microsoft_ad(
            Name=name,
            Password='T3$t!nG)(*',
            VpcSettings=dict(
                VpcId=vpc_id,
                SubnetIds=subnets,
            ),
        )
        assert 'DirectoryId' in res, 'Unexpected response: {}'.format(res)
        directory_id = res['DirectoryId']
        debug('Created Microsoft AD: {}'.format(directory_id))

        yield directory_id
    finally:
        if directory_id is not None:
            debug('Deleting Microsoft AD: {}'.format(directory_id))
            ds.delete_directory(DirectoryId=directory_id)


@contextmanager
@aws_client('ds')
def new_simple_directory(name, vpc_id, subnets=None, *, wait=True, ds):
    """Context manager to create and automatically clean up a Simple Directory
    service in the specified VPC.

    :param str name:
        Name of the directory. Must be a fully qualified name, such as
        ``foo.bar.com``.
    :param str vpc_id:
        ID of a VPC.
    :param list subnets: (optional)
        A list of subnet IDs where the service will be setup. If ommitted, the
        two public subnets are used.
    :param bool wait: (optional)
        Whether to wait for the directory to be active.

    :yields:
        A string. The ID of the created directory.

    """

    if subnets is None:
        subnets = [
            get_subnet_id(vpc_id, 'Public Subnet 1'),
            get_subnet_id(vpc_id, 'Public Subnet 2'),
        ]

    did = None

    try:
        res = ds.create_directory(
            Name=name,
            Password='T3$t!nG)(*',
            Size='Small',
            VpcSettings=dict(
                VpcId=vpc_id,
                SubnetIds=subnets,
            ),
        )
        assert 'DirectoryId' in res, 'Unexpected response: {}'.format(res)
        did = res['DirectoryId']
        debug('Created simple directory: {}'.format(did))

        if wait:
            wait_for_directory_state(did, 'Active')

        yield did
    finally:
        if did is not None:
            debug('Deleting simple directory: {}'.format(did))
            ds.delete_directory(DirectoryId=did)


@aws_client('ds')
def wait_for_directory_state(directory_id, state, *, ds):
    """Wait for a while for the specified directory to be in a certain state.

    :param str directory_id:
        ID of the directory to watch.
    :param str state:
        Desired state for the directory.

    """

    retry(
        ds.describe_directories,
        kwargs=dict(DirectoryIds=[directory_id]),
        msg='Waiting for directory {} to enter state {}...'.format(directory_id, state),
        until=lambda s: s['DirectoryDescriptions']['Stage'] == state,
        delay=10,
        max_attempts=30,
    )
