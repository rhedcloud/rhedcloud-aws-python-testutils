"""
=============================
AWS Service Catalog Utilities
=============================

"""

from contextlib import contextmanager
import json

from .decorators import aws_client, catch
from .lib import get_uuid
from .utils import retry


@contextmanager
@aws_client('servicecatalog')
def new_portfolio(prefix, *, servicecatalog):
    """Context manager to create a new portfolio and remove it when it is no
    longer required.

    :param str prefix:
        Prefix to use with the portfolio's name.

    :yields:
        A dictionary with information about the created portfolio.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    portfolio = None

    try:
        portfolio = create_portfolio(name, servicecatalog=servicecatalog)

        yield portfolio
    finally:
        if portfolio is not None:
            delete_portfolio(portfolio['Id'], servicecatalog=servicecatalog)


@aws_client('servicecatalog')
def create_portfolio(name, *, servicecatalog):
    """Create a new portfolio.

    :param str name:
        Name to give the portfolio.

    :returns:
        A dictionary with information about the created portfolio.

    """

    res = servicecatalog.create_portfolio(
        DisplayName=name,
        ProviderName='test',
        IdempotencyToken=name,
    )
    assert 'PortfolioDetail' in res, 'Unexpected response: {}'.format(res)
    portfolio = res['PortfolioDetail']
    print('Created portfolio: {}'.format(portfolio['Id']))

    return portfolio


@catch(Exception, msg='Failed to delete portfolio: {ex}')
@aws_client('servicecatalog')
def delete_portfolio(identifier, *, servicecatalog):
    """Attempt to delete a portfolio.

    :param str identifier:
        A portfolio ID.

    """

    print('Deleting portfolio: {}'.format(identifier))
    servicecatalog.delete_portfolio(Id=identifier)


@catch(Exception, msg='Failed to cleanup portfolios: {ex}')
@aws_client('servicecatalog')
def cleanup_portfolios(*, servicecatalog):
    """Attempt to remove any unused portfolios."""

    res = servicecatalog.list_portfolios()
    for portfolio in res.get('PortfolioDetails', []):
        pid = portfolio['Id']
        cleanup_constraints(pid, servicecatalog=servicecatalog)
        delete_portfolio(pid, servicecatalog=servicecatalog)


@contextmanager
@aws_client('servicecatalog')
def new_product(prefix, url, *, servicecatalog):
    """Context manager to create a new product and remove it when it is no
    longer required.

    :param str prefix:
        Prefix to use with the product's name.
    :param str url:
        URL to a CloudFormation template.

    :yields:
        A dictionary with information about the created product.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    product = None

    try:
        product = create_product(name, url, servicecatalog=servicecatalog)

        yield product
    finally:
        if product is not None:
            delete_product(
                product['ProductViewDetail']['ProductViewSummary']['ProductId'],
                servicecatalog=servicecatalog,
            )


@aws_client('servicecatalog')
def create_product(name, url, *, servicecatalog):
    """Create a new product.

    :param str name:
        Name to give the product.
    :param str url:
        URL to a CloudFormation template.

    :returns:
        A dictionary with information about the created product.

    """

    res = servicecatalog.create_product(
        Name=name,
        Owner='rhedcloud',
        ProductType='CLOUD_FORMATION_TEMPLATE',
        ProvisioningArtifactParameters=dict(
            Info=dict(
                LoadTemplateFromURL=url,
            ),
            Type='CLOUD_FORMATION_TEMPLATE',
        ),
        IdempotencyToken=name,
    )
    assert 'ProductViewDetail' in res and 'ProvisioningArtifactDetail' in res, \
        'Unexpected response: {}'.format(res)

    product_id = res['ProductViewDetail']['ProductViewSummary']['ProductId']
    print('Created product: {}'.format(product_id))

    return res


@contextmanager
@aws_client('servicecatalog')
def copy_product(prefix, source_arn, *, servicecatalog):
    """Context manager to copy a product and remove the copy when it is no
    longer required.

    :param str prefix:
        Prefix to use with the product's name.
    :param str source_arn:
        Arn of the product to copy.

    :yields:
        A dictionary with information about the copied product.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    pid = None
    try:
        res = servicecatalog.copy_product(SourceProductArn=source_arn, TargetProductName=name)

        token = res['CopyProductToken']
        res = retry(
            servicecatalog.describe_copy_product_status,
            kwargs=dict(CopyProductToken=token),
            msg='Waiting for {} to finish copying'.format(name),
            until=lambda r: r['CopyProductStatus'] != 'IN_PROGRESS',
        )
        assert res['CopyProductStatus'] == 'SUCCEEDED', 'Failed to copy product with response: {}'.format(res)

        pid = res['TargetProductId']
        yield pid
    finally:
        if pid is not None:
            delete_product(pid, servicecatalog=servicecatalog)


@catch(Exception, msg='Failed to delete product: {ex}')
@aws_client('servicecatalog')
def delete_product(identifier, *, servicecatalog):
    """Attempt to delete a product.

    :param str identifier:
        A product ID.

    """

    print('Deleting product: {}'.format(identifier))
    servicecatalog.delete_product(Id=identifier)


@catch(Exception, msg='Failed to cleanup products: {ex}')
@aws_client('servicecatalog')
def cleanup_products(*, servicecatalog):
    """Attempt to remove any unused products."""

    res = servicecatalog.search_products_as_admin()
    for product in res.get('ProductViewDetails', []):
        delete_product(
            product['ProductViewSummary']['ProductId'],
            servicecatalog=servicecatalog,
        )


@contextmanager
@aws_client('servicecatalog')
def new_portfolio_product(portfolio_id, product_id, *, servicecatalog):
    """Context manager to associate a product with a portfolio and disassociate
    it when it is no longer required.

    :param str portfolio_id:
        ID of a portfolio.
    :param str product_id:
        ID of a product.

    """

    created = False
    params = dict(
        ProductId=product_id,
        PortfolioId=portfolio_id,
    )

    try:
        res = servicecatalog.associate_product_with_portfolio(**params)
        assert isinstance(res, dict), 'Unexpected response: {}'.format(res)
        created = True

        yield res
    finally:
        if created:
            servicecatalog.disassociate_product_from_portfolio(**params)


@contextmanager
@aws_client('servicecatalog')
def new_launch_constraint(portfolio_id, product_id, role, *, servicecatalog):
    """Context manager to create a new launch constraint on a product in the
    specified portfolio and remove the constraint when it is no longer required.

    :param str portfolio_id:
        ID of a portfolio.
    :param str product_id:
        ID of a product.
    :param IAM.Role role:
        An IAM Role resource.

    :yields:
        A dictionary with information about the created constraint.

    """

    constraint_id = None

    try:
        res = create_launch_constraint(
            portfolio_id,
            product_id,
            role,
            servicecatalog=servicecatalog,
        )
        constraint_id = res['ConstraintDetail']['ConstraintId']

        yield res
    finally:
        if constraint_id is not None:
            delete_constraint(constraint_id, servicecatalog=servicecatalog)


@aws_client('servicecatalog')
def create_launch_constraint(portfolio_id, product_id, role, *, servicecatalog):
    """Create a new launch constraint on a product in the specified portfolio.

    :param str portfolio_id:
        ID of a portfolio.
    :param str product_id:
        ID of a product.
    :param IAM.Role role:
        An IAM Role resource.

    :returns:
        A dictionary with information about the created constraint.

    """

    res = servicecatalog.create_constraint(
        PortfolioId=portfolio_id,
        ProductId=product_id,
        Parameters=json.dumps(dict(
            RoleArn=role.arn,
        )),
        Type='LAUNCH',
    )
    assert 'ConstraintDetail' in res, 'Unexpected response: {}'.format(res)
    print('Created launch constraint: {}'.format(res['ConstraintDetail']['ConstraintId']))

    return res


@catch(Exception, msg='Failed to delete constraint: {ex}')
@aws_client('servicecatalog')
def delete_constraint(identifier, *, servicecatalog):
    """Attempt to delete a constraint.

    :param str identifier:
        A constraint ID.

    """

    print('Deleting constraint: {}'.format(identifier))
    servicecatalog.delete_constraint(Id=identifier)


@catch(Exception, msg='Failed to cleanup constraints: {ex}')
@aws_client('servicecatalog')
def cleanup_constraints(portfolio_id, *, servicecatalog):
    """Attempt to remove any unused constraints in the specified portfolio.

    :param str portfolio_id:
        ID of the portfolio which will have constraints cleaned up.

    """

    res = servicecatalog.list_constraints_for_portfolio(
        PortfolioId=portfolio_id,
    )

    for const in res.get('ConstraintDetails', []):
        delete_constraint(
            const['ConstraintId'],
            servicecatalog=servicecatalog,
        )


@contextmanager
@aws_client('servicecatalog')
def new_portfolio_principal(portfolio_id, arn, *, servicecatalog):
    """Context manager to associate a new principal (IAM User, Group, or Role)
    with the specified portfolio and remove the prinicpal when it is no longer
    required.

    :param str portfolio_id:
        ID of a portfolio.
    :param str arn:
        The ARN of the resource to associate with the portfolio.

    """

    params = dict(
        PortfolioId=portfolio_id,
        PrincipalARN=arn,
    )
    created = False

    try:
        servicecatalog.associate_principal_with_portfolio(
            PrincipalType='IAM',
            **params,
        )
        print('Associated principal with portfolio {}: {}'.format(portfolio_id, arn))
        created = True

        yield
    finally:
        if created:
            print('Disassociating principal from portfolio {}: {}'.format(
                portfolio_id, arn
            ))
            servicecatalog.disassociate_principal_from_portfolio(**params)


@contextmanager
@aws_client('servicecatalog')
def new_product_plan(prefix, product_id, artifact_id, *, servicecatalog):
    """Context manager to create a new product provisioning plan and remove it
    when it is no longer required.

    :param str prefix:
        Prefix to use with the plan's name.
    :param str product_id:
        ID of a product.
    :param str artifact_id:
        ID of a provisioning artifact.

    :yields:
        A string: the ID of the created plan.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    plan_id = None

    try:
        plan_id = create_product_plan(
            name,
            product_id,
            artifact_id,
            servicecatalog=servicecatalog)

        yield plan_id
    finally:
        if plan_id is not None:
            delete_product_plan(plan_id, servicecatalog=servicecatalog)


@aws_client('servicecatalog')
def create_product_plan(name, product_id, artifact_id, *, servicecatalog):
    """Create a new product provisioning plan.

    :param str name:
        Name of the plan to create.
    :param str product_id:
        ID of a product.
    :param str artifact_id:
        ID of a provisioning artifact.

    :yields:
        A string: the ID of the created plan.

    """

    res = servicecatalog.create_provisioned_product_plan(
        PlanName=name,
        PlanType='CLOUDFORMATION',
        ProductId=product_id,
        ProvisionedProductName=name,
        ProvisioningArtifactId=artifact_id,
    )
    assert 'PlanId' in res, 'Unexpected response: {}'.format(res)
    plan_id = res['PlanId']
    print('Created product plan: {}'.format(plan_id))

    return plan_id


@catch(Exception, msg='Failed to delete constraint: {ex}')
@aws_client('servicecatalog')
def delete_product_plan(plan_id, *, servicecatalog):
    """Attempt to delete the specified product provisioning plan.

    :param str plan_id:
        ID of the plan to delete.

    """

    print('Deleting product plan: {}'.format(plan_id))
    servicecatalog.delete_provisioned_product_plan(PlanId=plan_id)


@contextmanager
@aws_client('servicecatalog')
@aws_client('cloudformation')
def new_product_launch(
    prefix, product_id, artifact_id, provisioning_params, *, servicecatalog, cloudformation
):
    """Context manager to create a new product launch and terminate it
    when it is no longer required.

    :param str prefix:
        Prefix to use with the plan's name.
    :param str product_id:
        ID of a product.
    :param str artifact_id:
        ID of a provisioning artifact.
    :param list provisioning_params:
        List of parameters used by service catalog when provisioning this product

    :yields:
        A dictionary of information about the launched product

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        res = create_product_launch(
            name,
            product_id,
            artifact_id,
            provisioning_params,
            servicecatalog=servicecatalog
        )

        record_id = res['RecordDetail']['RecordId']
        yield res
    finally:
        if res is not None:
            terminate_product_launch(name=name, servicecatalog=servicecatalog)
            launch_info = servicecatalog.describe_record(Id=record_id)
            stack_arn = next(
                item['OutputValue']
                for item in launch_info['RecordOutputs']
                if item['OutputKey'] == 'CloudformationStackARN'
            )
            cloudformation.get_waiter('stack_delete_complete').wait(StackName=stack_arn)


@aws_client('servicecatalog')
def create_product_launch(name, product_id, artifact_id, provisioning_params, *, servicecatalog):
    """Create a new product launch.

    :param str prefix:
        Prefix to use with the plan's name.
    :param str product_id:
        ID of a product.
    :param str artifact_id:
        ID of a provisioning artifact.
    :param list provisioning_params:
        List of parameters used by service catalog when provisioning this product

    :yields:
        A dictionary of information about the launched product

    """

    res = servicecatalog.provision_product(
        ProductId=product_id,
        ProvisioningArtifactId=artifact_id,
        ProvisionedProductName=name,
        ProvisioningParameters=provisioning_params,
    )

    return res


@catch(Exception, msg='Failed to delete constraint: {ex}')
@aws_client('servicecatalog')
def terminate_product_launch(name, *, servicecatalog):
    """Attempt to delete the specified product launch.

    :param str name:
        Name of the launch to terminate.

    """

    print('Terminating provisioned product: {}'.format(name))
    servicecatalog.terminate_provisioned_product(ProvisionedProductName=name)
