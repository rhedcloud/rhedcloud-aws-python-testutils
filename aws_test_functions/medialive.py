"""
=============
AWS MediaLive
=============

Utilities for AWS MediaLive.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import has_status, retry


AUDIO_DESCRIPTIONS = dict(
    AudioTypeControl='FOLLOW_INPUT',
    LanguageCodeControl='FOLLOW_INPUT',
    Name='Test_ML_Audio_Description',
    AudioSelectorName='Test_ML_Audio_Selector',
)

M3U8_SETTINGS = dict(
    AudioFramesPerPes=4,
    AudioPids='492,493,494',
    EcmPid='180',
    PcrControl="PCR_EVERY_PES_PACKET",
    PmtPid='480',
    ProgramNum=1,
    Scte35Behavior='NO_PASSTHROUGH',
    Scte35Pid='500',
    TimedMetadataBehavior='NO_PASSTHROUGH',
    VideoPid='481',
)

OUTPUTS = dict(
    OutputSettings=dict(HlsOutputSettings=dict(HlsSettings=dict(StandardHlsSettings=dict(M3u8Settings=M3U8_SETTINGS)))),
    VideoDescriptionName='Test_ML_Video_Description',
    AudioDescriptionNames=['Test_ML_Audio_Description'],
)

HLS_CDN_SETTINGS = dict(
    HlsBasicPutSettings=dict(ConnectionRetryInterval=1, FilecacheDuration=300, NumRetries=10, RestartDelay=15)
)


@contextmanager
@aws_client('medialive')
def new_medialive_input_security_group(delete, *, medialive):
    """Context manager to create a MediaLive input security group and remove it when it's
    no longer used.

    :param bool delete:
        Whether or not to delete the input security group when finished.

    :yields:
        A MediaLive input security group id

    """

    res = None
    input_sg_id = None
    try:
        res = medialive.create_input_security_group(WhitelistRules=[{'Cidr': '170.140.0.0/16'}])
        assert has_status(res, 200, 201)
        input_sg_id = res['SecurityGroup']['Id']
        yield input_sg_id
    finally:
        if delete and res is not None:
            res = medialive.delete_input_security_group(InputSecurityGroupId=input_sg_id)
            assert has_status(res, 200, 201)


@contextmanager
@aws_client('medialive')
def new_medialive_input(prefix, input_sg_id, hls_url_primary, hls_url_backup, delete, *, medialive):
    """Context manager to create a MediaLive input and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the input's name.
    :param str input_sg_id:
        The input security group ID to use for this input.
    :param str hls_url_primary:
        The location of a hosted m3u8 file.
    :param str hls_url_backup:
        A secondary location of the hosted m3u8 file.
    :param bool delete:
        Whether or not to delete the input when finished.

    :yields:
        A MediaLive input id

    """

    res = None
    input_id = None
    input_name = '{}-{}'.format(prefix, get_uuid())
    try:
        res = medialive.create_input(
            InputSecurityGroups=[input_sg_id],
            Name=input_name,
            Sources=[dict(Url=hls_url_primary), dict(Url=hls_url_backup)],
            Type='URL_PULL',
        )
        assert has_status(res, 200, 201)
        input_id = res['Input']['Id']
        yield input_id
    finally:
        if delete and res is not None:
            res = medialive.delete_input(InputId=input_id)
            assert has_status(res, 200, 201)


@contextmanager
@aws_client('medialive')
def new_medialive_channel(prefix, destination, input_id, role, delete, *, medialive):
    """Context manager to create a MediaLive channel and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the input's name.
    :param dict destination:
        A dictionary describing the MediaLive channel destinations.
    :param str role:
        An ARN of an IAM Role configured to run MediaLive services.
    :param str input_id:
        The input ID to use for this channel.
    :param bool delete:
        Whether or not to delete the input when finished.

    :yields:
        A MediaLive channel id

    """

    res = None
    channel_id = None
    channel_name = '{}-{}'.format(prefix, get_uuid())
    try:
        res = medialive.create_channel(
            Destinations=[destination],
            EncoderSettings=dict(
                AudioDescriptions=[AUDIO_DESCRIPTIONS],
                OutputGroups=[
                    dict(
                        Name='Test_ML_Output_Group',
                        OutputGroupSettings=dict(
                            HlsGroupSettings=dict(
                                HlsCdnSettings=HLS_CDN_SETTINGS, Destination=dict(DestinationRefId=destination['Id'])
                            )
                        ),
                        Outputs=[OUTPUTS],
                    )
                ],
                TimecodeConfig=dict(Source="EMBEDDED"),
                VideoDescriptions=[dict(Name='Test_ML_Video_Description')],
            ),
            InputAttachments=[dict(InputId=input_id)],
            Name=channel_name,
            RoleArn=role,
        )
        assert has_status(res, 200, 201)

        channel_id = res['Channel']['Id']
        status = retry(
            medialive.describe_channel,
            kwargs={'ChannelId': channel_id},
            msg='Waiting for MediaLive channel {} to finish creating'.format(channel_id),
            until=lambda s: s['State'] != 'CREATING',
            delay=10,
            max_attempts=15,
        )
        assert status['State'] == 'IDLE', "Failed to create MediaLive channel {} with status {}".format(
            channel_id, status['State']
        )

        medialive.start_channel(ChannelId=channel_id)
        status = retry(
            medialive.describe_channel,
            kwargs={'ChannelId': channel_id},
            msg='Waiting for MediaLive channel {} to start'.format(channel_id),
            until=lambda s: s['State'] == 'RUNNING',
            delay=10,
            max_attempts=15,
        )
        assert status['State'] == 'RUNNING', "MediaLive channel {} failed to start with status {}".format(
            channel_id, status['State']
        )

        yield channel_id
    finally:
        if delete and res is not None:
            medialive.stop_channel(ChannelId=channel_id)
            status = retry(
                medialive.describe_channel,
                kwargs={'ChannelId': channel_id},
                msg='Waiting for MediaLive channel {} to stop'.format(channel_id),
                until=lambda s: s['State'] != 'STOPPING',
                delay=10,
                max_attempts=15,
            )
            assert status['State'] == 'IDLE', "Failed to stop MediaLive channel {} with status {}".format(
                channel_id, status['State']
            )

            res = medialive.delete_channel(ChannelId=channel_id)
            assert has_status(res, 200, 201)
            status = retry(
                medialive.describe_channel,
                kwargs={'ChannelId': channel_id},
                msg='Waiting for MediaLive channel {} to finish deleting'.format(channel_id),
                until=lambda s: s['State'] == 'DELETED',
                delay=10,
                max_attempts=15,
            )
            assert status['State'] == 'DELETED', "Failed to delete MediaLive channel {} with status {}".format(
                channel_id, status['State']
            )
