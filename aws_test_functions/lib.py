"""
=====================
Core Function Library
=====================

This is the source of functions common to more than one test.

.. todo::
    Reorganize this module to keep related functions in meaningfully named
    modules.

"""

from contextlib import ExitStack, contextmanager
from functools import lru_cache

try:
    from urllib.request import urlopen, Request
except ImportError:
    from urllib2 import urlopen, Request
import json
import ipaddress
import pprint
import time
import uuid

from botocore.exceptions import ClientError
import boto3

from .decorators import aws_client, aws_resource, catch, ignore_errors
from .env import IAM_DELAY, RHEDCLOUD_VPC_TYPE_TAG
from .iam import get_access_key
from .utils import dict_to_filters, has_status, get_uuid, retry


@catch(ClientError, msg="Error: {ex}")
@contextmanager
@aws_resource("iam")
def new_test_user(prefix, *, iam):
    """Context manager that creates a test user and removes that test user when
    the block finishes.

    :param str prefix:
        The prefix to assign to the new IAM user name.

    :yields:
        The IAM.User resource for the created user.

    """

    user = username = None

    try:
        info = create_test_user(prefix, iam=iam.meta.client)
        username = info["UserName"]
        user = iam.User(username)

        yield user
    except Exception:
        raise
    finally:
        if user is not None:
            print("Removing test user: {}".format(username))
            delete_test_user(username)


@contextmanager
@aws_client("iam")
def user_policy(username, role_name, policy_name=None, *, iam):
    """Context manager that adds or updates an inline policy for the specified
    `username` and cleans up after itself when the block finishes.

    :param str username:
        The name of the AWS User to modify.
    :param str role_name:
        Name of the AWS Role whose policy will be applied to `username`.
    :param str policy_name: (optional)
        Name of the Role Policy to use. If omitted, `root` is used by default.
    :param botocore.client.IAM iam: (optional)
        An existing IAM client. If omitted, a new client will be created.

    :yields:
        The same `username` that was passed into the context manager.

    """

    if policy_name is None:
        policy_name = 'root'

    try:
        policy_doc = get_role_policy_doc(role_name)
        json_policy_doc = json.dumps(policy_doc)
        print('Policy doc: {}'.format(json_policy_doc))

        iam.put_user_policy(
            UserName=username,
            PolicyName=policy_name,
            PolicyDocument=json_policy_doc)

        yield username
    except Exception:
        raise
    finally:
        try:
            iam.delete_user_policy(UserName=username, PolicyName=policy_name)
        except ClientError as ex:
            print('Unexpected error when deleting user inline policy\n{}'.format(ex))


@contextmanager
@aws_resource("iam")
def new_user(prefix, *policies, stack=None, iam):
    """Create a new test user with certain IAM Policies attached.

    :param str prefix:
        Prefix for the generated user name.
    :param *str policies: (optional)
        One or more names of IAM Policies to attach to the new user.
    :param contextlib.ExitStack stack: (optional)
        An existing stack instance.

    :yields:
        An IAM User resource.

    """

    if stack is None:
        stack = ExitStack()

    with stack:
        u = stack.enter_context(new_test_user(prefix, iam=iam))

        if policies:
            stack.enter_context(attach_policy(u, *policies))

        yield u


def get_policy_arn(policy, account=None):
    """Get a full ARN for the specified `policy`.

    :param str policy:
        IAM Policy name.
    :param str account: (optional)
        AWS account number. If omitted, the account number will be determined
        automatically.

    :returns:
        A string.

    Usage:

    >>> get_policy_arn('arn:aws:iam::99999:policy/RHEDcloudManagementSubnetPolicy')
    'arn:aws:iam::99999:policy/RHEDcloudManagementSubnetPolicy'
    >>> get_policy_arn('AmazonEC2FullAccess')
    'arn:aws:iam::aws:policy/AmazonEC2FullAccess'
    >>> get_policy_arn('RHEDcloudManagementSubnetPolicy')
    'arn:aws:iam::935483523827:policy/rhedcloud/RHEDcloudManagementSubnetPolicy'

    """

    return get_arn('policy', policy, account)


def get_role_arn(role, account=None):
    """Get a full ARN for the specified `role`.

    :param str role:
        IAM Role name.
    :param str account: (optional)
        AWS account number. If omitted, the account number will be determined
        automatically.

    :returns:
        A string.

    Usage:

    >>> get_role_arn('arn:aws:iam::99999:role/RHEDcloudAdministratorRole')
    'arn:aws:iam::99999:role/RHEDcloudAdministratorRole'
    >>> get_role_arn('RHEDcloudAuditorRole')
    'arn:aws:iam::935483523827:role/rhedcloud/RHEDcloudAuditorRole'
    >>> get_role_arn('AWSServiceRoleForECS')
    'arn:aws:iam::aws:role/AWSServiceRoleForECS'

    """

    return get_arn('role', role, account)


def get_arn(resource_type, name, account=None):
    """Get a full ARN for the `resource_type` having the specified `name`.

    If `name` does not appear to be a full ARN, the full ARN will be determined
    based on whether `RHEDcloud` appears in the name. If `RHEDcloud` is not
    present, the object is assumed to be a built-in AWS object. Otherwise, it
    is assumed to be created within the current AWS account.

    :param str resource_type:
        Type of AWS resource, such as `role` or `policy`.
    :param str name:
        AWS resource name.
    :param str account: (optional)
        AWS account number. If omitted, the account number will be determined
        automatically.

    :returns:
        A string.

    Usage:

    >>> get_arn('policy', 'arn:aws:iam::99999:policy/RHEDcloudManagementSubnetPolicy')
    'arn:aws:iam::99999:policy/RHEDcloudManagementSubnetPolicy'
    >>> get_arn('policy', 'AmazonEC2FullAccess')
    'arn:aws:iam::aws:policy/AmazonEC2FullAccess'
    >>> get_arn('policy', 'RHEDcloudManagementSubnetPolicy')
    'arn:aws:iam::935483523827:policy/rhedcloud/RHEDcloudManagementSubnetPolicy'

    """

    arn = name
    pattern = 'arn:aws:iam::{}:{}/{}'
    if not name.startswith('arn:aws'):
        if 'rhedcloud' in name.lower():
            if account is None:
                account = get_account_number()

            if 'rhedcloud/' not in name:
                resource_type = '{}/rhedcloud'.format(resource_type)
        else:
            account = 'aws'

        arn = pattern.format(account, resource_type.lower(), name)

    return arn


@contextmanager
def attach_policy(obj, *policies):
    """Context manager that attaches a Policy with the specified `policy_arn`
    to the specified IAM resource and detaches the same policy when the block
    finishes.

    :param IAM.Resource obj:
        The resource which will have the specified policy attached.
    :param str policies:
        One or more policy names or ARNs to attach to `obj`.

    """

    policy_arns = [get_policy_arn(p) for p in policies]

    try:
        for arn in policy_arns:
            obj.attach_policy(PolicyArn=arn)

        yield
    except Exception:
        raise
    finally:
        for arn in policy_arns:
            ignore_errors(obj.detach_policy)(PolicyArn=arn)


@contextmanager
@catch(ClientError, ValueError, reraise=True, msg="Failed to create test user with requested role\n{ex}")
@aws_resource("iam")
def create_test_user_with_role(prefix, role_name, policy_name=None, *, iam):
    """Create test user with the inline policy of the specified role.

    This is a context manager which attempts to remove the created test user
    upon completion.

    :param str prefix:
        The prefix to assign to the new IAM user name.
    :param str role_name:
        Name of the AWS role to apply to the created user.
    :param str policy_name: (optional)
        Name of the AWS role policy to apply to the created user.

    :yields:
        The temporary IAM.User resource.

    """

    with new_test_user(prefix, iam=iam) as user:
        with user_policy(user.user_name, role_name, policy_name, iam=iam.meta.client):
            yield user


@catch(ClientError, reraise=True, msg="Unexpected error when getting role inline policy\n{ex}")
@aws_resource("iam")
def get_role_policy_doc(role_name, *, iam):
    """Get inline policy document of the specified role.

    :param str role_name:
        Name of the AWS role policy to apply to the created user.

    :returns:
        A policy document as a dictionary.

    """

    role = iam.Role(role_name)
    total = 0
    doc = {}

    for policy in role.policies.all():
        if policy.name == 'root':
            doc = policy.policy_document

        total += 1

    assert total == 1, 'Incorrect number of policies ({})'.format(total)

    return doc


@aws_client('s3')
def wait_for_bucket_exists(bucketName, *, s3):
    s3_bucket_exists_waiter = s3.get_waiter('bucket_exists')
    s3_bucket_exists_waiter.wait(Bucket=bucketName)


@aws_client('s3')
def wait_for_bucket_not_exists(bucketName, *, s3):
    s3_bucket_not_exists_waiter = s3.get_waiter('bucket_not_exists')
    s3_bucket_not_exists_waiter.wait(Bucket=bucketName)


@aws_client('s3')
def add_aes256_encryption(bucketName, *, s3):
    s3.put_bucket_encryption(
        Bucket=bucketName,
        ServerSideEncryptionConfiguration=dict(
            Rules=[dict(
                ApplyServerSideEncryptionByDefault=dict(
                    SSEAlgorithm='AES256',
                ),
            )],
        ),
    )


@aws_resource('s3')
def create_bucket(name, wait=True, addSSE=True, *, s3, **kwargs):
    """Create a new S3 bucket.

    :param str name:
        Name for the new S3 bucket.
    :param bool wait: (optional)
        Whether to wait for the bucket to be created.
    :param bool addSSE: (optional)
        Whether to add SSE(using AES256) to the bucket

    :returns:
        An S3.Bucket resource.

    """

    kwargs['Bucket'] = name
    bucket = s3.create_bucket(**kwargs)

    if wait:
        bucket.wait_until_exists()

    if addSSE:
        add_aes256_encryption(name, s3=s3.meta.client)

    print('Created S3 bucket: {}'.format(name))
    return bucket


@aws_resource('s3')
def delete_bucket(bucket_name, wait=True, *, s3):
    """Removes all objects from an S3 bucket and deletes the bucket.

    :param str bucket_name:
        The (short) name of the bucket to delete
    :param bool wait: (optional)
        True (default) will use waiter, False returns immediately (in which
        case S3 might not be consistent)

    :returns:
        None if the bucket existed and was successfully deleted, string value
        'BucketNotFound' otherwise

    """

    if not bucket_exists(bucket_name, s3=s3.meta.client):
        return 'BucketNotFound'

    try:
        empty_bucket(bucket_name, s3=s3.meta.client)
    finally:
        bucket = s3.Bucket(bucket_name)
        bucket.delete()

        if wait:
            print("Waiting for bucket {} to be deleted".format(bucket_name))
            bucket.wait_until_not_exists()


@aws_client('s3')
def empty_bucket(bucketName, wait=True, *, s3):
    '''
        removes all objects from an s3 bucket
        param bucketName - string value, the (short) name of the bucket to empty
        param wait - boolean value, True(default) will use waiter, False returns immediately (in which case S3 might not be consistent)
        returns None if the bucket existed and all contents were successfully deleted, string value 'BucketNotFound' otherwise
        raises ClientError
    '''

    if not bucket_exists(bucketName, s3=s3):
        return 'BucketNotFound'

    waiter = s3.get_waiter('object_not_exists')

    while True:
        response = s3.list_object_versions(Bucket=bucketName)
        if not (response.get('DeleteMarkers') or response.get('Versions')):
            # short circuit when there are no more objects in the bucket
            break

        for key in ('DeleteMarkers', 'Versions'):
            if key not in response:
                continue

            objs = [{
                'Key': obj['Key'],
                'VersionId': obj['VersionId'],
            } for obj in response[key]]

            print('Deleting {} {} objects from {}'.format(len(objs), key, bucketName))
            s3.delete_objects(
                Bucket=bucketName,
                Delete={'Objects': objs},
            )

            print('Waiting for objects to be deleted')
            for obj in objs:
                waiter.wait(Bucket=bucketName, **obj)


@aws_client('s3')
def bucket_exists(bucketName, *, s3):
    """Queries AWS for an S3 bucket.

    :param str bucketName:
        The (short) name of the bucket to query.

    :returns:
        True if the bucket exists or False if it does not.

    :raises ClientError:
        For all codes other than 404

    """

    try:
        s3.list_objects(Bucket=bucketName, MaxKeys=0)
        return True
    except ClientError as e:
        if not has_status(e.response, 404):
            print("Unexpected error: %s" % e)
            raise e

    return False


def bucket_is_empty(bucketName):
    '''Synonym for is_bucket_empty(bucketName).'''

    return is_bucket_empty(bucketName)


@aws_client('s3')
def is_bucket_empty(bucket_name, *, s3):
    """Queries AWS for an S3 bucket.

    :param str bucket_name:
        The (short) name of the bucket to query.

    :returns:
        * True if the bucket DOES NOT contain objects
        * False if the bucket DOES contain objects
        * None if the bucket does not exist

    """

    if not bucket_exists(bucket_name, s3=s3):
        return None

    return 'Contents' not in s3.list_objects(Bucket=bucket_name)


@aws_client('s3')
def find_objects_in_bucket(bucket_name, *object_names, s3):
    """Search for the specified `object_names` in the S3 bucket with the
    specified `bucket_name`.

    :param str bucket_name:
        Name of the S3 bucket to query.
    :param str object_names:
        One or more object names to search for in the specified S3 bucket.

    :returns:
        A dictionary whose keys are the specified object names and values are
        whether each respective object was found in the bucket. If the
        specified bucket does not exist, the dicionary will have all False
        values.

    Usage:

    >>> bucket = create_bucket('foib-test')
    Created S3 bucket: foib-test
    >>> find_objects_in_bucket(bucket.name, 'upload_file.rtf')
    {'upload_file.rtf': False}
    >>> bucket.upload_file('./tests/upload_file.rtf', 'upload_file.rtf')
    >>> find_objects_in_bucket(bucket.name, 'upload_file.rtf')
    {'upload_file.rtf': True}
    >>> delete_bucket(bucket.name)
    Deleting 1 Versions objects from foib-test
    Waiting for objects to be deleted

    """

    results = dict.fromkeys(object_names, False)

    if bucket_exists(bucket_name, s3=s3):
        objs = s3.list_objects(Bucket=bucket_name).get('Contents', [])
        for obj in objs:
            for key in results:
                if obj['Key'] == key:
                    results[key] = True

    return results


@aws_client('iam')
def create_session_for_test_user(username, *, iam):
    """Creates an access key for an IAM User.

    :param str username:
        Name of an IAM User.

    :returns:
        A boto3 Session using the specified user's credentials.

    """

    access_key_dict = get_access_key(username, iam=iam)

    session = boto3.session.Session(
        aws_access_key_id=access_key_dict['AccessKeyId'],
        aws_secret_access_key=access_key_dict['SecretAccessKey'],
    )

    # must delay as the create op is eventually consistent: see https://github.com/hashicorp/vault/issues/687
    print("AWS Support Case ID: 4855716111: AccessKeyId==", access_key_dict['AccessKeyId'])
    _wait_for_consistency(session)

    return session


@aws_client('iam')
def delete_test_user(userName, *, iam):
    '''
        Deletes an iam user (if it exists)
        returns None
        raises ClientError
    '''
    try:
        # must delete access key(s) before user can be deleted
        response = iam.list_access_keys(UserName=userName)
        accessKeysAry = response['AccessKeyMetadata']
        for accessKey in accessKeysAry:
            iam.delete_access_key(UserName=userName, AccessKeyId=accessKey['AccessKeyId'])

    except ClientError as e:
        if e.response['Error']['Code'] == 'NoSuchEntity':
            return
        else:
            print("Unexpected error: %s" % e)
            raise e

    try:
        # must detach policy(ies) before user can be deleted
        response = iam.list_attached_user_policies(UserName=userName)
        policies = response['AttachedPolicies']
        for policy in policies:
            iam.detach_user_policy(UserName=userName, PolicyArn=policy['PolicyArn'])

    except ClientError as e:
        if e.response['Error']['Code'] == 'NoSuchEntity':
            return
        else:
            print("Unexpected error: %s" % e)
            raise e

    iam.delete_user(UserName=userName)


@aws_client('iam')
def create_test_user(prefix=None, *, iam):
    """Creates an iam user using a randomized name.

    :param str prefix:
        The prefix to assign to the new IAM user name.

    :returns:
        The new user as a boto3 User dictionary object.

    :raises ClientError:

    """

    if prefix is None:
        prefix = 'testuser'

    username = '{}-{}'.format(prefix, uuid.uuid4())[:64]
    res = iam.create_user(UserName=username)

    assert 'User' in res, 'Unexpected response: {}'.format(res)
    user = res['User']

    if IAM_DELAY:
        time.sleep(IAM_DELAY)

    print('Created test user: {}'.format(username))

    return user


@aws_client('iam')
def attach_policy_to_user(userName, policyArn, *, iam):
    '''
        Attaches a managed policy to a user
        returns None
        param userName - string value, the name of an iam user, e.g. the 'UserName' attribute of a boto3 User dict
        param policyArn - string value, the ARN of the managed policy
    '''
    iam.attach_user_policy(UserName=userName, PolicyArn=policyArn)


def create_test_policy_using_client(iamClient):
    """Creates an iam managed policy using a supplied context.

    :param iamClient:
        Boto3 client object, an instance of the Client class that will be used
        create the policy.

    :returns:
        The new policy as a boto3 Policy dictionary object

    Usage:

    .. code-block:: python

        #example:
        user = createTestUser()
        session = createSessionForTestUser(user['UserName'])
        policy = create_test_policy_using_client(session.client('iam'))


        Policy object dict:
        {
            'Policy': {
                'PolicyName': 'string',
                'PolicyId': 'string',
                'Arn': 'string',
                'Path': 'string',
                'DefaultVersionId': 'string',
                'AttachmentCount': 123,
                'IsAttachable': True|False,
                'Description': 'string',
                'CreateDate': datetime(2015, 1, 1),
                'UpdateDate': datetime(2015, 1, 1)
            }
        }

    """

    policyName = "testpolicy-{}".format(get_uuid())
    policyDocument = """{
                         "Version": "2012-10-17",
                         "Statement": [
                           {
                             "Effect": "Allow",
                             "Action": "*",
                             "Resource": "*"
                           }
                         ]
                       }"""
    createPolicyResp = iamClient.create_policy(
        PolicyName=policyName, PolicyDocument=policyDocument, Description="Created by CFN policy test script"
    )
    return createPolicyResp


def delete_test_policy_using_client(iamClient, policyArn):
    """Deletes an IAM managed policy using a supplied client.

    :param IAM.Client iamClient:
        An instance of the Client class that will be used delete the policy.
    :param str policyArn:
        ARN of the policy to delete.

    :raises ClientError:
        When the policy cannot be deleted.

    """

    print('Deleting IAM Policy: {}'.format(policyArn))
    iamClient.delete_policy(PolicyArn=policyArn)


def get_account_number():
    """Returns the AWS account number of the default boto3 session"""

    return get_account_info()["Account"]


@aws_client('sts')
def get_account_info(*, sts):
    """Obtains the AWS identity information of the default boto3 session client

    :returns:
        A boto3 CallerId dict

    .. code-block:: python

        {
            'UserId': 'string',
            'Account': 'string',
            'Arn': 'string'
        }

    """

    return sts.get_caller_identity()


def print_account_info():
    """Pretty print the the AWS identity information of the default boto3
    session client.

    :returns:
        A boto3 CallerId dict

    .. code-block:: python

        {
            'UserId': 'string',
            'Account': 'string',
            'Arn': 'string'
        }

    """

    account_id = get_account_info()
    pp = pprint.PrettyPrinter(indent=4, width=160)
    pp.pprint(account_id)
    return account_id


def _wait_for_consistency(session, delay=IAM_DELAY):
    """This is a private function to this module that loops until an sts
    get_caller_identity command succeeds or a limit on the number of attempts
    is reached.

    It is used to delay the processing of the script while a new access key is
    in transit to STS see createSessionForTestUser for more context.

    :returns:
        None

    """

    sts = session.client('sts')

    return retry(
        sts.get_caller_identity,
        msg='Checking STS consistency...',
        delay=delay,
    )


@catch(ClientError, return_type=list, msg='Failed to create boto3 resource:\n{ex}')
@aws_resource('ec2')
@lru_cache()
def get_vpc_ids(rhedcloud_vpc_type, *, ec2):
    """Obtains a list of all VPC resource IDs matching the specified RHEDcloud
    Vpc type.

    :param str rhedcloud_vpc_type:
        Must be `1`, `2`, 'Firewall' or `All`.

    :returns:
        A list.

    :raises ValueError:
        When the specified VPC type is incorrect.

    Example list:
        ['vpc-xxxxxxxx', 'vpc-xxxxxxxx', etc...]

    Example loop that prints out all of the Type1 VPC IDs in an account:

        for vpc in get_vpc_ids('1'):
            print(vpc)

    """

    if rhedcloud_vpc_type not in ('1', '2', 'Firewall', 'All'):
        raise ValueError("rhedcloud_vpc_type must be '1', '2', 'Firewall' or 'All'")

    if rhedcloud_vpc_type in ('1', '2', 'Firewall'):
        filters = [{
            'Name': 'tag:{}'.format(RHEDCLOUD_VPC_TYPE_TAG),
            'Values': [rhedcloud_vpc_type],
        }]
        ec2Vpcs = list(ec2.vpcs.filter(Filters=filters))
    elif rhedcloud_vpc_type == 'All':
        ec2Vpcs = list(ec2.vpcs.all())

    return [vpc.id for vpc in ec2Vpcs]


@catch(ClientError, return_value=False, msg='Failed to create boto3 resource:\n{ex}')
@aws_client('ec2')
def get_vpc_cidr(vpc_id, *, ec2):
    '''
    Obtains the CIDR space assigned to the VPC.

    Returns a the CIDR space as a string

    Example string:
    '10.64.60.0/23'
    '''

    response = ec2.describe_vpcs(VpcIds=[vpc_id])
    return response['Vpcs'][0]['CidrBlock']


@catch(ClientError, return_value=False, msg='Failed to create boto3 resource:\n{ex}')
@aws_client('ec2')
def check_subnet_state(vpc_id, subnet_name, *, ec2):
    """Verify the subnet state is available.  Using the VPC Type and subnet
    name this test will loop through all of the VPCs of the specified Type,
    identify the subnets matching the specified name, and check its state.

    :param str vpc_id:
        AWS VPC ID.
    :param str subnet_name:
        Name of the subnet to verify.

    :returns:
        True if the state is 'available'.

    """

    subnet = ec2.describe_subnets(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': subnet_name,
    }))

    return subnet['Subnets'][0]['State'] == 'available'


@catch(ClientError, msg='Failed to create boto3 resource: {ex}')
@aws_client('ec2')
def check_subnet_cidr(vpc_id, subnet_name, *, ec2):
    '''
        Verify the named subnet state is available for all VPCs of the named type.
        Using the VPC Type and subnet name this test will loop through all of the
        VPCs of the specified Type, identify the subnets matching the specified name,
        and check its state.

        Returns true if the state is 'available'.

    '''

    vpcCidr = get_vpc_cidr(vpc_id)
    print(vpcCidr)
    vpcCidrNetwork = ipaddress.ip_network(vpcCidr)

    subnet = ec2.describe_subnets(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': subnet_name,
    }))
    subnetCidr = subnet['Subnets'][0]['CidrBlock']
    print(subnetCidr)
    subnetCidrNetwork = ipaddress.ip_network(subnetCidr)

    return subnetCidrNetwork.overlaps(vpcCidrNetwork)


@catch(ClientError, msg='Failed to create boto3 resource: {ex}')
@aws_client('ec2')
def check_subnet_az(vpc_id, subnet_name, partner_subnet_name, region, *, ec2):
    """Using the VPC Type and subnet name this test will loop through all of
    the VPCs of the specified RHEDcloudVpcType, identify the subnets matching
    the specified name, and verify:

    #. the availability zone is in the specified AWS region,
    #. specified subnet's AZ is different from the subnet's partner AZ.

    Returns true if the subnet is in the specified Region and is in a different
    AZ from the partner subnet.

    """

    subnet = ec2.describe_subnets(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': subnet_name,
    }))
    subnetAz = subnet['Subnets'][0]['AvailabilityZone']

    partnerSubnet = ec2.describe_subnets(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': partner_subnet_name,
    }))
    partnerAz = partnerSubnet['Subnets'][0]['AvailabilityZone']

    if (region not in subnetAz) or (subnetAz == partnerAz):
        return False

    return True


@catch(ClientError, msg='Failed to create boto3 resource: {ex}')
@aws_client('ec2')
def check_subnet_name(vpc_id, subnet_name, *, ec2):
    '''
        Verify the deployed subnet's tag name matches the expected name.
        Using the VPC Type and Subnet name this test will loop through all of the
        VPCs of the specified Type, identify all of the subnets, and look for one
        with the correct name.

        Returns true if the subnet name matches the expected name.

    '''

    subnets = ec2.describe_subnets(Filters=dict_to_filters({
        'vpc-id': vpc_id,
    }))['Subnets']

    for subnet in subnets:
        for tag in subnet['Tags']:
            if tag['Key'] == 'Name' and tag['Value'] == subnet_name:
                return True

    return False


@aws_client('ec2')
def get_vpc_route_table(vpc_id, route_table_name, *, ec2):
    """Return information about the specified route table within the specified VPC.

    :param str vpc_id:
        AWS VPC ID.
    :param str route_table_name:
        Name of a route table.

    :returns:
        A dictionary.

    """

    filters = dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': route_table_name,
    })

    tables = ec2.describe_route_tables(Filters=filters)['RouteTables']
    assert len(tables) > 0, 'No route table called "{}" in VPC {}'.format(route_table_name, vpc_id)

    return tables[0]


@catch(ClientError, msg='Failed to create boto3 resource: {ex}')
@aws_client('ec2')
def check_route_table(vpc_id, route_table_name, *, ec2):
    """Check the routing table for the specified `route_table_name` within the
    specified `vpc_id`.

    :param str vpc_id:
        AWS VPC ID.
    :param str route_table_name:
        Name of the route table to check.

    :returns:
        False if the default gateway is inactive, True otherwise.

    """

    route_table = get_vpc_route_table(vpc_id, route_table_name, ec2=ec2)
    for route in route_table['Routes']:
        if 'DestinationCidrBlock' not in route:
            continue

        if route['DestinationCidrBlock'] == '0.0.0.0/0' and route['State'] != 'active':
            return False

    return True


@catch(ClientError, IndexError, msg='Failed to retrieve subnet ID:\n{ex}')
@aws_client('ec2')
def get_subnet_id(vpc_id, subnet_name, *, ec2):
    """Given a vpc-id and subnet name, this function will return the subnet-id
    for that subnet.

    :param str vpc_id:
        ID of a VPC.
    :param str subnet_name:
        Name of a subnet with the the specified VPC.

    :returns:
        A string.

    """

    filters = [{
        'Name': 'vpc-id',
        'Values': [vpc_id]
    }, {
        'Name': 'tag:Name',
        'Values': [subnet_name]
    }]

    subnets = ec2.describe_subnets(Filters=filters)
    subnetid = subnets['Subnets'][0]['SubnetId']

    return subnetid


@catch(ClientError, return_value='',
       msg='Unexpected error when getting subnet name: {ex}')
@aws_resource('ec2')
def get_subnet_name(subnet_id, *, ec2):
    '''Getting a subnet name'''

    subnet_name = ''
    for tag in ec2.Subnet(subnet_id).tags:
        if tag['Key'] == 'Name':
            subnet_name = tag['Value']
            break

    return subnet_name


@contextmanager
@aws_resource('s3')
def new_bucket(prefix, wait=True, *, s3, **kwargs):
    """Context manager to create a new S3 bucket and remove it when the block
    finishes.

    :param str prefix:
        Prefix for the randomly-generated bucket name.
    :param bool wait: (optional)
        Whether to wait for the bucket to be created.

    :yields:
        The created S3.Bucket resource.

    """

    bucket_name = '{}-{}'.format(prefix, get_uuid()).replace('_', '-')[:63].lower()
    bucket = None

    try:
        bucket = create_bucket(bucket_name, wait=wait, s3=s3, **kwargs)
        yield bucket
    except Exception:
        raise
    finally:
        if bucket is not None:
            print('Removing S3 bucket: {}'.format(bucket_name))
            delete_bucket(bucket_name, wait=False, s3=s3)


@contextmanager
def new_ec2_instance(prefix=None, **kwargs):
    """Context manager to create a new EC2 instance and clean it up when the
    block finishes.

    If a `role` keyword argument is specified, an Instance Profile will be
    created and the specified role will be attached to it. The Instance Profile
    will then be attached to the created EC2 instance.

    :param str prefix: (optional)
        Prefix to give the EC2 instance when generating a random name.

    :yields:
        An EC2.Instance resource.

    """

    if prefix is None:
        prefix = 'test'

    name = '{}-{}'.format(prefix, get_uuid())
    instance = None
    role = kwargs.get('role')

    try:
        if role is not None:
            # automatically create an instance profile when a role is specified
            with new_instance_profile(name, role=role) as profile:
                kwargs.update({
                    'profile': profile,
                })

                instance = create_ec2_instance(name, **kwargs)
                yield instance
        else:
            instance = create_ec2_instance(name, **kwargs)
            yield instance
    except Exception:
        raise
    finally:
        if instance is not None:
            delete_ec2_instance(instance, **kwargs)


@aws_client('ec2')
def get_default_security_group(vpc_id, *, ec2):
    """Return the default security group for the specified VPC.

    :param str vpc_id:
        ID of the VPC.

    :returns:
        A dictionary.

    """

    res = ec2.describe_security_groups(
        Filters=[{
            'Name': 'vpc-id',
            'Values': [vpc_id],
        }],
    )
    assert 'SecurityGroups' in res, 'unexpected response: {}'.format(res)
    matching = [sg for sg in res['SecurityGroups'] if sg['GroupName'] == 'default']
    assert len(matching), 'default security group not found for VPC: {}'.format(vpc_id)

    return matching[0]


@aws_client('ec2')
def reset_security_groups(instance, *, ec2):
    """Remove any custom security groups from the specified EC2 instance.

    :param EC2.Instance instance:
        EC2 Instance resource to modify.

    """

    print('Resetting security groups for instance: {}'.format(instance.instance_id))
    default_sg = get_default_security_group(instance.vpc_id)['GroupId']

    instance.modify_attribute(
        Groups=[default_sg],
    )


@aws_client('ssm')
def wait_for_managed_instance(instance_id, timeout=300, *, ssm):
    """Wait for the EC2 instnace having the specified `instance_id` to appear
    as a managed instance in Systems Manager.

    :param str instance_id:
        The ID of an EC2 instance that is expected to appear in Systems
        Manager.
    :param int timeout: (optional)
        Number of seconds to wait before giving up.

    :raises ValueError:
        When the specified EC2 instance does not appear within the specified
        timeout.

    """

    max_attempts = timeout // 10
    filters = dict_to_filters({
        'InstanceIds': [instance_id],
    }, key_name='Key')

    res = retry(
        ssm.describe_instance_information,
        kwargs=dict(Filters=filters),
        msg='Looking for managed instance: {}'.format(instance_id),
        show=True,
        pred=lambda exc: 'AccessDenied' not in str(exc),
        until=lambda s: len(s['InstanceInformationList']) == 1,
        delay=10,
        max_attempts=max_attempts)

    assert len(res['InstanceInformationList']) == 1, 'Instance {} not found after {} seconds'.format(instance_id, timeout)


@contextmanager
def new_ssm_role(prefix=None):
    """Context manager to create a new IAM Role for Systems Manager managed
    instances and clean up the role upon completion.

    :param str prefix: (optional)
        Prefix to use when randomly generating a role name.

    """

    if prefix is None:
        prefix = 'TestSSMRole'

    name = '{}-{}'.format(prefix, get_uuid())
    role = None

    try:
        role = create_ssm_role(name)
        yield role
    except Exception:
        raise
    finally:
        if role is not None:
            role.cleanup()
            role.delete()


@contextmanager
@aws_resource('iam')
def new_role(prefix, service, *policies, delay=10, exact_name=False, iam, **kwargs):
    """Create a new temporary role with the given `statement`.

    :param str prefix:
        Prefix for the randomly-generated bucket name.
    :param str service:
        Service to setup in the Role's trust relationships.
    :param str policies:
        One or more policies to attach to the role.
    :param int delay: (optional)
        Number of seconds to wait before yielding the role for use. Default is
        10 seconds.

    :yields:
        An IAM.Role resource.

    """

    if exact_name:
        name = prefix
    else:
        name = '{}-role-{}'.format(prefix, get_uuid())[:64]

    kwargs['service'] = service
    role = None

    try:
        role = create_role(name, iam=iam, **kwargs)
        print('Created role: {}'.format(role.role_name))

        with attach_policy(role, *policies):
            # give some time for the role to become "assumable"
            time.sleep(delay)

            yield role
    finally:
        if role is not None:
            delete_role(role=role)


@aws_resource('iam')
def get_or_create_role(name, *policies, service=None, delay=10, iam, **kwargs):
    """Get or create a new IAM Role.

    :param str name:
        Name of the IAM Role to get or create.
    :param str policies:
        One or more IAM Policies to attach to the role.
    :param str service:
        The AWS Service to trust.
    :param int delay: (optional)
        Number of seconds to wait before attaching any policies to the role
        when it's created.

    :returns:
        An IAM Role resource.

    """

    role = None

    try:
        role = iam.Role(name)
        role.load()

        print('Found existing role: {}'.format(role.arn))
    except ClientError:
        kwargs.update({
            'service': service,
            'iam': iam,
        })

        role = create_role(name, **kwargs)
        for policy in policies:
            arn = get_policy_arn(policy)
            print('Attaching policy {} to role {}'.format(arn, role.arn))
            role.attach_policy(PolicyArn=arn)

        time.sleep(delay)

    return role


@aws_resource('iam')
def create_role(
    name, service='ec2', statement=None, *, service_role=False, rhedcloud_role=False, iam,
    **kwargs
):
    """Create a new service IAM Role.

    :param str name:
        Name of the Role.
    :param str/list(str) service: (optional)
        Service or list of services to allow access to. Defaults to `ec2`.
    :param list statement: (optional)
        Role details. Overrides `service` if specified.
    :param bool service_role: (optional)
        Whether to create a service role. Default is ``False``.
    :param bool rhedcloud_role: (optional)
        Whether to add /rhedcloud/ path to role. Default is ``False``.

    :returns:
        An IAM Role resource.

    """

    if not isinstance(service, (list, tuple)):
        service = [service]

    if statement is None:
        statement = [{
            'Effect': 'Allow',
            'Action': 'sts:AssumeRole',
            'Principal': {
                'Service': ['{}.amazonaws.com'.format(svc) for svc in service],
            },
        }]

    doc = json.dumps({
        'Version': '2012-10-17',
        'Statement': statement,
    }, indent=2)
    print('Role policy document:\n{}'.format(doc))

    kwargs.update(dict(
        RoleName=name,
        AssumeRolePolicyDocument=doc,
    ))

    if service_role:
        kwargs['Path'] = '/service-role/'

    if rhedcloud_role:
        kwargs['Path'] = '/rhedcloud/'

    role = iam.create_role(**kwargs)
    print('Created role: {}'.format(role.arn))

    return role


@contextmanager
@aws_resource('iam')
def new_policy(prefix, statement, descr=None, *, iam):
    name = '{}-policy-{}'.format(prefix, get_uuid())
    policy = None

    try:
        policy = create_policy(name, statement, descr, iam=iam)
        print('Created policy: {}'.format(policy.policy_name))

        yield policy
    except Exception:
        raise
    finally:
        if policy is not None:
            # short circuit if the policy was never created, as there's no
            # teardown to be done
            # raise StopIteration

            for grp in policy.attached_groups.all():
                print('Removing group "{}" from policy: {}'.format(grp.group_name, policy.policy_name))
                policy.detach_group(GroupName=grp.group_name)

            for role in policy.attached_roles.all():
                print('Removing role "{}" from policy: {}'.format(role.role_name, policy.policy_name))
                policy.detach_role(RoleName=role.role_name)

            for user in policy.attached_users.all():
                print('Removing user "{}" from policy: {}'.format(user.user_name, policy.policy_name))
                policy.detach_user(UserName=user.user_name)

            print('Removing policy: {}'.format(policy.policy_name))
            policy.delete()


@aws_resource('iam')
def create_policy(name, statement, path=None, descr=None, *, iam):
    """Create a new IAM policy.

    :param str name:
        Name of the IAM Policy to create.
    :param list statement:
        Policy statement.
    """

    doc = json.dumps({
        'Version': '2012-10-17',
        'Statement': statement,
    }, indent=2)
    print('Policy document:\n{}'.format(doc))

    if path is None:
        path_name = "/"
    else:
        path_name = path

    return iam.create_policy(
        PolicyName=name,
        Description=descr or 'Sample policy created for testing',
        PolicyDocument=doc,
        Path=path_name,
    )


@aws_resource('iam')
def delete_role(*, role=None, role_name=None, iam):
    """Delete a IAM Role.

    :param IAM.Role role:
        The IAM.Role to delete.
    :param str role_name:
        Name of the IAM Role to delete.

    Usage:

    Either `role` or `role_name` must be specified by keyword, otherwise a
    `TypeError` will be raised:

    >>> delete_role('FakeRoleName')
    Traceback (most recent call last):
    ...
    TypeError: delete_role() takes 0 positional arguments but 1 was given
    >>> delete_role(role={}, role_name='FakeRoleName')
    Traceback (most recent call last):
    ...
    TypeError: must specify one of 'role' or 'role_name' but not both

    If you already have the IAM.Role resource in scope:

    >>> iam = boto3.resource('iam')
    >>> r = iam.Role('FakeRoleName')
    >>> delete_role(role=r)

    Alternatively, a role name may be specified:

    >>> delete_role(role_name='FakeRoleName')

    """

    if not ((role is None) ^ (role_name is None)):
        raise TypeError("must specify one of 'role' or 'role_name' but not both")

    if role is None:
        role = iam.Role(role_name)

    for policy in role.attached_policies.all():
        role.detach_policy(PolicyArn=policy.arn)

    print('Removing role: {}'.format(role.role_name))
    role.delete()


@aws_client('iam')
def create_ssm_role(name, *, iam):
    """Create a new IAM Role with access to Systems Manager.

    :param str name:
        Name of the new Role.

    :returns:
        The created IAM.Role resource.

    """

    arn = 'arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM'

    create_role(name, service='ec2')
    role = boto3.resource('iam').Role(name)
    role.attach_policy(PolicyArn=arn)
    role.cleanup = lambda: role.detach_policy(PolicyArn=arn)

    return role


@contextmanager
def new_instance_profile(name, role=None, wait=True):
    """Context manager to create a new Instance Profile and remove it when the
    block finishes.

    :param str name:
        Name of the Instance Profile.
    :param IAM.Role role: (optional)
        A Role to attach to the Instance Profile.
    :param bool wait: (optional)
        Wait for the Instance Profile to be ready.

    :yields:
        An IAM.InstanceProfile resource.

    """

    instance_profile = None

    try:
        instance_profile = create_instance_profile(name, role=role, wait=wait)
        yield instance_profile
    finally:
        if instance_profile is not None:
            if role is not None:
                instance_profile.remove_role(RoleName=role.name)

            print('Deleting instance profile: {}'.format(instance_profile.instance_profile_name))
            instance_profile.delete()


@aws_client('iam')
def create_instance_profile(name, role=None, wait=True, *, iam):
    """Create a new Instance Profile with the specified `name` and optional
    `role`.

    :param str name:
        Name of the Instance Profile.
    :param IAM.Role role: (optional)
        A Role to attach to the Instance Profile.
    :param bool wait: (optional)
        Wait for the Instance Profile to be ready.

    :returns:
        An IAM.InstanceProfile resource.

    """

    instance_profile = None

    res = iam.create_instance_profile(InstanceProfileName=name)
    print('Created instance profile: {}'.format(res['InstanceProfile']['InstanceProfileName']))

    instance_profile = boto3.resource('iam').InstanceProfile(name)

    if role is not None:
        instance_profile.add_role(RoleName=role.name)

    if wait:
        # give some time for the instance profile to be created completely
        time.sleep(30)

        # TODO: figure out why this doesn't work
        # waiter = iam.get_waiter('instance_profile_exists')
        # waiter.wait(InstanceProfileName=name)

    return instance_profile


def create_ec2_instance(name, ami=None, size=None, key_name=None, role=None, profile=None, session=None, **kwargs):
    """Create a new EC2 instance.

    :param str name:
        Name of the new EC2 instance.
    :param str ami: (optional)
        ID of the AMI to use when creating the EC2 instance. Defaults to a
        recent version of Amazon Linux.
    :param str size: (optional)
        Instance type to create. Defaults to `t2.nano`.
    :param str key_name: (optional)
        Name of an SSH keypair to assign to the instance.
    :param IAM.Role role: (optional)
        Role to assign to the instance.
    :param IAM.InstanceProfile role: (optional)
        Instance Profile to assign to the instance.
    :param boto3.session.Session Session: (optional)
        A specific Session to use when interacting with the API. If omitted,
        the current session will be used.

    :returns:
        An EC2.Instance object for the created instance.

    """

    if session is None:
        session = boto3

    params = dict(
        ImageId=ami or 'ami-6057e21a',
        InstanceType=size or 't2.nano',
        MinCount=1, MaxCount=1,
        TagSpecifications=[{
            'ResourceType': 'instance',
            'Tags': [{
                'Key': 'Name',
                'Value': name,
            }]
        }],
        UserData="""#!/bin/bash
date > /tmp/demo
        """,
    )
    params.update(kwargs)

    if key_name is not None:
        params['KeyName'] = key_name

    if profile is not None:
        params['IamInstanceProfile'] = {
            'Arn': profile.arn,
        }

    ec2 = session.resource('ec2')
    instances = ec2.create_instances(**params)

    assert len(instances) == 1, 'EC2 instance not created'
    instance = instances[0]
    print('Created EC2 instance: {} ({})'.format(name, instance.instance_id))

    return instance


def delete_ec2_instance(instance, **kwargs):
    """Delete an EC2 instance.

    :param EC2.Instance instance:
        An EC2 Instance resource.

    """

    if 'SecurityGroupIds' in kwargs:
        reset_security_groups(instance)

    print('Removing EC2 instance: {}'.format(instance.instance_id))
    instance.terminate()


def get_stack_name_from_vpc(vpc):
    """Return the name of the CloudFormation stack to which `vpc` belongs.

    :param EC2.Vpc vpc:
        A VPC resource.

    :returns:
        A string.

    :raises ValueError:
        If `vpc` does not appear to be associated with any CloudFormation
        stacks.

    """

    for tag in vpc.tags:
        if tag['Key'] == 'aws:cloudformation:stack-name':
            return tag['Value']

    raise ValueError('{} was not created using CloudFormation'.format(vpc.id))


@aws_resource('ec2')
def get_stack_name_from_vpc_id(vpc_id, ec2):
    """Return the name of the CloudFormation stack to which the VPC with the
    specified `vpc_id` belongs.

    :param str vpc_id:
        ID of a VPC.
    :param ec2.ServiceResource ec2: (optional)
        An existing EC2 Service Resource.

    :returns:
        A string.

    :raises ValueError:
        If `vpc` does not appear to be associated with any CloudFormation
        stacks.

    """

    return get_stack_name_from_vpc(ec2.Vpc(vpc_id))


@aws_resource('iam')
def find_policy_for_stack(stack_name, policy_name, *, iam):
    """Return the ARN for a policy by the name of `policy_name` in the
    specified CloudFormation stack.

    If no policy can be found that is specific to the current CloudFormation
    stack, an attempt will be made to use a built-in policy.

    If no matches can be found, the `policy_name` will be returned on the
    assumption that it is already a full ARN.

    :param str stack_name:
        The current CloudFormation stack name.
    :param str policy_name:
        Name of the stack-specific policy to find.

    :returns:
        A string.

    """

    needles = (
        # support VPCs with dynamic policy names
        'policy/rhedcloud/{}-{}'.format(stack_name, policy_name),

        # support for built-in policies
        'policy/{}'.format(policy_name),
    )

    for needle in needles:
        for policy in iam.policies.all():
            if needle in policy.arn:
                return policy.arn

    return policy_name


@catch(ClientError, msg='Failed to create boto3 resource: {ex}')
@aws_client('ec2')
def check_subnet_association(vpc_id, route_table_name, subnet_name, *, ec2):
    print("VPC:", vpc_id)
    match = False
    subnet_id = get_subnet_id(vpc_id, subnet_name)
    print("Subnet:", subnet_id)

    route_table = get_vpc_route_table(vpc_id, route_table_name, ec2=ec2)
    for association in route_table['Associations']:
        if association['SubnetId'] == subnet_id:
            match = True

        print("Assoc[subnetid]:", association['SubnetId'], "cume_match_var:", match)  # !!!

    return match


@aws_client('ec2')
def get_vpc_nacl(vpc_id, name=None, *, ec2):
    """Retrieve a NACL for the specified VPC that has the specified name.

    :param str vpc_id:
        AWS VPC ID.
    :param str name: (optional)
        Name of the NACL. Default is `VPC-to-VPC NACL`.

    :returns:
        A dictionary.

    :raises AssertionError:
        If no NACL with the specified `name` is found in the specified
        `vpc_id`.

    """

    if name is None:
        name = 'VPC-to-VPC NACL'

    nacls = ec2.describe_network_acls(Filters=dict_to_filters({
        'vpc-id': vpc_id,
        'tag:Name': name,
    }))['NetworkAcls']

    assert len(nacls) > 0
    return nacls[0]


@aws_client('ec2')
def get_vpc_nacl_associations(vpc_id, name=None, *, ec2):
    """Return the associations for the named NACL in the specified VPC.

    :param str vpc_id:
        AWS VPC ID.
    :param str name: (optional)
        Name of the NACL. Default is `VPC-to-VPC NACL`.

    :returns:
        A list of dictionaries.

    :raises AssertionError:
        If no NACL with the specified `name` is found in the specified
        `vpc_id`.

    """

    return get_vpc_nacl(vpc_id, name=name, ec2=ec2)['Associations']


@aws_client('ec2')
def get_vpc_nacl_entries(vpc_id, name=None, *, ec2):
    """Return the entries for the named NACL in the specified VPC.

    :param str vpc_id:
        AWS VPC ID.
    :param str name: (optional)
        Name of the NACL. Default is `VPC-to-VPC NACL`.

    :returns:
        A list of dictionaries.

    :raises AssertionError:
        If no NACL with the specified `name` is found in the specified
        `vpc_id`.

    """

    return get_vpc_nacl(vpc_id, name=name, ec2=ec2)['Entries']


@aws_client('ec2')
def check_vpc2vpc_nacl_entry(vpc_id, entry, *, ec2):
    """Compare the specified NACL `entry` with the entries in the NACL for the
    specified VPC.

    :param str vpc_id:
        AWS VPC ID.
    :param dict entry:
        A NACL entry that is expected to be found in the VPC's NACL.

    :returns:
        True if `entry` is found in the VPC's NACL, False otherwise.

    """

    nacl_entries = get_vpc_nacl_entries(vpc_id, ec2=ec2)
    for nacl_entry in nacl_entries:
        if nacl_entry == entry:
            return True

    return False


@aws_client('ec2')
def check_subnet_nacl_association(vpc_id, subnet_name, *, ec2):
    """Check for a subnet association in the specified NACL.

    :param str vpc_id:
        AWS VPC ID.
    :param str subnet_name:
        Name of the subnet that is expected to have an association in the VPC's
        NACL.

    :returns:
        True if the specified subnet has an association in the VPC's NACL,
        False otherwise.

    """

    subnet_id = get_subnet_id(vpc_id, subnet_name)
    for association in get_vpc_nacl_associations(vpc_id, ec2=ec2):
        if association['SubnetId'] == subnet_id:
            return True

    return False


@aws_client('ec2')
def get_flow_log_attr(vpc_id, attr, *, ec2):
    """Retrieve an attribute from a Flow Log.

    :param str vpc_id:
        AWS VPC ID.
    :param str attr:
        Name of the attribute to return.

    """

    flow_logs = ec2.describe_flow_logs(Filters=dict_to_filters({
        'resource-id': vpc_id,
    }))['FlowLogs']

    assert len(flow_logs) > 0, 'No Flow Logs found'
    return flow_logs[0][attr]


@aws_client('ec2')
def check_route_table_subnet_associations(vpc_id, route_table_name, *subnet_names, ec2):
    """Check that the specified `subnet_names` are associated with the
    specified VPC.

    :param str vpc_id:
        AWS VPC ID.
    :param str route_table_name:
        Name of the route table.
    :param str subnet_names:
        One or more subnet names that are expected to be associated with the
        VPC's routing table.

    :raises AssertionError:
        When one or more of the named subnets is not associated with the VPC.

    """

    assoc_count = 0
    expected_count = len(subnet_names)
    found = dict.fromkeys(subnet_names, False)

    route_table = get_vpc_route_table(vpc_id, route_table_name, ec2=ec2)
    for assoc in route_table['Associations']:
        subnet_name = get_subnet_name(assoc['SubnetId'])

        for name in found:
            if name == subnet_name:
                found[name] = True

        assoc_count += 1

    # There must be 2 associations to 2 mgmt subnets.
    assert assoc_count == expected_count, 'No of subnets is not equal to {}'.format(expected_count)

    for subnet, was_found in found.items():
        assert was_found, 'Cannot find association to {}'.format(subnet)


def get_uri_contents(uri, encoding='utf-8', binary=False):
    """Get the contents at a URL with the GET method.

    :param str uri:
        URI to fetch.
    :param str encoding: (optional)
        Encoding.
    :param bool binary: (optional)
        If content returned should be binary.

    :returns:
        Content at URI as a string or byte string.

    """

    req = Request(uri)
    resp = urlopen(req)
    data = resp.read()

    if not binary:
        data = data.decode(encoding, errors='replace')

    return data
