"""
============================================
AWS Identity and Access Management Utilities
============================================

Utilities for AWS IAM.

"""

from .decorators import aws_client


class IAMState:
    """Wrapper around state that may be maintained throughout the duration of a
    pytest session.

    """

    def __init__(self):
        self._access_key_cache = {}

    @aws_client('iam')
    def get_access_key(self, username, *, iam):
        """Retrieve access credentials for the specified user. If no
        credentials have yet been requested for the user in the session, new
        credentials will be created and cached.

        :param str username:
            IAM User name.

        :returns:
            A dictionary.

        """

        data = self._access_key_cache.get(username)
        if not data:
            # create new credentials if we don't have any cached
            print('Creating credentials for: {}'.format(username))
            res = iam.create_access_key(UserName=username)
            assert 'AccessKey' in res, 'Unexpected response: {}'.format(res)

            data = self._access_key_cache[username] = res['AccessKey']
        else:
            print('Using existing credentials for: {}'.format(username))

        return data


_iam_state = IAMState()


get_access_key = _iam_state.get_access_key
