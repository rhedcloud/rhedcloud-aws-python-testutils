"""
=================
AWS IoT Utilities
=================

"""

from contextlib import contextmanager

from .decorators import (
    aws_client,
    ignore_errors,
)
from .utils import (
    make_identifier,
    unexpected,
)


@contextmanager
@aws_client('iot')
def new_certificate(*, iot):
    """Context manager to create a new certificate and remove it when it is no
    longer required.

    :yields:
        A dictionary containing information about the new certificate.

    """

    cert_id = None

    try:
        res = iot.create_keys_and_certificate(setAsActive=False)
        assert 'certificateId' in res, unexpected(res)
        cert_id = res['certificateId']
        print('Created certificate: {}'.format(cert_id))

        yield res
    finally:
        if cert_id is not None:
            ignore_errors(delete_certificate)(cert_id, iot=iot)


@contextmanager
@aws_client('iot')
def new_thing(prefix, *, iot):
    """Context manager to create a new thing and remove it when it is no longer
    required.

    :param str prefix:
        Prefix to use when generating the thing's name.

    :yields:
        A dictionary containing information about the new thing.

    """

    name = make_identifier('test-thing')
    created = False

    try:
        res = iot.create_thing(thingName=name)
        assert 'thingArn' in res, unexpected(res)
        print('Created thing: {}'.format(name))
        created = True

        yield res
    finally:
        if created:
            ignore_errors(delete_thing)(name, iot=iot)


@aws_client('iot')
def delete_certificate(cert_id, *, iot):
    """Delete an IoT Certificate.

    :param str cert_id:
        The ID of a Certificate.

    """

    iot.update_certificate(
        certificateId=cert_id,
        newStatus='INACTIVE',
    )

    print('Deleting certificate: {}'.format(cert_id))
    iot.delete_certificate(
        certificateId=cert_id,
        forceDelete=True,
    )


@aws_client('iot')
def delete_thing_type(name, *, iot):
    """Delete an IoT thing type.

    :param str name:
        Name of the thing type.

    """

    print('Deleting thing type: {}'.format(name))
    iot.deprecate_thing_type(thingTypeName=name)
    ignore_errors(iot.delete_thing_type)(thingTypeName=name)


@ignore_errors
@aws_client('iot')
def cleanup_thing_types(*, iot):
    """Remove unused thing types."""

    delete = ignore_errors(delete_thing_type)

    res = iot.list_thing_types()
    for thing_type in res['thingTypes']:
        delete(thing_type['thingTypeName'], iot=iot)


@aws_client('iot')
def delete_thing(name, *, iot):
    """Delete an IoT thing.

    :param str name:
        Name of the thing.

    """

    print('Deleting thing: {}'.format(name))
    iot.delete_thing(thingName=name)


@aws_client('iot')
def cancel_job(job_id, *, iot):
    """Cancel an IoT job.

    :param str job_id:
        ID of the job.

    """

    print('Canceling job: {}'.format(job_id))
    return iot.cancel_job(jobId=job_id)


@aws_client('iot')
def delete_policy(name, *, iot):
    """Delete an IoT policy.

    :param str name:
        Name of the policy.

    """

    print('Deleting policy: {}'.format(name))
    return iot.delete_policy(policyName=name)


@aws_client('iot')
def delete_topic_rule(name, *, iot):
    """Delete an IoT topic rule.

    :param str name:
        Name of the topic rule.

    """

    print('Deleting topic rule: {}'.format(name))
    return iot.delete_topic_rule(ruleName=name)
