"""
=========================================
AWS Relational Database Service Utilities
=========================================

Helpers for working with AWS RDS.

"""

from collections import UserDict
from contextlib import contextmanager
import time

from aws_test_functions import aws_client, get_uuid
from .vpc import get_vpc_subnets


# Default parameters used when creating RDS instances for most engines. Each
# engine may override these values using the ``RDS_ENGINE_PARAMS`` dictionary.
# Parameters may also be excluded for specific engines by setting the value to
# ``None`` for in ``RDS_ENGINE_PARAMS``.
RDS_DEFAULT_PARAMS = dict(
    DBName='test_rds',
    StorageType='standard',
    AllocatedStorage=10,
    MasterUsername='rhedcloudtest',
    MasterUserPassword='Em0r4^3mO%y',
    BackupRetentionPeriod=0,
    PubliclyAccessible=False,
    StorageEncrypted=True,
    MultiAZ=False,
    AvailabilityZone='us-east-1b',
    # smallest class that supports encryption
    DBInstanceClass='db.t2.small',
)


# These parameters are ignored when creating RDS instances using Aurora
# clusters, since the cluster manages most of these details. These are shared
# between both MySQL and PostgreSQL versions of Aurora.
IGNORE_PARAMS_FOR_AURORA = dict(
    DBName=None,
    BackupRetentionPeriod=None,
    StorageType=None,
    AllocatedStorage=None,
    MultiAZ=None,

    # smallest size that Aurora supports for both MySQL and PostgreSQL
    DBInstanceClass='db.r4.large',
)

# Parameter overrides or omissions per engine.
RDS_ENGINE_PARAMS = {
    'sqlserver-web': dict(
        # do not specify a DBName for SQL Server
        DBName=None,

        # minimum size for SQL Server is 20GB
        AllocatedStorage=20,
    ),
    'oracle-ee': dict(
        DBName='ORCL',
    ),
    'aurora': IGNORE_PARAMS_FOR_AURORA,
    'aurora-mysql': IGNORE_PARAMS_FOR_AURORA,
    'aurora-postgresql': IGNORE_PARAMS_FOR_AURORA,
}

# Set DEBUG_INSTANCE to the RDS Instance Name for an existing instance that you
# wish to use to develop and debug tests without having to wait for the same
# instance to get rebuilt over and over and over.
DEBUG_INSTANCE = None
# DEBUG_INSTANCE = 'test-rds-oracle-ee-b4ca1446-cbc9-4cbd-a46c-55b00334cc80'


class RDSInstance(UserDict):
    """RDSInstance is a wrapper around a dictionary containing RDS instance information."""

    @aws_client('rds')
    def __init__(self, data, rds=None):
        self.data = data
        self.rds = rds

    def wait(self):
        """Wait for the RDS instance to be available for use.

        The information about the RDS instance must be updated once it becomes
        available, since information like the endpoint or hostname to access
        the instance is unknown until then.

        :returns:
            A dictionary.

        """

        self.data.update(wait_for_rds_instance(self.data['DBInstanceIdentifier'], rds=self.rds))
        return self.data

    def cleanup(self):
        """Delete the RDS instance.

        Also make a feeble attempt to remove the DB subnet group that was
        created for this RDS instance.

        """

        if self.data and not DEBUG_INSTANCE:
            delete_db_instance(self.data['DBInstanceIdentifier'], self.data['Engine'], rds=self.rds)

            # this will most likely fail, since it takes a long time for RDS
            # instances to be destroyed (and they'll still be attached to the
            # subnet group until they're fully deleted)
            delete_db_subnet_group(self.data['DBSubnetGroup']['DBSubnetGroupName'], rds=self.rds)


@contextmanager
@aws_client('rds')
def new_db_instance(prefix, engine, vpc_id, *, rds, **kwargs):
    """Context manager to create a new RDS instance and destroy it when we're done with it.

    :param str prefix:
        Prefix to give the RDS instance.
    :param str engine:
        Database engine to use.
    :param str vpc_id:
        ID of the VPC where the RDS instance should be created.

    :yields:
        A containing dictionary with RDS instance information.

    """

    db = None

    try:
        db = new_db_instance_async(prefix, engine, vpc_id, rds=rds, **kwargs)
        db.wait()

        yield db
    finally:
        if db is not None:
            db.cleanup()


def new_db_instance_async(prefix, engine, vpc_id, *, rds, **kwargs):
    """Create a new RDS instance without waiting for it to be ready.

    :param str prefix:
        Prefix to give the RDS instance.
    :param str engine:
        Database engine to use.
    :param str vpc_id:
        ID of the VPC where the RDS instance should be created.

    :yields:
        A containing dictionary with RDS instance information.

    """

    name = '{}-{}'.format(prefix, get_uuid())[:60]
    subnets = [s['SubnetId'] for s in get_vpc_subnets(vpc_id)]
    group = create_db_subnet_group(subnets, rds=rds)

    kwargs.update({
        # do not wait for the instance to be available before yielding control
        'wait': False,
        'DBSubnetGroupName': group['DBSubnetGroupName'],
    })

    db = create_db_instance(name, engine, rds=rds, **kwargs)

    return RDSInstance(db, rds=rds)


def is_aurora(engine):
    """Reusable check to see if the specified RDS engine is Aurora."""

    return 'aurora' in engine


def get_db_instance_params(engine, **kwargs):
    """Include default parameter values for the specified RDS engine.

    :param str engine:
        RDS engine.

    :returns:
        A dictionary.

    .. note::

        The dictionary passed into (and returned by) this function is modified
        in place.

    """

    kwargs.update(dict(
        Engine=engine,
    ))

    for key, value in RDS_DEFAULT_PARAMS.items():
        # optionally override the value based on the engine
        value = RDS_ENGINE_PARAMS.get(engine, {}).get(key, value)

        if value is None:
            # skip this parameter
            continue

        if key not in kwargs or kwargs[key] is None:
            print('Setting default {} RDS value for {}: {}'.format(engine, key, value))
            kwargs[key] = value

    return kwargs


@aws_client('rds')
def create_db_instance(name, engine, *, wait=True, rds, **kwargs):
    """Create a new RDS instance.

    :param str name:
        Name to give the RDS instance.
    :param str engine:
        Database engine to use.

    :returns:
        A dictionary with RDS instance information.

    """

    if DEBUG_INSTANCE:
        db = rds.describe_db_instances(
            DBInstanceIdentifier=DEBUG_INSTANCE,
        )['DBInstances'][0]

    else:
        kwargs['DBInstanceIdentifier'] = name
        kwargs = get_db_instance_params(engine, **kwargs)

        if is_aurora(engine):
            # create an Aurora cluster before creating an RDS instance
            cluster = create_aurora_cluster(name, rds=rds, **kwargs)

            # link the RDS instance with the new cluster
            kwargs.update({
                'DBClusterIdentifier': cluster['DBClusterIdentifier'],
            })

            # the following settings are managed by the cluster, not the
            # instances within the cluster
            # kwargs.pop('DBName')
            kwargs.pop('MasterUsername')
            kwargs.pop('MasterUserPassword')
            kwargs.pop('StorageEncrypted')

            # we don't have subnets in enough AZs to use a custom subnet group :(
            kwargs.pop('DBSubnetGroupName')

        res = rds.create_db_instance(**kwargs)
        assert 'DBInstance' in res, 'Unexpected result: {}'.format(res)

        db = res['DBInstance']
        db_id = db['DBInstanceIdentifier']
        print('Created RDS instance: {}'.format(db_id))

        if wait:
            db = wait_for_rds_instance(db_id, rds=rds)

    # the password is not included when describing RDS instances, so we need to
    # include it manually
    db['MasterUserPassword'] = kwargs.get('MasterUserPassword', RDS_DEFAULT_PARAMS['MasterUserPassword'])

    return db


@aws_client('rds')
def create_aurora_cluster(name, *, rds, **kwargs):
    """Create an Aurora cluster.

    :param str name:
        The name for the cluster.

    :returns:
        A dictionary containing information about the Aurora cluster.

    """

    # rebuild the keyword arguments for creating the cluster
    kwargs = {
        key: kwargs[key]
        for key in (
            'DBSubnetGroupName',
            'Engine',
            'MasterUsername',
            'MasterUserPassword',
            'StorageEncrypted',
        )
    }

    name = name[:34]
    print('Creating Aurora cluster: {}...'.format(name))
    res = rds.create_db_cluster(
        DatabaseName='test_rds',
        DBClusterIdentifier=name,
        # AvailabilityZones=[
        #     'us-east-1b',
        #     'us-east-1c',
        #     'us-east-1d',
        # ],
        **kwargs)
    assert 'DBCluster' in res, 'Unexpected result: {}'.format(res)

    # wait for the cluster to be created
    for attempt in range(12):
        res = rds.describe_db_clusters(DBClusterIdentifier=name)
        assert 'DBClusters' in res and len(res['DBClusters'])
        cluster = res['DBClusters'][0]

        if cluster['Status'] == 'available':
            break

        time.sleep(5)

    print('Created Aurora cluster: {}'.format(name))
    return cluster


@aws_client('rds')
def wait_for_rds_instance(db_instance_identifier, state=None, *, delay=30, max_attempts=60, rds):
    """Wait for the specified RDS instance to be in a certain state.

    :param str db_instance_available:
        The Instance Identifier for the RDS instance.
    :param str state: (optional)
        The state to wait for, such as ``available`` or ``deleted``. Default is
        `available`.
    :param int delay: (optional)
        Number of seconds to wait between checking the instance state.
    :param int max_attempts: (optional)
        Maximum number of times the instance state will be checked.

    :returns:
        A dictionary with information about the RDS instance.

    """

    if state is None:
        state = 'available'

    waiter = rds.get_waiter('db_instance_{}'.format(state))

    print('Waiting on RDS instance to be {}: {}'.format(state, db_instance_identifier))
    waiter.wait(
        DBInstanceIdentifier=db_instance_identifier,
        WaiterConfig={
            'Delay': delay,
            'MaxAttempts': max_attempts,
        },
    )

    # describe the newly-created instance after it's ready so we can get
    # additional info, such as the instance endpoint
    desc = rds.describe_db_instances(DBInstanceIdentifier=db_instance_identifier)

    return desc['DBInstances'][0]


@aws_client('rds')
def delete_db_instance(db_instance_identifier, engine, *, rds):
    """Delete an RDS instance.

    :param str db_instance_identifier:
        The Identifier for the RDS instance.
    :param str engine:
        The database enginer, used to determine whether we're deleting an
        instance or a cluster.

    """

    print('Deleting RDS instance: {}'.format(db_instance_identifier))
    rds.delete_db_instance(
        DBInstanceIdentifier=db_instance_identifier,
        SkipFinalSnapshot=True,
    )

    if is_aurora(engine):
        # TODO: it can take a very long time to fully delete an RDS instance,
        # which needs to happen before the cluster can be removed. Maybe handle
        # that automatically sometime.
        # delete_aurora_cluster(db_instance_identifier[:34], rds=rds)
        pass


@aws_client('rds')
def cleanup_aurora_clusters(*, rds):
    """Remove any Aurora clusters that are linked with no RDS instances."""

    for cluster in rds.describe_db_clusters()['DBClusters']:
        if len(cluster['DBClusterMembers']):
            continue

        delete_aurora_cluster(cluster['DBClusterIdentifier'], rds=rds)


@aws_client('rds')
def delete_aurora_cluster(db_cluster_identifier, *, rds):
    """Delete a specific Aurora cluster.

    :param str db_cluster_identifier:
        Name of the Aurora cluster.

    """

    print('Deleting Aurora cluster: {}'.format(db_cluster_identifier))
    rds.delete_db_cluster(
        DBClusterIdentifier=db_cluster_identifier,
        SkipFinalSnapshot=True,
    )


@contextmanager
@aws_client('rds')
def new_db_subnet_group(subnets, *, rds):
    """Context manager to create and destroy a DB subnet group.

    :param list subnets:
        One or more subnet IDs.

    :yields:
        A dictionary with information about the DB subnet group.

    """

    group = name = None

    try:
        group = create_db_subnet_group(subnets, rds=rds)
        name = group['DBSubnetGroupName']

        yield group
    finally:
        if group is not None:
            delete_db_subnet_group(name, rds=rds)


@aws_client('rds')
def create_db_subnet_group(subnets, *, rds):
    """Create a new RDS subnet group for the specified subnets.

    :param list subnets:
        One or more subnet IDs.

    :returns:
        A dictionary with information about the DB subnet group.

    """

    name = 'test-{}'.format(get_uuid().replace('-', '')[:10])
    res = rds.create_db_subnet_group(
        DBSubnetGroupName=name,
        DBSubnetGroupDescription=name,
        SubnetIds=subnets,
    )
    assert 'DBSubnetGroup' in res

    group = res['DBSubnetGroup']
    print('Created DB subnet group: {}'.format(group['DBSubnetGroupName']))

    return group


@aws_client('rds')
def cleanup_db_subnet_groups(*, rds):
    """Remove all DB subnet groups that are not in use."""

    for info in rds.describe_db_subnet_groups()['DBSubnetGroups']:
        delete_db_subnet_group(info['DBSubnetGroupName'], rds=rds)


@aws_client('rds')
def delete_db_subnet_group(group_name, *, rds):
    """Delete a DB Subnet Group.

    :param str group_name:
        The name of the DB Subnet Group.

    """

    try:
        print('Deleting DB subnet group: {}'.format(group_name))
        rds.delete_db_subnet_group(
            DBSubnetGroupName=group_name,
        )
    except Exception as ex:
        print('Failed to remove DB Subnet Group: {}'.format(ex))


@aws_client('rds')
def cleanup_option_groups(*, rds):
    """Remove any option groups that are not in use."""

    for info in rds.describe_option_groups()['OptionGroupsList']:
        name = info['OptionGroupName']
        if 'default' in name:
            # can't delete default option groups
            continue

        try:
            print('Deleting option group: {}'.format(name))
            rds.delete_option_group(OptionGroupName=name)
        except Exception as ex:
            print('Failed to delete option group {}: {}'.format(name, ex))


@aws_client('rds')
def cleanup_db_parameter_groups(*, rds):
    """Remove any parameter groups that are not in use."""

    for info in rds.describe_db_parameter_groups()['DBParameterGroups']:
        name = info['DBParameterGroupName']
        if 'default' in name:
            # can't delete default parameter groups
            continue

        try:
            print('Deleting parameter group: {}'.format(name))
            rds.delete_db_parameter_group(DBParameterGroupName=name)
        except Exception as ex:
            print('Failed to delete parameter group {}: {}'.format(name, ex))
