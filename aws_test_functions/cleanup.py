"""
=================
Cleanup Utilities
=================

"""

from .decorators import aws_client
from .lib import ClientError, delete_test_policy_using_client
from .utils import debug


def cleanup():
    """Cleanup any unused resources."""

    cleanup_iam_policies()


@aws_client('iam')
def cleanup_iam_policies(needle='test', *, iam):
    """Attempt to remove any unused IAM Policies."""

    more_policies = True
    params = dict(
        Scope='Local',
    )

    debug('Cleaning up unused IAM Policies...')
    while more_policies:
        res = iam.list_policies(**params)

        # handle multiple pages of policies
        more_policies = res['IsTruncated']
        if more_policies:
            params['Marker'] = res['Marker']

        for policy in res['Policies']:
            # skip non-test policies
            if needle not in policy['PolicyName']:
                continue

            try:
                delete_test_policy_using_client(iam, policy['Arn'])
            except ClientError as ex:
                debug(ex)
