"""
================
AWS KinesisVideo
================

Utilities for AWS KinesisVideo.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import has_status, unexpected


@contextmanager
@aws_client('kinesisvideo')
def new_kinesisvideo_stream(prefix, *, kinesisvideo):
    """Context manager to create a KinesisVideo stream and remove it when it's
    no longer used.

    :yields:
        A KinesisVideo ARN

    """

    res = None
    kv_arn = None
    name = '{}-{}'.format(prefix, get_uuid())
    try:
        res = kinesisvideo.create_stream(StreamName=name, MediaType='video/h264')
        assert has_status(res, 200), unexpected(res)

        kv_arn = res['StreamARN']
        yield kv_arn
    finally:
        if res is not None:
            res = kinesisvideo.delete_stream(StreamARN=kv_arn)
            assert has_status(res, 200), unexpected(res)
