"""
=====================
AWS Glacier Utilities
=====================

Shared utilities for interacting with AWS Glacier.

"""

from .decorators import aws_client
from .utils import has_status, unexpected


@aws_client('glacier')
def get_glacier_vault_inventory(name, *, glacier):
    """Attempt to retrieve the inventory for the specified vault.

    :param str name:
        Name of a vault in Glacier.

    :returns:
        A dictionary. But really, we will likely always get None because
        Glacier is glacially slow.

    """

    job = glacier.initiate_job(
        vaultName=name,
        jobParameters=dict(
            Type='inventory-retrieval',
        ),
    )
    assert 'jobId' in job, 'Unexpected response: {}'.format(job)

    return get_glacier_job_output(name, job['jobId'], glacier=glacier)


@aws_client('glacier')
def get_glacier_job_status(vault_name, job_id, *, glacier):
    """Retrieve the status of the specified job.

    :param str vault_name:
        Name of the Glacier vault the job belongs to.
    :param str job_id:
        ID of the job to check.

    :returns:
        A dictionary.

    """

    print('Checking job status: {}'.format(job_id))
    status = glacier.describe_job(vaultName=vault_name, jobId=job_id)
    assert 'Completed' in status, 'Unexpected response: {}'.format(status)

    return status


@aws_client('glacier')
def get_glacier_job_output(vault_name, job_id, *, glacier):
    """Get the output from a Glacier job.

    :param str vault_name:
        Name of the Glacier vault the job belongs to.
    :param str job_id:
        ID of the job to check.

    :returns:
        A dictionary. But really, we will likely always get None because
        Glacier is glacially slow.

    """

    res = None
    status = get_glacier_job_status(vault_name, job_id, glacier=glacier)
    if status['Completed']:
        res = glacier.get_job_output(vaultName=vault_name, jobId=job_id)
        assert 'body' in res, 'Unexpected response: {}'.format(res)

    return res


@aws_client('glacier')
def delete_glacier_archive(vault_name, archive_id, *, glacier):
    """Delete an archive from the specified vault.

    :param str vault_name:
        Name of the Glacier vault where the archive resides.
    :param str archive_id:
        ID of the archive to delete.

    :returns:
        A dictionary.

    """

    print('Deleting from vault {}: {}'.format(vault_name, archive_id))
    res = glacier.delete_archive(
        vaultName=vault_name,
        archiveId=archive_id,
    )

    assert 'ResponseMetadata' in res, 'Unexpected response: {}'.format(res)
    assert has_status(res, 204), unexpected(res)

    return res


@aws_client('glacier')
def delete_glacier_vault(name, *, glacier):
    """Helper for deleting Glacier Vaults.

    :param str name:
        Name of the Vault to delete.

    :returns:
        A dictionary.

    """

    print('Deleting vault {}'.format(name))

    inv = get_glacier_vault_inventory(name, glacier=glacier)
    if inv:
        # TODO delete any inventory if we ever get something back from this...
        pass

    return glacier.delete_vault(vaultName=name)


@aws_client('glacier')
def cleanup_glacier_vaults(*, glacier):
    """Attempt to clean up any old Vaults.

    :returns:
        An integer: The number of vaults removed.

    """

    count = 0

    print('Cleaning up old vaults...')
    for vault in glacier.list_vaults()['VaultList']:
        try:
            delete_glacier_vault(vault['VaultName'], glacier=glacier)
            count += 1
        except Exception as ex:
            print(ex)

    print('Removed {} vaults'.format(count))
    return count
