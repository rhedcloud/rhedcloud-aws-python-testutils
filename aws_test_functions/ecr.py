"""
==============================
AWS Elastic Container Registry
==============================

Utilities for AWS ECR.

"""

from contextlib import contextmanager
import base64

try:
    from docker import APIClient
    from docker.utils import kwargs_from_env
except ImportError:
    print("WARNING: Failed it import docker library; ECR utilities might be broken")

from .decorators import aws_client, ignore_errors
from .lib import get_uuid
from .utils import has_status, unexpected


@contextmanager
@aws_client("ecr")
def new_ecr_repository(prefix, *, ecr):
    """Context manager to create a new ecr repository and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the input's name.

    :yields:
        An ECR Repository name

    """

    name = "{}-{}".format(prefix, get_uuid())
    res = None
    try:
        res = create_ecr_repository(name, ecr=ecr)
        yield name
    finally:
        if res is not None:
            print("Removing ECR repository: {}".format(name))
            res = ecr.delete_repository(repositoryName=name, force=True)
            assert has_status(res, 200), unexpected(res)


@aws_client("ecr")
def create_ecr_repository(name, ecr):
    """Create a new ECR repository

    :param str name:
        Name of the repository to create.

    :returns:
        The response from the create_repository operation

    """

    res = ecr.create_repository(repositoryName=name)
    assert has_status(res, 200), unexpected(res)
    print("Created ECR Repository: {}".format(name))
    return res


@aws_client("ecr")
def docker_to_ecr(stack, *, ecr):
    name = stack.enter_context(new_ecr_repository(prefix="test_ecr"))

    # Retrieve authorization information
    tag = "awsorg/testecr"
    token_response = ecr.get_authorization_token()
    auth_data = token_response["authorizationData"][0]
    token = auth_data["authorizationToken"]
    endpoint = auth_data["proxyEndpoint"]
    auth_string = base64.b64decode(token.encode()).decode()
    username, password = auth_string.split(":")

    docker_client = APIClient(**kwargs_from_env(assert_hostname=False))

    try:
        existent_images = docker_client.images()

        # cleanup docker images
        stack.callback(cleanup_docker, docker_client, existent_images)
    except Exception as ex:
        print("Failed to list existing images: {}".format(ex))

    # build the docker image
    build_response = [
        line
        for line in docker_client.build(
            tag=tag, path="./tests/resources/fetch-and-run", rm=True, decode=True
        )
    ]
    num_lines = len(build_response)
    assert num_lines > 3, "Malformed build response/n {}".format(build_response)
    image_id_line = build_response[num_lines - 2]["stream"]
    assert (
        "Successfully built" in image_id_line
    ), "Malformed build response/n {}".format(
        image_id_line
    )

    # tag the image
    repo = "{}/{}".format(endpoint.replace("https://", ""), name)
    assert docker_client.tag(
        tag, repo
    ), "Failed to tag docker image for repo {}".format(repo)

    # push the image
    list(
        docker_client.push(
            repo,
            auth_config={"username": username, "password": password},
            stream=True,
            decode=True,
        )
    )

    # validate the image was pushed
    image_description = ecr.describe_images(repositoryName=name)
    assert "imageDetails" in image_description and len(
        image_description["imageDetails"]
    )

    return repo


def cleanup_docker(client, exclude_images=None):
    """Remove docker images.

    :param client:
        A handle to the Docker API.
    :param iter exclude_images: (optional)
        An iterable of images to NOT delete.

    """

    if exclude_images is None:
        exclude_images = tuple()

    remove_image = ignore_errors(client.remove_image)

    for image in client.images():
        if image in exclude_images:
            continue

        for tag in image["RepoTags"]:
            remove_image(tag)
