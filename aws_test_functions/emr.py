"""
=====================
AWS Elastic MapReduce
=====================

"""

from contextlib import contextmanager

from .decorators import aws_client


@contextmanager
@aws_client('emr')
def new_emr_cluster(name, role, profile, subnet_id, *, emr):
    """Context manager to create and automatically destroy an EMR Cluster.

    :param EMR.Client emr:
        A handle to the EMR APIs.
    :param str name:
        Name of the cluster to create.
    :param IAM.Role role:
        IAM Role to use in the cluster.
    :param IAM.InstanceProfile profile:
        IAM Instance Profile to use in the cluster.
    :param str subnet_id:
        ID of the subnet where the cluster should reside.

    :yields:
        A string: the ID of the cluster.

    """

    job_flow_id = None

    try:
        res = emr.run_job_flow(
            Name=name,
            ReleaseLabel='emr-5.11.1',
            Instances=dict(
                InstanceGroups=[dict(
                    Name='Master Nodes',
                    Market='ON_DEMAND',
                    InstanceRole='MASTER',
                    InstanceType='m4.large',
                    InstanceCount=1,
                ), dict(
                    Name='Core Nodes',
                    Market='ON_DEMAND',
                    InstanceRole='CORE',
                    InstanceType='m4.large',
                    InstanceCount=2,
                )],
                KeepJobFlowAliveWhenNoSteps=False,
                TerminationProtected=False,
                Ec2SubnetId=subnet_id,
            ),
            Steps=[],
            VisibleToAllUsers=True,
            ServiceRole=role.role_name,
            JobFlowRole=profile.instance_profile_name,
        )
        assert 'JobFlowId' in res, 'Unexpected response: {}'.format(res)
        job_flow_id = res['JobFlowId']

        yield job_flow_id
    finally:
        if job_flow_id is not None:
            emr.terminate_job_flows(JobFlowIds=[job_flow_id])
