"""
===========
AWS Kinesis
===========

Utilities for AWS Kinesis.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import has_status, retry, unexpected


TEST_SQL_APP = '''CREATE OR REPLACE STREAM "TestSCPOutputStream" (
 datetime VARCHAR(30),
 status INTEGER,
 statusCount INTEGER);
CREATE OR REPLACE PUMP "STREAM_PUMP" AS
 INSERT INTO "TestSCPOutputStream"
 SELECT
 STREAM TIMESTAMP_TO_CHAR('yyyy-MM-dd''T''HH:mm:ss.SSS', LOCALTIMESTAMP) as datetime,
 "response" as status,
 COUNT(*) AS statusCount
 FROM "TestSCPInputStream_001"
 GROUP BY
 "response",
 FLOOR(("TestSCPInputStream_001".ROWTIME - TIMESTAMP '1970-01-01 00:00:00') minute / 1 TO MINUTE);'''


TEST_INPUT_SCHEMA = dict(
    RecordColumns=[
        {'SqlType': 'VARCHAR(16)', 'Name': 'host', 'Mapping': '$.host'},
        {'SqlType': 'VARCHAR(32)', 'Name': 'datetime', 'Mapping': '$.datetime'},
        {'SqlType': 'VARCHAR(64)', 'Name': 'request', 'Mapping': '$.request'},
        {'SqlType': 'SMALLINT', 'Name': 'response', 'Mapping': '$.response'},
        {'SqlType': 'SMALLINT', 'Name': 'bytes', 'Mapping': '$.bytes'},
        {'SqlType': 'VARCHAR(128)', 'Name': 'agent', 'Mapping': '$.agent'},
        {'SqlType': 'VARCHAR(32)', 'Name': 'referrer', 'Mapping': '$.referrer'},
    ],
    RecordFormat={'MappingParameters': {'JSONMappingParameters': {'RecordRowPath': '$'}}, 'RecordFormatType': 'JSON'},
    RecordEncoding='UTF-8',
)


@contextmanager
@aws_client('firehose')
def new_firehose_delivery_stream(prefix, role_arn, bucket_name, *, firehose):
    """Context manager to create a ECS cluster and remove it when it's
    no longer used.

    :param str prefix:
        A string to use as a prefix for a randomly generated name
    :param str role_arn:
        The role to use for firehose
    :param str bucket_name:
        The name of the bucket to deliver to.

    :yields:
        A Firehose delivery stream name

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        res = firehose.create_delivery_stream(
            DeliveryStreamName=name,
            DeliveryStreamType='DirectPut',
            ExtendedS3DestinationConfiguration={
                'RoleARN': role_arn,
                'BucketARN': 'arn:aws:s3:::' + bucket_name,
                'BufferingHints': {'SizeInMBs': 1, 'IntervalInSeconds': 60},
            },
        )
        status = retry(
            firehose.describe_delivery_stream,
            kwargs={'DeliveryStreamName': name},
            msg='Checking startup status for {}'.format(name),
            until=lambda s: s['DeliveryStreamDescription']['DeliveryStreamStatus'] == 'ACTIVE',
        )
        assert (
            status['DeliveryStreamDescription']['DeliveryStreamStatus'] == 'ACTIVE'
        ), 'Failed to create delivery stream'

        print('Created Delivery Stream: {}'.format(name))
        yield name
    finally:
        if res is not None:
            print('Removing Delivery Stream: {}'.format(name))
            res = firehose.delete_delivery_stream(DeliveryStreamName=name)
            assert has_status(res, 200), unexpected(res)


@contextmanager
@aws_client('kinesisanalytics')
def new_kinesis_analytics_application(
    prefix, input_stream_arn, output_stream_arn, role_arn, start, *, kinesisanalytics
):
    """Context manager to create a ECS cluster and remove it when it's
    no longer used.

    :param str prefix:
        A string to use as a prefix for a randomly generated name
    :param str input_stream_arn:
        The role to use for the firehose input stream
    :param str output_stream_arn:
        The role to use for the firehose output stream
    :param str role_arn:
        The role to use for the application
    :param bool start:
        Whether to start the application after creation
    :param str bucket_name:
        The name of the bucket to deliver to.

    :yields:
        A Kinesis Analytics Application name

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    create_time = None
    try:
        res = kinesisanalytics.create_application(
            ApplicationName=name,
            ApplicationDescription='TestSCPApp',
            Inputs=[
                {
                    'NamePrefix': 'TestSCPInputStream',
                    'KinesisFirehoseInput': {'ResourceARN': input_stream_arn, 'RoleARN': role_arn},
                    'InputSchema': TEST_INPUT_SCHEMA,
                }
            ],
            Outputs=[
                {
                    'Name': 'TestSCPOutputStream',
                    'KinesisFirehoseOutput': {'ResourceARN': output_stream_arn, 'RoleARN': role_arn},
                    'DestinationSchema': {'RecordFormatType': 'JSON'},
                }
            ],
            ApplicationCode=TEST_SQL_APP,
        )
        print('Created Analytics Application: {}'.format(name))
        assert has_status(res, 200), unexpected(res)

        if start:
            app_desc = kinesisanalytics.describe_application(ApplicationName=name)
            input_desc = app_desc['ApplicationDetail']['InputDescriptions']
            trim_input_desc = []
            for single_input in input_desc:
                trim_input_desc.append(
                    dict(
                        Id=single_input['InputId'],
                        InputStartingPositionConfiguration={'InputStartingPosition': 'LAST_STOPPED_POINT'},
                    )
                )

            kinesisanalytics.start_application(ApplicationName=name, InputConfigurations=trim_input_desc)
        yield name
    finally:
        if res is not None:
            print('Removing Analytics Application: {}'.format(name))
            app_desc = kinesisanalytics.describe_application(ApplicationName=name)
            create_time = app_desc['ApplicationDetail']['CreateTimestamp']
            res = kinesisanalytics.delete_application(ApplicationName=name, CreateTimestamp=create_time)
            assert has_status(res, 200), unexpected(res)
