"""
========================
AWS Simple Email Service
========================

Utilities for interacting with AWS Simple Email Service (SES).

.. warning::

    This utility is configured to automatically delete messages that it
    consumes. It is probably unwise to hook this up to an email address that
    you care about even just a little.

Environment Variables
=====================

The following environment variables may be used to tweak behavior:

* :envvar:`IMAP_SERVER`: The IMAP server that will be used when checking for
  received email. Default is ``imap.mail.yahoo.com``.
* :envvar:`IMAP_USER`: The username to use when authenticating with the IMAP
  server.
* :envvar:`IMAP_PASS`: The password to use when authenticating with the IMAP
  server.
* :envvar:`IMAP_MAILBOX`: The mailbox to check by default. Default is
  ``INBOX``.

"""

from imaplib import IMAP4_SSL
import os
import re

from .decorators import aws_client


# Default information for an inbox that can be used for testing purposes. Each
# value can be overridden using environment variables.
IMAP_SERVER = os.getenv('IMAP_SERVER') or 'imap.mail.yahoo.com'
IMAP_USER = os.getenv('IMAP_USER') or 'zm7gxhpvquuorxeyata@yahoo.com'
IMAP_PASS = os.getenv('IMAP_PASS') or 'fiwVyFLaMuXFxFTHmgn9'
IMAP_MAILBOX = os.getenv('IMAP_MAILBOX') or 'INBOX'

URL_RE = re.compile(r'(https://email-verification\.[^\s]+)')


def get_messages(server=IMAP_SERVER, user=IMAP_USER, password=IMAP_PASS, mailbox=IMAP_MAILBOX, delete=True):
    """Generator that returns the body of each email in the specified mailbox.

    :param str server: (optional)
        Hostname or IP of the IMAP server to connect to.
    :param str user: (optional)
        Username to use when authenticating with the specified server.
    :param str password: (optional)
        Password to use when authenticating with the specified server.
    :param str mailbox: (optional)
        Mailbox to check.
    :param bool delete: (optional)
        Whether to mark messages for deletion after consuming them.

    :yields:
        The body of each email found in the specified mailbox.

    """

    with IMAP4_SSL(server) as m:
        m.noop()
        m.login(user, password)
        m.select(mailbox)

        try:
            typ, data = m.search(None, 'ALL')
            for num in reversed(data[0].split()):
                typ, data = m.fetch(num, '(RFC822)')

                try:
                    yield data[0][1].decode()
                finally:
                    if delete:
                        # mark the message for deletion
                        m.store(num, '+FLAGS', '\\Deleted')
        finally:
            if delete:
                m.expunge()


def get_verification_email(mailbox=IMAP_MAILBOX, delete=True):
    """Look for an email that appears to contain a validation link from SES. If
    such an email is found, return its body along with the extracted validation
    URL.

    :param str mailbox: (optional)
        Mailbox to check.
    :param bool delete: (optional)
        Whether to mark messages for deletion after consuming them.

    :raises FileNotFoundError:
        When no validation email is detected.

    :returns:
        A 2-tuple containing the email and the validation URL extracted from
        the email body.

    """

    for msg in get_messages(mailbox=mailbox, delete=delete):
        match = URL_RE.findall(msg)
        if match:
            return msg, match[0]

    raise FileNotFoundError('No verification email found')


def get_email(mailbox=IMAP_MAILBOX, delete=True):
    """Retrieve the next email in the specified mailbox.

    :param str mailbox: (optional)
        Mailbox to check.
    :param bool delete: (optional)
        Whether to mark messages for deletion after consuming them.

    :raises FileNotFoundError:
        When no email is detected.

    :returns:
        A string: the email.

    """

    for msg in get_messages(mailbox=mailbox, delete=delete):
        return msg

    raise FileNotFoundError('No email found')


@aws_client('ses')
def find_identity(address, *, ses):
    """Look for the specified email address as an identity (regardless of
    validation state) in SES.

    This is used primarily as a sanity check in test functions that otherwise
    may not exercise any SES API calls. It allows the test function to bail out
    early if the SES API is blocked by the SCP.

    :param str address:
        The address to look for.

    """

    res = ses.list_identities()
    assert 'Identities' in res, 'Unexpected response: {}'.format(res)
    assert address in res['Identities'], 'Address {} not a known identity: {}'.format(address, res)
