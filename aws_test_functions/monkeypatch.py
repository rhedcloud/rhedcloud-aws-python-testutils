"""
==================
AWS Monkey Patches
==================

Utilities to work around frustrations with the AWS libraries.

"""

from functools import wraps

from botocore.exceptions import ClientError
import botocore.client

from .utils import retry


TOKEN_ERRORS = ('InvalidClientTokenId', 'UnrecognizedClientException')


def token_error_retry(f):
    """Decorator to automatically retry API requests that appear to fail from
    "invalid token" errors.

    :param callable f:
        A function to decorate.

    :returns:
        A function that provides the retry logic for ``f``.

    """

    @wraps(f)
    def wrapped(*args, **kwargs):
        return retry(
            f,
            args=args,
            kwargs=kwargs,
            msg='Retrying API call (token error)...',
            msg_min_attempts=1,
            max_attempts=20,
            pred=lambda ex: ex.response['Error']['Code'] in TOKEN_ERRORS,
            exceptions=(ClientError,),
        )

    return wrapped


# maintain access to the unpatched method
botocore.client.BaseClient._orig_make_api_call = botocore.client.BaseClient._make_api_call

# monkeypatch the AWS libs to retry requests that fail with token errors
botocore.client.BaseClient._make_api_call = token_error_retry(botocore.client.BaseClient._make_api_call)
