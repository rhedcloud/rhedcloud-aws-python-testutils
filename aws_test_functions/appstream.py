"""
=============
AWS AppStream
=============

"""

from .decorators import aws_client, ignore_errors
from .utils import debug, has_status, retry, unexpected


def _wait_for_state(func, name, key, state, **kwargs):
    """Helper to wait for a resource to be in a particular state.

    :param callable func:
        A function that should return a dict that has a list of objects having
        a ``State`` key.
    :param str name:
        Name of the resource whose state will be checked.
    :param str key:
        The key for the list of resources.
    :param str state:
        Name of the state the resource is expected to reach.

    :returns:
        A dictionary.

    Usage:

    >>> import boto3
    >>> appstream = boto3.client('appstream')
    >>> res = _wait_for_state(appstream.describe_stacks, 'foo', 'Stacks', 'STOPPED')

    """

    def _test(res):
        return key in res and len(res[key]) and res[key][0]['State'] == state

    delay = kwargs.pop('delay', 30)

    res = retry(
        func,
        kwargs=dict(
            Names=[name],
        ),
        msg='Waiting for {} to reach {}: {}'.format(key, state, name),
        until=_test,
        delay=delay,
        **kwargs,
    )
    assert _test(res), unexpected(res)

    return res


@aws_client('appstream')
def delete_fleet(name, *, appstream):
    """Delete a fleet.

    :param str name:
        Name of the fleet to delete.

    """

    debug('Deleting fleet: {}'.format(name))
    return appstream.delete_fleet(Name=name)


@aws_client('appstream')
def delete_image(name, *, appstream):
    """Delete an image.

    :param str name:
        Name of the image to delete.

    """

    debug('Deleting image: {}'.format(name))
    return appstream.delete_image(Name=name)


@aws_client('appstream')
def stop_image_builder(name, *, appstream):
    """Stop an image builder.

    :param str name:
        Name of the image builder to stop.

    """

    debug('Stopping image builder: {}'.format(name))
    res = appstream.stop_image_builder(Name=name)
    assert has_status(res, 200), unexpected(res)

    return _wait_for_state(appstream.describe_image_builders, name, 'ImageBuilders', 'STOPPED')


@aws_client('appstream')
def delete_image_builder(name, *, max_attempts=30, appstream):
    """Delete an image builder.

    :param str name:
        Name of the image builder to delete.

    """

    def _test(exc):
        exc_str = str(exc)
        for needle in ('STOPPING', 'PENDING'):
            if needle in exc_str:
                return True

        return False

    return retry(
        appstream.delete_image_builder,
        kwargs={'Name': name},
        msg='Deleting image builder: {}'.format(name),
        pred=_test,
        show=True,
        delay=30,
        max_attempts=max_attempts,
    )


@aws_client('appstream')
def delete_stack(name, *, appstream):
    """Delete a stack.

    :param str name:
        Name of the stack to delete.

    """

    debug('Deleting stack: {}'.format(name))
    return appstream.delete_stack(Name=name)


@aws_client('appstream')
def cleanup_fleets(appstream):
    """Attempt to clean up any unused fleets."""

    stop = ignore_errors(appstream.stop_fleet)
    delete = ignore_errors(delete_fleet)

    res = appstream.describe_fleets()
    for fleet in res['Fleets']:
        stop(Name=fleet['Name'])
        delete(fleet['Name'], appstream=appstream)


@aws_client('appstream')
def cleanup_stacks(appstream):
    """Attempt to clean up any unused stacks."""

    delete = ignore_errors(delete_stack)

    res = appstream.describe_stacks()
    for stack in res['Stacks']:
        delete(stack['Name'], appstream=appstream)


@aws_client('appstream')
def cleanup_image_builders(appstream):
    """Attempt to clean up any unused image builders."""

    stop = ignore_errors(stop_image_builder)
    delete = ignore_errors(delete_image_builder)
    delete = delete_image_builder

    res = appstream.describe_image_builders()
    for builder in res['ImageBuilders']:
        stop(builder['Name'], appstream=appstream)
        delete(builder['Name'], appstream=appstream)
