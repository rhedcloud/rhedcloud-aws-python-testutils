"""
==============
AWS MediaStore
==============

Utilities for AWS MediaStore.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import has_status, retry, unexpected


@contextmanager
@aws_client('mediastore')
def new_mediastore_container(prefix, delete, *, mediastore):
    """Context manager to create a MediaStore container and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the container's name.
    :param bool delete:
        Whether or not to delete the container when finished.

    :yields:
        A MediaStore container name

    """

    name = '{}_{}'.format(prefix, get_uuid().replace('-', ''))
    res = None
    try:
        res = mediastore.create_container(ContainerName=name)
        assert has_status(res, 200), unexpected(res)

        status = retry(
            mediastore.describe_container,
            kwargs=dict(ContainerName=name),
            msg='Waiting for container {} to enter ACTIVE state...'.format(name),
            until=lambda s: s['Container']['Status'] == 'ACTIVE',
            delay=10,
            max_attempts=30,
            pred=lambda ex: False,
        )

        assert status['Container']['Status'] == 'ACTIVE', "Container failed to activate: {}".format(status)
        yield name
    finally:
        if res is not None and delete:
            res = mediastore.delete_container(ContainerName=name)
            assert has_status(res, 200), unexpected(res)
