"""
==============
AWS Transcribe
==============

Utilities for AWS Transcribe.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import has_status, retry, unexpected


@contextmanager
@aws_client("transcribe")
def new_transcription_job(prefix, media_file_uri, media_format, language_code, wait, *, transcribe):
    """Context manager to create a Transcribe job and wait for it to complete

    :param str prefix:
        Prefix to give the transcribe job when generating a name
    :param str media_file_uri:
        The URI of the file to transcribe
    :param str media_format:
        The format of the given media file
    :param str language_code:
        The language code of the given media file
    :param bool wait:
        Whether to wait for the job to complete

    :yields:
        A Transcription Job Name

    """

    name = "{}-{}".format(prefix, get_uuid())
    res = None
    started = False
    try:
        res = transcribe.start_transcription_job(
            TranscriptionJobName=name,
            Media=dict(MediaFileUri=media_file_uri),
            MediaFormat=media_format,
            LanguageCode=language_code,
        )

        assert has_status(res, 200), unexpected(res)
        started = True

        print("Started Transcription Job: {}".format(name))

        yield name
    finally:
        if wait and started:
            res = retry(
                transcribe.get_transcription_job,
                kwargs=dict(TranscriptionJobName=name),
                msg="Waiting for Transcription job {} to complete...".format(name),
                until=lambda s: s["TranscriptionJob"]["TranscriptionJobStatus"] in ("COMPLETED", "FAILED"),
                show=True,
                delay=20,
                max_attempts=20,
            )

            assert res["TranscriptionJob"]["TranscriptionJobStatus"] in (
                "COMPLETED",
                "FAILED",
            ), unexpected(res)
