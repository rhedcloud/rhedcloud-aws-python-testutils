"""
======================
AWS Pinpoint Utilities
======================

"""

from .decorators import aws_client


@aws_client('pinpoint')
def delete_app(app_id, *, pinpoint):
    """Delete an app.

    :param str app_id:
        The ID of the app to delete.

    """

    print('Deleting app: {}'.format(app_id))
    pinpoint.delete_app(ApplicationId=app_id)


@aws_client('pinpoint')
def delete_segment(app_id, segment_id, *, pinpoint):
    """Delete a segment.

    :param str app_id:
        The ID of the app to delete.
    :param str segment_id:
        The ID of the segment to delete.

    """

    print('Deleting segment: {}'.format(segment_id))
    pinpoint.delete_segment(
        ApplicationId=app_id,
        SegmentId=segment_id,
    )


@aws_client('pinpoint')
def delete_campaign(app_id, campaign_id, *, pinpoint):
    """Delete a campaign.

    :param str app_id:
        The ID of the app to delete.
    :param str campaign_id:
        The ID of the campaign to delete.

    """

    print('Deleting campaign: {}'.format(campaign_id))
    pinpoint.delete_campaign(
        ApplicationId=app_id,
        CampaignId=campaign_id,
    )
