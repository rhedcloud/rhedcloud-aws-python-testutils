"""
=================
AWS WAF Utilities
=================

"""

from .decorators import aws_client, ignore_errors

waf_client = aws_client('waf', 'waf-regional')


@waf_client
def get_token(*, waf):
    """Generate change token to use in WAF API requests.

    :returns:
        A string.

    """

    return waf.get_change_token()['ChangeToken']


@ignore_errors
@waf_client
def cleanup_waf(*, waf):
    """Clean up any unused resources from previous tests."""

    print('Cleaning up old WAF resources...')
    cleanup_rules(waf=waf)
    cleanup_match_sets(waf=waf)
    cleanup_pattern_sets(waf=waf)
    cleanup_web_acls(waf=waf)


@waf_client
def cleanup_web_acls(*, waf):
    """Delete any web ACLs created during testing."""

    for acl in waf.list_web_acls()['WebACLs']:
        delete_web_acl(acl['WebACLId'], waf=waf)


@waf_client
def cleanup_rules(*, waf):
    """Delete any rule predicates created during testing."""

    for rule in waf.list_rules()['Rules']:
        delete_rule_predicates(rule['RuleId'], waf=waf)
        waf.delete_rule(
            RuleId=rule["RuleId"],
            ChangeToken=get_token(waf=waf),
        )


@waf_client
def cleanup_match_sets(*, waf):
    """Delete any match sets created during testing."""

    # get all regex matches
    for match in waf.list_regex_match_sets()['RegexMatchSets']:
        print('deleting match')
        # get regex match details
        match_id = match['RegexMatchSetId']
        get_response = waf.get_regex_match_set(
            RegexMatchSetId=match_id
        )

        # update regex matches to delete rules, then delete match
        match_set = get_response['RegexMatchSet']
        regex_tuples = match_set['RegexMatchTuples']
        for regex_tuple in regex_tuples:
            print('deleting regex tuple')
            waf.update_regex_match_set(
                RegexMatchSetId=match_set['RegexMatchSetId'],
                Updates=[{
                    'Action': 'DELETE',
                    'RegexMatchTuple': regex_tuple
                }],
                ChangeToken=get_token(waf=waf)
            )

        # delete regex pattern
        waf.delete_regex_match_set(
            RegexMatchSetId=match_id,
            ChangeToken=get_token(waf=waf)
        )


@waf_client
def cleanup_pattern_sets(*, waf):
    """Delete any pattern sets created during testing."""

    # get all regex patterns
    for pattern in waf.list_regex_pattern_sets()['RegexPatternSets']:
        print('deleting pattern')
        # get regex pattern details
        pattern_id = pattern['RegexPatternSetId']
        get_response = waf.get_regex_pattern_set(
            RegexPatternSetId=pattern_id
        )

        # update regex patterns to delete regex strings
        regex_strings = get_response['RegexPatternSet']['RegexPatternStrings']
        for regex_string in regex_strings:
            print('deleting pattern string')
            waf.update_regex_pattern_set(
                RegexPatternSetId=get_response['RegexPatternSetId'],
                Updates=[{
                    'Action': 'DELETE',
                    'RegexPatternString': regex_string
                }],
                ChangeToken=get_token(waf=waf)
            )

        # delete regex pattern
        delete_regex_pattern_set(pattern['RegexPatternSetId'], waf=waf)


@waf_client
def delete_rule_predicates(rule_id, *, waf):
    """Delete all predicates for a specific rule.

    :param str rule_id:
        ID of a WAF rule.

    """

    res = waf.get_rule(RuleId=rule_id)
    for predicate in res['Rule']['Predicates']:
        delete_rule_predicate(rule_id, predicate, waf=waf)


@waf_client
def delete_web_acl(acl_id, *, waf):
    """Delete a web ACL.

    :param str acl_id:
        ID of an ACL.

    """

    res = waf.get_web_acl(WebACLId=acl_id)
    for rule in res['WebACL']['Rules']:
        delete_web_acl_rule(acl_id, rule)

    print('Deleting Web ACL: {}'.format(acl_id))
    waf.delete_web_acl(
        WebACLId=acl_id,
        ChangeToken=get_token(waf=waf),
    )


@waf_client
def delete_web_acl_rule(acl_id, rule, *, waf):
    """Delete a rule from a web ACL.

    :param str acl_id:
        ID of an ACL.
    :param dict rule:
        A rule.

    """

    rule_id = rule['RuleId']

    print('Deleting rule {} from ACL {}'.format(rule_id, acl_id))
    waf.update_web_acl(
        WebACLId=acl_id,
        Updates=[{
            'Action': 'DELETE',
            'ActivatedRule': rule
        }],
        ChangeToken=get_token(waf=waf)
    )


@waf_client
def delete_rule(rule_id, *, waf):
    """Delete a rule.

    :param str rule_id:
        ID of a rule.

    """

    delete_rule_predicates(rule_id, waf=waf)

    print('Deleting rule {}'.format(rule_id))
    waf.delete_rule(
        RuleId=rule_id,
        ChangeToken=get_token(waf=waf)
    )


@waf_client
def delete_rule_predicate(rule_id, predicate, *, waf):
    """Delete a rule predicate.

    :param str rule_id:
        ID of a rule.
    :param dict predicate:
        A predicate.

    """

    print('Deleting predicate {} from rule {}'.format(predicate['DataId'], rule_id))
    waf.update_rule(
        RuleId=rule_id,
        Updates=[{
            'Action': 'DELETE',
            'Predicate': predicate
        }],
        ChangeToken=get_token(waf=waf)
    )


@waf_client
def delete_regex_pattern_set(pattern_id, *, waf):
    """Delete a regex pattern set.

    :param str pattern_id:
        ID of a regex pattern set.

    """

    print('Deleting pattern set: {}'.format(pattern_id))
    waf.delete_regex_pattern_set(
        RegexPatternSetId=pattern_id,
        ChangeToken=get_token(waf=waf)
    )
