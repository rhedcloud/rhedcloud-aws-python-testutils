"""
======================
AWS Elastic Transcoder
======================

Utilities for AWS Elastic Transcoder.

"""

from aws_test_functions import (
    aws_client,
    has_status,
    ignore_errors,
    unexpected,
)


@ignore_errors
@aws_client('elastictranscoder')
def cleanup(*, elastictranscoder):
    """Clean up any resources left behind by previous tests."""

    cleanup_pipelines(elastictranscoder=elastictranscoder)


@aws_client('elastictranscoder')
def cleanup_pipelines(*, elastictranscoder):
    """Clean up any pipelines left behind by previous tests."""

    print('Cleaning up Elastic Transcoder pipelines...')
    res = elastictranscoder.list_pipelines()
    for pipeline in res['Pipelines']:
        ignore_errors(delete_pipeline)(
            pipeline['Id'],
            elastictranscoder=elastictranscoder,
        )


@aws_client('elastictranscoder')
def delete_pipeline(pipeline_id, *, elastictranscoder):
    """Delete an Elastic Transcoder pipeline.

    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.

    """

    cancel_pipeline_jobs(pipeline_id, elastictranscoder=elastictranscoder)

    print('Deleting Elastic Transcoder pipeline: {}'.format(pipeline_id))
    res = elastictranscoder.delete_pipeline(Id=pipeline_id)
    assert has_status(res, 202), unexpected(res)


@aws_client('elastictranscoder')
def cancel_pipeline_jobs(pipeline_id, *, elastictranscoder):
    """Cancel any jobs for a given Elastic Transcoder pipeline.

    :param str pipeline_id:
        ID of an Elastic Transcoder pipeline.

    """

    res = elastictranscoder.list_jobs_by_pipeline(
        PipelineId=pipeline_id,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'Jobs' in res, unexpected(res)

    for job in res['Jobs']:
        if job['Status'] != 'Submitted':
            continue

        print('Canceling job {} for pipeline {}'.format(job['Id'], pipeline_id))
        ignore_errors(elastictranscoder.cancel_job)(Id=job['Id'])
