"""
===============
AWS API Gateway
===============

Utilities for AWS API Gateway.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .utils import debug, has_status, make_identifier, unexpected


@contextmanager
@aws_client('apigateway')
def new_rest_api(prefix, *, apigateway):
    """Context manager to create an API for AWS API Gateway and remove it
    when it is no longer used.

    :yields:
        An API Gateway Name

    """

    name = make_identifier(prefix)
    api_id = None

    try:
        res = apigateway.create_rest_api(name=name)
        assert has_status(res, 201), unexpected(res)

        api_id = res['id']
        debug('Created API Gateway REST API: {}'.format(api_id))

        yield api_id
    finally:
        if api_id is not None:
            debug('Removing API Gateway REST API: {}'.format(api_id))
            res = apigateway.delete_rest_api(restApiId=api_id)
            assert has_status(res, 202), unexpected(res)
