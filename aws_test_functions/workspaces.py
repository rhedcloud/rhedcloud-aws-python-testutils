"""
==============
AWS WorkSpaces
==============

"""

from contextlib import contextmanager

from .decorators import aws_client
from .directoryservice import new_simple_directory
from .lib import ClientError


@aws_client('workspaces')
def get_bundle_id(name, *, workspaces):
    """Find the ID of a bundle to use when creating a Workspace.

    :param str name:
        Name of the bundle to find.

    :returns:
        A string.

    """

    res = workspaces.describe_workspace_bundles(
        Owner='AMAZON',
    )
    assert 'Bundles' in res, 'Unexpected response: {}'.format(res)

    for bundle in res['Bundles']:
        if bundle['Name'] == name:
            return bundle['BundleId']


@contextmanager
@aws_client('ds')
def new_directory_id(vpc_id, subnets=None, *, ds):
    """Context manager to create and automatically clean up a Simple Directory
    service in the specified VPC.

    :param str vpc_id:
        ID of a VPC.
    :param list subnets: (optional)
        A list of subnet IDs where the service will be setup. If ommitted, the
        two public subnets are used.

    :yields:
        A string. The ID of the created directory.

    """

    with new_simple_directory('test.workspaces.com', vpc_id, subnets, wait=False, ds=ds) as did:
        yield did


@contextmanager
@aws_client('workspaces')
def new_workspace_id(directory_id, user, *, workspaces):
    """Context manager to create and automatically clean up a WorkSpace using
    the specified Simple Directory.

    :param str directory_id:
        ID of a Simple AWS Directory service.
    :param IAM.User user:
        An IAM User resource to use when setting up the WorkSpace.

    :yields:
        A string. The ID of the created WorkSpace.

    """

    workspace_id = None
    params = dict(
        Workspaces=[dict(
            DirectoryId=directory_id,
            UserName=user.user_name,
            BundleId=get_bundle_id('Value with Windows 10'),
        )],
    )

    try:
        res = workspaces.create_workspaces(**params)
        assert 'PendingWorkspaces' in res, 'Unexpected response: {}'.format(res)
        assert len(res['PendingWorkspaces']), 'Unexpected response: {}'.format(res)

        ws = res['PendingWorkspaces'][0]
        workspace_id = ws['WorkspaceId']
        print('Created workspace: {}'.format(workspace_id))

        yield workspace_id
    finally:
        if workspace_id is not None:
            try:
                delete_workspace(workspace_id, workspaces=workspaces)
            except ClientError as ex:
                print('Error deleting workspace: {}'.format(ex))


@aws_client('workspaces')
def delete_workspace(workspace_id, *, workspaces):
    """Delete a WorkSpace.

    :param str workspace_id:
        ID of the workspace to delete.

    """

    print('Deleting workspace: {}'.format(workspace_id))
    workspaces.terminate_workspaces(
        TerminateWorkspaceRequests=[dict(
            WorkspaceId=workspace_id,
        )],
    )
