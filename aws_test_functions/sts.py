"""
==========================
AWS Security Token Service
==========================

Utilities to interact with STS.

"""

from contextlib import ContextDecorator
import os

import boto3

from aws_test_functions import (
    aws_client,
    debug,
    has_status,
    make_identifier,
    orgs,
    unexpected,
)

# ASSUME_ROLE = os.getenv("RHEDCLOUD_ASSUME_ROLE", "role/OrganizationAccountAccessRole")
ASSUME_ROLE = os.getenv("RHEDCLOUD_ASSUME_ROLE", "role/rhedcloud/RHEDcloudMaintenanceOperatorRole")
ASSUME_ROLE_REGION = os.getenv("RHEDCLOUD_ASSUME_ROLE_REGION", "us-east-1")


@aws_client("sts")
@aws_client("organizations")
def get_target_account_session(account, role_session_prefix, *, sts, organizations, assume_role=None, region_name=ASSUME_ROLE_REGION):
    """Gain access to the target AWS account from a master account session.

    :param str account:
        The ID of the target AWS account.
    :param str role_session_prefix:
        Prefix to give to the role session.
    :param str assume_role: (optional)
        IAM Role to assume in the target account.
    :param str region_name: (optional)
        Name of the AWS region for the session.

    :returns:
        A Session object configured to use the AWS API within the context of
        the target AWS account.

    """

    if isinstance(account, str) and not account.isdigit():
        account = orgs.lookup_account(account, organizations=organizations)["Id"]

    if assume_role is None:
        assume_role = ASSUME_ROLE

    arn = "arn:aws:iam::{}:{}".format(account, assume_role)
    session_name = make_identifier(role_session_prefix)

    debug("Assuming role {} from {} (session name: {})".format(
        arn, sts.get_caller_identity()['Arn'], session_name,
    ))
    res = sts.assume_role(
        RoleArn=arn,
        RoleSessionName=session_name,
    )
    assert has_status(res, 200), unexpected(res)

    creds = res["Credentials"]
    session = boto3.Session(
        aws_access_key_id=creds["AccessKeyId"],
        aws_secret_access_key=creds["SecretAccessKey"],
        aws_session_token=creds["SessionToken"],
        region_name=region_name,
    )

    return session


class target_account_access(ContextDecorator):
    """Reusable context manager to temporarily set the appropriate credential
    information for the target AWS account.

    """

    def __init__(self, account, session_name, *, sts=None, assume_role=None, region_name=None):
        self.account = account
        self.session_name = session_name
        self.client = sts
        self.assume_role = assume_role
        self.region_name = region_name or ASSUME_ROLE_REGION

        self._prior_session = None
        self._target_session = None

    def __enter__(self):
        self._prior_session = self.client or boto3.DEFAULT_SESSION

        # setup a session for the target account
        debug("Switching to target account ({}) session...".format(self.account))

        if self._target_session is None:
            self._target_session = get_target_account_session(
                self.account,
                self.session_name,
                sts=self.client,
                assume_role=self.assume_role,
                region_name=self.region_name,
            )

        boto3.DEFAULT_SESSION = self._target_session

        return self._target_session

    def __exit__(self, exc_type, exc_value, traceback):
        # revert to the original session
        debug("Switching to original session...")
        boto3.DEFAULT_SESSION = self._prior_session
