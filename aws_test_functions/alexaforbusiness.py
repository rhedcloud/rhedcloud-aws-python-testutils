"""
======================
AWS Alexa For Business
======================

Utilities for Alexa For Business.

"""

from .decorators import aws_client, ignore_errors
from .utils import debug, has_status, unexpected

_alexa = aws_client('alexa', 'alexaforbusiness')


@_alexa
def cleanup_users(*, alexa):
    """Remove all Alexa for Business users."""

    delete = ignore_errors(delete_user)

    res = alexa.search_users()
    assert has_status(res, 200), unexpected(res)

    users = res.get('Users', [])
    for user in users:
        delete(user['UserArn'], user['EnrollmentId'], alexa=alexa)


@_alexa
def delete_user(user_arn, enrollmment_id, *, alexa):
    """Delete an Alexa for Business user.

    :param str user_arn:
        ARN of the user to delete.
    :param str enrollment_id:
        User's enrollment ID.

    """

    debug('Deleting user: {}'.format(user_arn))
    res = alexa.delete_user(
        UserArn=user_arn,
        EnrollmentId=enrollmment_id,
    )
    assert has_status(res, 200), unexpected(res)
