"""
==================
AWS SecretsManager
==================

Utilities for AWS SecretsManager.

"""

from contextlib import contextmanager

from .decorators import aws_client, ignore_errors
from .utils import (
    has_status,
    make_identifier,
    unexpected,
)


@contextmanager
@aws_client('secretsmanager')
def new_secret(prefix, description, secret_string, *, secretsmanager):
    """Context manager to create a secret and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to give the secret when generating a name

    :yields:
        A secret ARN

    """

    name = make_identifier(prefix)

    try:
        arn = create_secret(name, description, secretsmanager=secretsmanager)
        yield arn
    finally:
        if arn is not None:
            delete_secret(arn, secretsmanager=secretsmanager)


@aws_client('secretsmanager')
def create_secret(name, description, secret_string, *, secretsmanager):
    """Create a new secret.

    :param str name:
        Name of the secret.
    :param str description:
        Description of the secret.
    :param str secret_string:
        The secret string to store.

    :returns:
        A string.

    """

    res = secretsmanager.create_secret(
        Name=name,
        Description=description,
        SecretString=secret_string
    )
    assert has_status(res, 200), unexpected(res)
    assert 'ARN' in res, unexpected(res)
    arn = res['ARN']

    print('Created Secret with name: {} and ARN {}'.format(name, arn))
    return arn


@aws_client('secretsmanager')
def delete_secret(arn, *, secretsmanager):
    """Delete a secret.

    :param str arn:
        The ARN of the secret.

    """

    print('Deleting Secret: {}'.format(arn))
    res = secretsmanager.delete_secret(SecretId=arn, RecoveryWindowInDays=7)  # Minimum
    assert has_status(res, 200), unexpected(res)


@ignore_errors
@aws_client('secretsmanager')
def cleanup_secrets(*, secretsmanager):
    """Attempt to remove any unused data pipelines.

    """

    res = secretsmanager.list_secrets()
    if 'Secrets' in res:
        for secret in res['Secrets']:
            delete_secret(secret['ARN'], secretsmanager=secretsmanager)
