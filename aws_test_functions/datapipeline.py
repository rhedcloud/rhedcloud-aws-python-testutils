"""
================
AWS DataPipeline
================

Utilities for AWS DataPipeline.

"""

from contextlib import contextmanager

from .decorators import aws_client, ignore_errors
from .lib import get_uuid
from .utils import debug, has_status, unexpected


@contextmanager
@aws_client('datapipeline')
def new_data_pipeline(prefix, *, datapipeline):
    """Context manager to create a Data Pipeline and remove it when it's
    no longer used.

    :yields:
        A DataPipeline ID

    """

    name = '{}-{}'.format(prefix, get_uuid())
    pipeline_id = None

    try:
        pipeline_id = create_pipeline(name, datapipeline=datapipeline)
        yield pipeline_id
    finally:
        if pipeline_id is not None:
            ignore_errors(delete_pipeline)(pipeline_id, datapipeline=datapipeline)


@aws_client('datapipeline')
def create_pipeline(name, *, datapipeline):
    """Create a new data pipeline.

    :param str name:
        Name of the pipeline.

    :returns:
        A string.

    """

    uid = '{}-uid'.format(name)
    res = datapipeline.create_pipeline(
        name=name,
        uniqueId=uid,
    )
    assert has_status(res, 200), unexpected(res)
    assert 'pipelineId' in res, unexpected(res)
    pipeline_id = res['pipelineId']

    debug('Created Data Pipeline with UID: {} and Pipe ID {}'.format(uid, pipeline_id))
    return pipeline_id


@aws_client('datapipeline')
def delete_pipeline(pipeline_id, *, datapipeline):
    """Delete a data pipeline.

    :param str name:
        Name of the pipeline.

    """

    debug('Deleting Data Pipeline ID: {}'.format(pipeline_id))
    res = datapipeline.delete_pipeline(pipelineId=pipeline_id)
    assert has_status(res, 200), unexpected(res)


@ignore_errors
@aws_client('datapipeline')
def cleanup_pipelines(*, datapipeline):
    """Attempt to remove any unused data pipelines.

    """

    res = datapipeline.list_pipelines()
    for pipeline in res['pipelineIdList']:
        delete_pipeline(pipeline['id'], datapipeline=datapipeline)
