"""
======================
AWS Snowball Utilities
======================

This module contains helpers for the AWS Snowball service.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .kms import get_or_create_kms_key_arn

_SAMPLE_ADDRESS = dict(
    Name='test_snowball_address',
    Street1='123 Test St',
    City='Atlanta',
    StateOrProvince='GA',
    PhoneNumber='(101) 101-1010',
    Country='United States',
    PostalCode='31605',
)


@aws_client('snowball')
def get_or_create_address(*, snowball):
    """Return information about an address for use with Snowball jobs.

    Currently, addresses cannot be deleted, so we try to reuse any addresses
    already created. If no address already exists, a new address will be
    created.

    :returns:
        A dictionary.

    """

    addr = None
    for addr in snowball.describe_addresses()['Addresses']:
        print('Found address: {}'.format(addr['AddressId']))
        break

    if addr is None:
        addr = snowball.create_address(Address=_SAMPLE_ADDRESS)
        print('Created address: {}'.format(addr['AddressId']))

    assert addr is not None, 'failed to get or create address'
    return addr


@contextmanager
@aws_client('snowball')
def new_snowball_job(role, bucket_name, *args, snowball, **kwargs):
    """Context manager to create a new Snowball job. Once the context block is
    done, the job is canceled to avoid charges.

    :param IAM.Role role:
        IAM Role for Snowball to use when exporting data.
    :param str bucket_name:
        Name of the S3 bucket to export.

    :yields:
        The Snowball Job ID.

    """

    job_id = None

    try:
        job_id = create_snowball_job(role, bucket_name, *args, **kwargs, snowball=snowball)
        yield job_id
    finally:
        if job_id is not None:
            # immediately cancel the snowball job to avoid charges
            cancel_snowball_job(job_id, snowball=snowball)


@aws_client('snowball')
def create_snowball_job(role, bucket_name, snowball_type=None, address_id=None, key_arn=None, *, snowball):
    """Create a Snowball Job and return its Job ID.

    :param IAM.Role role:
        IAM Role for Snowball to use when exporting data.
    :param str bucket_name:
        Name of the S3 bucket to export.
    :param str snowball_type: (optional)
        Type of AWS Snowball appliance to use for this job. Choices are
        `STANDARD` and `EDGE`. If omitted, `STANDARD` is used.
    :param str address_id: (optional)
        ID of an Address to which the Snowball shall be sent.
    :param str key_arn: (optional)
        ARN of the KMS key that will be used to encrypt the snowball data.

    :rtype: str
    :returns:
        The Snowball Job ID.

    """

    if address_id is None:
        address_id = get_or_create_address(snowball=snowball)['AddressId']

    if key_arn is None:
        key_arn = get_or_create_kms_key_arn()

    if snowball_type is None:
        snowball_type = 'STANDARD'

    assert snowball_type in ('STANDARD', 'EDGE')

    params = dict(
        AddressId=address_id,
        KmsKeyARN=key_arn,
        RoleARN=role.arn,
        Description='Test Snowball',
        JobType='EXPORT',
        Resources={
            'S3Resources': [{
                'BucketArn': 'arn:aws:s3:::{}'.format(bucket_name),
            }]
        },
        ShippingOption='SECOND_DAY',
        SnowballType=snowball_type,
    )

    if snowball_type == 'EDGE':
        params['SnowballCapacityPreference'] = 'T100'

    # create a snowball job to export from the S3 bucket
    response = snowball.create_job(**params)

    job_id = response.get('JobId')
    assert job_id is not None
    print('Created Snowball job: {}'.format(job_id))

    return job_id


@aws_client('snowball')
def cancel_snowball_job(job_id, *, snowball):
    """Cancel a Snowball Job.

    :param str job_id:
        ID of the Snowball Job to cancel.

    """

    cancel = snowball.cancel_job(JobId=job_id)
    assert isinstance(cancel, dict), 'Failed to cancel Snowball Job {}'.format(job_id)
    print('Canceled Snowball job: {}'.format(job_id))
