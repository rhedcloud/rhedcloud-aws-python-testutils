"""
=================================
AWS Elastic File System Utilities
=================================

"""

from contextlib import contextmanager

from .decorators import aws_client, catch
from .lib import ClientError, get_uuid
from .utils import retry


@contextmanager
@aws_client('efs')
def new_filesystem(encrypted, *, efs):
    """Context manager to create a new filesystem and automatically remove it
    when it's no longer required.

    :param bool encrypted:
        Whether or not the filesystem should be encrypted.

    :yields:
        A string: the ID of the created filesystem.

    """

    fs_id = None

    try:
        fs_id = create_file_system(encrypted, efs=efs)
        yield fs_id
    finally:
        if fs_id is not None:
            delete_file_system(fs_id, efs=efs)


@aws_client('efs')
def create_file_system(encrypted, *, efs):
    """Create a new filesystem.

    :param bool encrypted:
        Whether or not the filesystem should be encrypted.

    :returns:
        A string: the ID of the created filesystem.

    """

    token = get_uuid()

    res = efs.create_file_system(
        CreationToken=token,
        Encrypted=encrypted,
    )
    assert 'FileSystemId' in res, 'Unexpected response: {}'.format(res)
    fs_id = res['FileSystemId']
    print('Created filesystem: {}'.format(fs_id))

    return fs_id


@catch(Exception, msg='Failed to delete filesystem: {ex}')
@aws_client('efs')
def delete_file_system(filesystem_id, *, efs):
    """Delete a filesystem.

    :param str filesystem_id:
        The ID of the filesystem to delete.

    """

    # first try to remove any mount targets that use the specified filesystem
    targets = efs.describe_mount_targets(FileSystemId=filesystem_id)
    for target in targets.get('MountTargets', []):
        delete_mount_target(target['MountTargetId'], efs=efs)

    retry(
        efs.delete_file_system,
        kwargs=dict(
            FileSystemId=filesystem_id,
        ),
        msg='Deleting filesystem: {}'.format(filesystem_id),
        exceptions=ClientError,
        pred=lambda ex: 'FileSystemInUse' in str(ex),
    )


@catch(Exception, msg='Failed to cleanup file systems: {ex}')
@aws_client('efs')
def cleanup_file_systems(*, efs):
    """Attempt to delete any unused filesystems."""

    res = efs.describe_file_systems()
    for fs in res['FileSystems']:
        delete_file_system(fs['FileSystemId'], efs=efs)


@contextmanager
@aws_client('efs')
def new_mount_target(filesystem_id, subnet_id, *, efs):
    """Context manager to create a new filesystem mount target and
    automatically remove it when it's no longer required.

    :param str filesystem_id:
        ID of the filesystem to create a mount target for.
    :param str subnet_id:
        ID of a subnet where the mount target shall be available.

    :yields:
        A string: the ID of the created mount target.

    """

    target_id = None

    try:
        target_id = create_mount_target(filesystem_id, subnet_id, efs=efs)
        yield target_id
    finally:
        if target_id:
            delete_mount_target(target_id, efs=efs)


@aws_client('efs')
def create_mount_target(filesystem_id, subnet_id, *, efs):
    """Create a new filesystem mount target.

    :param str filesystem_id:
        ID of the filesystem to create a mount target for.
    :param str subnet_id:
        ID of a subnet where the mount target shall be available.

    :returns:
        A string: the ID of the created mount target.

    """

    res = retry(
        efs.create_mount_target,
        kwargs=dict(
            FileSystemId=filesystem_id,
            SubnetId=subnet_id,
        ),
        msg='Attempting to create mount target',
        exceptions=ClientError,
        pred=lambda ex: 'IncorrectFileSystemLifeCycleState' in str(ex),
    )
    assert 'MountTargetId' in res, 'Unexpected response: {}'.format(res)
    target_id = res['MountTargetId']
    print('Created mount target for filesystem {}: {}'.format(filesystem_id, target_id))

    return target_id


@catch(Exception, msg='Failed to delete mount target: {ex}')
@aws_client('efs')
def delete_mount_target(target_id, *, efs):
    """Delete a mount target.

    :param str target_id:
        The ID of the mount target to delete.

    """

    print('Deleting mount target: {}'.format(target_id))
    efs.delete_mount_target(MountTargetId=target_id)
