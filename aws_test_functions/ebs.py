"""
=========================
AWS Elastic Block Storage
=========================

AWS Elastic Block Storage utilities.

"""

from contextlib import contextmanager

import boto3

from .decorators import aws_client
from .lib import get_uuid

@contextmanager
@aws_client('ec2')
def new_ebs_snapshot(volume_id: str,*,ec2,**kwargs):
    """Context manager to create a new EBS snapshot and delete it when the block
    ends.

    :param str volume_id:
        The volume from which to create the snapshot.

    :yields:
        A Snapshot resource.

    """
    snapshot = None

    try:
        snapshot = create_ebs_snapshot(volume_id, ec2=ec2, **kwargs)
        yield snapshot
    finally:
        if snapshot is not None:
            print('Deleting EBS snapshot: {}'.format(snapshot))
            snapshot.delete()



@contextmanager
@aws_client('ec2')
def new_ebs_volume(prefix: str, az: str, size: int = 10, encrypted: bool = True, *, ec2, **kwargs):
    """Context manager to create a new EBS volume and delete it when the block
    ends.

    :param str prefix:
        Prefix to use when creating a random name for the volume.
    :param str az:
        Availability Zone ID where the volume should be created.
    :param int size: (optional)
        Size (in GB) of the volume to create.
    :param bool encrypted: (optional)
        Whether the volume should be encrypted.

    :yields:
        A Volume resource.

    """

    volume = None
    name = '{}-{}'.format(prefix, get_uuid())

    try:
        volume = create_ebs_volume(name, az, size, encrypted, ec2=ec2, **kwargs)
        yield volume
    finally:
        if volume is not None:
            print('Deleting EBS volume: {}'.format(name))
            volume.delete()


@aws_client('ec2')
def create_ebs_snapshot(volume_id: str, *, ec2, **kwargs):
    """Create a new EBS snapshot.

    :param str volume_id:
        The volume from which to create the snapshot

    :returns:
        A Snapshot resource.

    Description='string',
    VolumeId='string',
    TagSpecifications=[
        {
            'ResourceType': 'client-vpn-endpoint'|'customer-gateway'|'dedicated-host'|'dhcp-options'|'elastic-ip'|'fleet'|'fpga-image'|'host-reservation'|'image'|'instance'|'internet-gateway'|'key-pair'|'launch-template'|'natgateway'|'network-acl'|'network-interface'|'placement-group'|'reserved-instances'|'route-table'|'security-group'|'snapshot'|'spot-fleet-request'|'spot-instances-request'|'subnet'|'traffic-mirror-filter'|'traffic-mirror-session'|'traffic-mirror-target'|'transit-gateway'|'transit-gateway-attachment'|'transit-gateway-multicast-domain'|'transit-gateway-route-table'|'volume'|'vpc'|'vpc-peering-connection'|'vpn-connection'|'vpn-gateway'|'vpc-flow-log',
            'Tags': [
                {
                    'Key': 'string',
                    'Value': 'string'
                },
            ]
        },
    ],
    DryRun=True|False

    """

    kwargs.update({
        'Description': 'Created by aws_test_functions. If you see me, delete me.',
        'VolumeId': volume_id,
        'TagSpecifications': [{
            'ResourceType': 'snapshot',
            'Tags': [{
                'Key': 'Name',
                'Value': '{}-{}'.format('Test_Snap', get_uuid()),
            }],
        }],
        'DryRun': False,
    })

    res = ec2.create_snapshot(**kwargs)
    print('Created EBS snapshot: {}'.format(res))

    return boto3.resource('ec2').Snapshot(res['SnapshotId'])


@aws_client('ec2')
def create_ebs_volume(name: str, az: str, size: int = 10, encrypted: bool = True, *, ec2, **kwargs):
    """Create a new EBS volume.

    :param str name:
        Name for the new volume.
    :param str az:
        Availability Zone ID where the volume should be created.
    :param int size: (optional)
        Size (in GB) of the volume to create.
    :param bool encrypted: (optional)
        Whether the volume should be encrypted.

    :returns:
        A Volume resource.

    """

    assert encrypted is True or encrypted is False

    kwargs.update({
        'AvailabilityZone': az,
        'Encrypted': encrypted,
        'Size': size,
        'TagSpecifications': [{
            'ResourceType': 'volume',
            'Tags': [{
                'Key': 'Name',
                'Value': name,
            }],
        }],
    })

    res = ec2.create_volume(**kwargs)
    print('Created EBS volume: {}'.format(name))

    return boto3.resource('ec2').Volume(res['VolumeId'])
