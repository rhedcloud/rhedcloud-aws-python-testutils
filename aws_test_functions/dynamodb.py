"""
======================
AWS DynamoDB Utilities
======================

Shared utilities for AWS DynamoDB.

"""

from .decorators import aws_client, catch


@aws_client('dynamodb')
def create_table(name, *, dynamodb):
    """Create a new DynamoDB table.

    :param str name:
        Name of the table.

    :returns:
        A dictionary with information about the created DynamoDB table.

    """

    res = dynamodb.create_table(
        TableName=name,
        AttributeDefinitions=[{
            'AttributeName': 'test_attr',
            'AttributeType': 'S',
        }],
        KeySchema=[{
            'AttributeName': 'test_attr',
            'KeyType': 'HASH',
        }],
        ProvisionedThroughput={
            'ReadCapacityUnits': 1,
            'WriteCapacityUnits': 1,
        },
    )
    assert 'TableDescription' in res, 'Unexpected response: {}'.format(res)
    print('Waiting for DynamoDB table: {}'.format(name))

    waiter = dynamodb.get_waiter('table_exists')
    waiter.wait(TableName=name)

    # refresh the table data after it's fully created
    tbl = describe_table(name, dynamodb=dynamodb)
    print('Created DynamoDB table: {}'.format(name))

    return tbl


@aws_client('dynamodb')
def describe_table(name, *, dynamodb):
    """Get details about the named DynamoDB table.

    :param str name:
        Name of a DynamoDB table.

    :returns:
        A dictionary with information about the created DynamoDB table.

    """

    res = dynamodb.describe_table(TableName=name)
    assert 'Table' in res, 'Unexpected response: {}'.format(res)

    return res['Table']


@aws_client('dynamodb')
def put_item(table_name, *, dynamodb, **kwargs):
    """Helper for putting data into a DynamoDB table.

    :param str table_name:
        Name of a DynamoDB table.

    :returns:
        A dictionary.

    Usage:

    .. code-block:: python

        res = put_item('table_name', key1='value', key2='another value')

    .. note::

        This currently only supports String values.

    """

    res = dynamodb.put_item(
        TableName=table_name,
        Item={
            key: {'S': value}
            for key, value in kwargs.items()
        },
        ReturnConsumedCapacity='TOTAL',
    )
    assert 'ConsumedCapacity' in res, 'Unexpected response: {}'.format(res)

    return res


@catch(Exception, msg='Failed to delete DynamoDB table: {ex}')
@aws_client('dynamodb')
def delete_table(name, *, dynamodb):
    """Delete an active DynamoDB table.

    :param str name:
        Name of a DynamoDB table.

    """

    table = describe_table(name, dynamodb=dynamodb)
    if table['TableStatus'] == 'ACTIVE':
        print('Deleting DynamoDB table: {}'.format(name))
        res = dynamodb.delete_table(TableName=name)
        assert 'TableDescription' in res, 'Unexpected response: {}'.format(res)
