"""
================
AWS MediaPackage
================

Utilities for AWS MediaPackage.

"""

from contextlib import contextmanager

from .decorators import aws_client, ignore_errors
from .lib import get_uuid
from .ssm import new_ssm_parameter
from .utils import has_status


@contextmanager
@aws_client('mediapackage')
def new_mediapackage_channel(prefix, *, mediapackage):
    """Context manager to create a MediaPackage channel and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the channel's Id.

    :yields:
        A MediaPackage channel Id

    """

    channel_id = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        print('Creating MediaPackage Channel: {}'.format(channel_id))
        res = mediapackage.create_channel(Id=channel_id)
        assert has_status(res, 200, 202)
        yield channel_id
    finally:
        if res is not None:
            print('Deleting MediaPackage Channel: {}'.format(channel_id))
            res = ignore_errors(mediapackage.delete_channel)(Id=channel_id)
            assert has_status(res, 200, 202)


@contextmanager
@aws_client('mediapackage')
def new_origin_endpoint(prefix, channel_id, *, mediapackage):
    """Context manager to create a MediaPackage origin endpoint and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the origin endpoint's ID.
    :param str channel_id:
        The channel ID to associate with the endpoint.

    :yields:
        A MediaPackage origin endpoint ID

    """

    endpoint_id = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        print('Creating MediaPackage Origin Endpoint: {}'.format(endpoint_id))
        res = mediapackage.create_origin_endpoint(
            ChannelId=channel_id,
            Id=endpoint_id,
            HlsPackage={},
        )
        assert has_status(res, 200, 202)
        yield endpoint_id
    finally:
        if res is not None:
            print('Deleting MediaPackage Origin Endpoint: {}'.format(endpoint_id))
            res = ignore_errors(mediapackage.delete_origin_endpoint)(Id=endpoint_id)
            assert has_status(res, 200, 202)


@aws_client('mediapackage')
def create_channel_and_endpoint(suff, stack, *, mediapackage):
    """Create a channel, endpoint, and set of channel credentials.

    :param str suff:
        Suffix to give to each created resource.
    :param contextlib.ExitStack stack:
        A context manager shared across all MediaPackage tests.

    :returns:
        A dictionary.

    """

    channel_name = 'TestChannel{}'.format(suff)
    channel_id = stack.enter_context(new_mediapackage_channel(channel_name))

    res = mediapackage.describe_channel(Id=channel_id)
    endpoint = res['HlsIngest']['IngestEndpoints'][0]
    url = endpoint['Url']
    user = endpoint['Username']
    password = endpoint['Password']

    endpoint_name = 'TestOriginEndpoint{}'.format(suff)
    stack.enter_context(new_origin_endpoint(
        endpoint_name,
        channel_id=channel_id,
    ))

    cred_name = 'Channel{}Creds'.format(suff)
    channel_creds = stack.enter_context(new_ssm_parameter(
        cred_name,
        password,
        'SecureString',
    ))

    return dict(
        Username=user,
        PasswordParam=channel_creds,
        Url=url,
    )


@aws_client('mediapackage')
def create_channels_and_endpoints(stack, *, mediapackage):
    """Create two channels, endpoints, and set of channel credentials.

    :param contextlib.ExitStack stack:
        A context manager shared across all MediaPackage tests.

    :returns:
        A dictionary.

    """

    return [
        create_channel_and_endpoint(suff, stack, mediapackage=mediapackage)
        for suff in ('One', 'Two')
    ]


@aws_client('mediapackage')
def cleanup(*, mediapackage):
    cleanup_endpoints()
    cleanup_channels()


@aws_client('mediapackage')
def cleanup_endpoints(*, mediapackage):
    """Attempt to remove any unused endpoints."""

    res = mediapackage.list_origin_endpoints()
    for endpoint in res['OriginEndpoints']:
        name = endpoint['Id']
        print('Deleting endpoint: {}'.format(name))
        mediapackage.delete_origin_endpoint(Id=name)


@aws_client('mediapackage')
def cleanup_channels(*, mediapackage):
    """Attempt to remove any unused channels."""

    res = mediapackage.list_channels()
    for channel in res['Channels']:
        name = channel['Id']
        print('Deleting channel: {}'.format(name))
        mediapackage.delete_channel(Id=name)
