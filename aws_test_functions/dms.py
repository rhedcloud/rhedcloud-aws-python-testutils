"""
==============================
AWS Database Migration Service
==============================

"""

from contextlib import contextmanager
import json

from .decorators import aws_client, catch
from .lib import get_uuid
from .utils import dict_to_filters


@contextmanager
@aws_client('dms')
def new_replication_subnet_group(prefix, subnets, *, dms):
    """Context manager to create a new replication subnet group and attempt to
    remove it when we're done with it.

    :param str prefix:
        Prefix to use with the replication subnet group's name.
    :param list subnets:
        List of subnet IDs.

    :yields:
        A dictionary with information about the replication subnet group.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    sng = None

    try:
        sng = create_replication_subnet_group(name, subnets, dms=dms)
        yield sng
    finally:
        if sng is not None:
            delete_replication_subnet_group(name, dms=dms)


@aws_client('dms')
def create_replication_subnet_group(name, subnets, *, dms):
    """Create a new replication subnet group.

    :param str name:
        Name to give the replication subnet group.
    :param list subnets:
        List of subnet IDs.

    :returns:
        A dictionary with information about the replication subnet group.

    """

    res = dms.create_replication_subnet_group(
        ReplicationSubnetGroupIdentifier=name,
        ReplicationSubnetGroupDescription=name,
        SubnetIds=subnets,
    )
    assert 'ReplicationSubnetGroup' in res, 'Unexpected response: {}'.format(res)
    sng = res['ReplicationSubnetGroup']
    print('Created replication subnet group: {}'.format(name))

    return sng


@catch(Exception, msg='Failed to delete replication subnet group: {ex}')
@aws_client('dms')
def delete_replication_subnet_group(identifier, *, dms):
    """Delete a replication subnet group.

    :param str identifier:
        Identifier for the replication subnet group.

    """

    print('Deleting replication subnet group: {}'.format(identifier))
    dms.delete_replication_subnet_group(
        ReplicationSubnetGroupIdentifier=identifier,
    )


@contextmanager
@aws_client('dms')
def new_endpoint(prefix, *args, dms, **kwargs):
    """Context manager to create a new replication endpoint and attempt to
    clean it up when we're done with it.

    :param str prefix:
        Prefix to use with the endpoint's name.

    :yields:
        A dictionary with endpoint information.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    endpoint = None

    try:
        endpoint = create_endpoint(name, *args, dms=dms, **kwargs)
        yield endpoint
    finally:
        if endpoint is not None:
            delete_endpoint(endpoint['EndpointArn'], dms=dms)


@aws_client('dms')
def create_endpoint(name, endpoint_type, engine, db, *, dms):
    """Create a new replication endpoint.

    :param str name:
        Name for the endpoint.
    :param str endpoint_type:
        Type of endpoint, such as ``source`` or ``target``.
    :param str engine:
        Database engine for the endpoint.
    :param dict db:
        Information about an RDS instance.

    :returns:
        A dictionary with endpoint information.

    """

    res = dms.create_endpoint(
        EndpointIdentifier=name,
        EndpointType=endpoint_type,
        EngineName=engine,
        Username=db['MasterUsername'],
        Password=db['MasterUserPassword'],
        ServerName=db['Endpoint']['Address'],
        Port=db['Endpoint']['Port'],
    )
    assert 'Endpoint' in res, 'Unexpected response: {}'.format(res)
    endpoint = res['Endpoint']
    print('Created endpoint: {}'.format(name))

    return endpoint


@catch(Exception, msg='Failed to delete endpoint: {ex}')
@aws_client('dms')
def delete_endpoint(arn, *, dms):
    """Delete a replication endpoint.

    :param str arn:
        ARN of the endpoint to delete.

    """

    print('Deleting endpoint: {}'.format(arn))
    dms.delete_endpoint(EndpointArn=arn)


@contextmanager
@aws_client('dms')
def new_replication_instance(prefix, sng_id, *, dms):
    """Context manager to create a new replication instance and attempt to
    delete it when we're done with it.

    :param str prefix:
        Prefix to use with the replication instance's name.
    :param str sng_id:
        Replication subnet group ID.

    :yields:
        A dictionary with information about the replication instance.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    inst = None

    try:
        inst = create_replication_instance(name, sng_id, dms=dms)

        yield inst
    finally:
        if inst is not None:
            delete_replication_instance(inst['ReplicationInstanceArn'], dms=dms)


@aws_client('dms')
def create_replication_instance(name, sng_id, *, dms):
    """Create a new replication instance.

    :param str name:
        Replication instance name.
    :param str sng_id:
        Replication subnet group ID.

    :returns:
        A dictionary with information about the replication instance.

    """

    res = dms.create_replication_instance(
        ReplicationInstanceIdentifier=name,
        ReplicationInstanceClass='dms.t2.micro',
        ReplicationSubnetGroupIdentifier=sng_id,
        PubliclyAccessible=False,
    )
    assert 'ReplicationInstance' in res, 'Unexpected response: {}'.format(res)
    inst = res['ReplicationInstance']
    arn = inst['ReplicationInstanceArn']
    print('Created replication instance: {}'.format(arn))

    return inst


@catch(Exception, msg='Failed to delete replication instance: {ex}')
@aws_client('dms')
def delete_replication_instance(arn, *, dms):
    """Delete a replication instance.

    :param str arn:
        ARN of the instance to delete.

    """

    print('Deleting replication instance: {}'.format(arn))
    dms.delete_replication_instance(
        ReplicationInstanceArn=arn,
    )


@aws_client('dms')
def get_replication_instance_status(replicator_arn, *, dms):
    """Retrieve the status of the specified replication instance.

    :param str replicator_arn:
        ARN of the replication instance.

    :returns:
        A string.

    """

    filters = dict_to_filters({
        'replication-instance-arn': replicator_arn,
    })

    res = dms.describe_replication_instances(Filters=filters)
    assert 'ReplicationInstances' in res and len(res['ReplicationInstances']), \
        'Unexpected response: {}'.format(res)

    inst = res['ReplicationInstances'][0]
    # print('Found replication instance: {}'.format(inst))

    return inst['ReplicationInstanceStatus']


@contextmanager
@aws_client('dms')
def new_replication_task(prefix, *args, dms, **kwargs):
    """Context manager to create a new replication task and attempt to delete
    it when we're done with it.

    :param str prefix:
        Prefix to use with the replication task's name.

    :yields:
        A string: the ARN of the replication task.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    arn = None

    try:
        task = create_replication_task(name, *args, dms=dms, **kwargs)
        arn = task['ReplicationTaskArn']

        yield arn
    finally:
        if arn is not None:
            delete_replication_task(arn)


@aws_client('dms')
def create_replication_task(name, replicator_arn, source_endpoint_arn, target_endpoint_arn, *, dms):
    """Create a new replication task.

    :param str name:
        Name of the replication task.
    :param str replication_arn:
        ARN of the replication instance for the task.
    :param str source_endpoint_arn:
        ARN of the source endpoint.
    :param str target_endpoint_arn:
        ARN of the target endpoint.

    :returns:
        A string: the ARN of the replication task.

    """

    res = dms.create_replication_task(
        ReplicationTaskIdentifier=name,
        SourceEndpointArn=source_endpoint_arn,
        TargetEndpointArn=target_endpoint_arn,
        ReplicationInstanceArn=replicator_arn,
        MigrationType='full-load',
        TableMappings=json.dumps({
            'rules': [{
                'rule-id': '1',
                'rule-name': '1',
                'rule-type': 'selection',
                'object-locator': {
                    'schema-name': 'Test',
                    'table-name': '%',
                },
                'rule-action': 'include',
            }],
        }),
    )
    assert 'ReplicationTask' in res, 'Unexpected response: {}'.format(res)

    return res['ReplicationTask']


@catch(Exception, msg='Failed to delete replication task: {ex}')
@aws_client('dms')
def delete_replication_task(arn, *, dms):
    """Delete a replication task.

    :param str arn:
        ARN of the replication task.

    """

    print('Deleting replication task: {}'.format(arn))
    dms.delete_replication_task(ReplicationTaskArn=arn)


@aws_client('dms')
def get_replication_task(arn, *, dms):
    """Retrieve information about a replication task.

    :param str arn:
        ARN of the replication task.

    :returns:
        A dictionary.

    """

    filters = dict_to_filters({
        'replication-task-arn': arn,
    })

    res = dms.describe_replication_tasks(Filters=filters)
    assert 'ReplicationTasks' in res and len(res['ReplicationTasks']) == 1, \
        'Unexpected response: {}'.format(res)

    return res['ReplicationTasks'][0]


@aws_client('dms')
def get_replication_task_status(arn, *, dms):
    """Retrieve the status of a replication task.

    :param str arn:
        ARN of the replication task.

    :returns:
        A string.

    """

    return get_replication_task(arn, dms=dms)['Status']


@aws_client('dms')
def get_connection(endpoint_arn, *, dms):
    """Retrieve information about a connection between a replication instance
    and an endpoint.

    :param str endpoint_arn:
        ARN of the endpoint.

    :returns:
        A dictionary.

    """

    filters = dict_to_filters({
        'endpoint-arn': endpoint_arn,
    })

    res = dms.describe_connections(Filters=filters)
    assert 'Connections' in res and len(res['Connections']) == 1, \
        'Unexpected response: {}'.format(res)

    return res['Connections'][0]


@aws_client('dms')
def get_connection_status(endpoint_arn, *, dms):
    """Retrieve the status of a connection between a replication instance and
    an endpoint.

    :param str endpoint_arn:
        ARN of the endpoint.

    :returns:
        A string.

    """

    return get_connection(endpoint_arn, dms=dms)['Status']


@aws_client('dms')
def cleanup_replication_tasks(*, dms):
    """Remove any unused replication tasks."""

    res = dms.describe_replication_tasks()
    for task in res['ReplicationTasks']:
        delete_replication_task(task['ReplicationTaskArn'], dms=dms)


@aws_client('dms')
def cleanup_endpoints(*, dms):
    """Remove any unused endpoints."""

    res = dms.describe_endpoints()
    for endpoint in res['Endpoints']:
        delete_endpoint(endpoint['EndpointArn'], dms=dms)


@aws_client('dms')
def cleanup_replication_instances(*, dms):
    """Remove any unused replication instances."""

    res = dms.describe_replication_instances()
    for inst in res['ReplicationInstances']:
        delete_replication_instance(inst['ReplicationInstanceArn'], dms=dms)


@aws_client('dms')
def cleanup_replication_subnet_groups(*, dms):
    """Remove any unused replication subnet groups."""

    res = dms.describe_replication_subnet_groups()
    for sng in res['ReplicationSubnetGroups']:
        delete_replication_subnet_group(sng['ReplicationSubnetGroupIdentifier'], dms=dms)
