"""
===========
AWS Route53
===========

This module contains helpers for the AWS Route53.

"""

from contextlib import contextmanager

from .decorators import aws_client


@contextmanager
@aws_client('route53')
def new_record_set(zone_id, name, record_type, value, *, ttl=60, route53):
    """Context manager to automatically create a single DNS record and remove
    it when testing finishes.

    :param str zone_id:
        ID of the Hosted Zone the new DNS record will belong to.
    :param str name:
        Name of the DNS record.
    :param str record_type:
        DNS record type (A, CNAME, TXT, etc).
    :param str value:
        Value for the new DNS record.

    :yields:
        A dictionary.

    """

    params = dict(
        HostedZoneId=zone_id,
        ChangeBatch=dict(
            Changes=[dict(
                Action='CREATE',
                ResourceRecordSet=dict(
                    Name=name,
                    Type=record_type,
                    TTL=ttl,
                    ResourceRecords=[dict(
                        Value=value,
                    )],
                )
            )],
        ),
    )
    request_id = None

    try:
        res = route53.change_resource_record_sets(**params)
        assert 'ChangeInfo' in res, 'Unexpected response: {}'.format(res)
        request_id = res['ChangeInfo']['Id']
        print('Created record set: {}'.format(name))

        # make sure the update took
        res = route53.list_resource_record_sets(HostedZoneId=zone_id)
        assert 'ResourceRecordSets' in res, 'Unexpected response: {}'.format(res)

        yield res['ResourceRecordSets']
    finally:
        if request_id is not None:
            print('Deleting record set: {}'.format(name))
            params['ChangeBatch']['Changes'][0]['Action'] = 'DELETE'
            route53.change_resource_record_sets(**params)
