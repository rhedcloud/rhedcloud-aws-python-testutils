"""
===========================
AWS Organizations Utilities
===========================

This module contains helpers for AWS Organizations.

Environment Variables
=====================

There are several environment variables that can be set to change the behavior
of these utilities:

* :envvar:`RHEDCLOUD_AUTOMOVE_ACCOUNT`: When set, this instructs ``pytest`` to
  move the AWS account to the :envvar:`RHEDCLOUD_SETUP_ORG` before doing
  anything else.
* :envvar:`RHEDCLOUD_ACCOUNT_NAME`: Name of the AWS account that will be
  shuffled between AWS Organizational Units. Defaults to `Serviceforge 110`.
* :envvar:`RHEDCLOUD_OU_SWITCH_PAUSE`: Number of seconds to wait after moving
  an account from one OU to another. A delay is necessary in order for the
  Service Control Policies to take effect. Default is 5 seconds.
* :envvar:`RHEDCLOUD_SETUP_PROFILE`: Name of the AWS profile with credentials to
  move accounts between OUs. Default is `rhedcloud-serviceforge-100`.
* :envvar:`RHEDCLOUD_TEST_PROFILE`: Name of the AWS profile with credentials to
  access the account where test resources will be created and tests performed.
  Default is `rhedcloud-serviceforge-110`.
* :envvar:`RHEDCLOUD_SETUP_ORG`: Name of the OU where test resource will be
  created and destroyed. Default is `RHEDcloudAccountAdministrationOrg`.
* :envvar:`RHEDCLOUD_TEST_ORG`: Name of the OU where tests will be performed.
  Default is `RHEDcloudStandardAccountOrg`.

Organizational Units and AWS Account Shuffling
==============================================

The basic flow of the utilities contained herein is as follows:

.. figure:: _static/account_ou_swapping.png
   :target: _static/account_ou_swapping.large.png

#. As soon as ``pytest`` is launched, it encounters the session-wide pytest
   fixture called :py:func:`in_setup_org`. This fixture is set to automatically
   take effect based on whether the :envvar:`RHEDCLOUD_AUTOMOVE_ACCOUNT`
   environment variable is set.
#. The :py:func:`in_setup_org` fixture first calls :py:func:`move_to_setup_org`
   to ensure that the AWS account is in the `RHEDcloudAccountAdministrationOrg`
   OU.
#. Once the AWS account is in the correct OU, resources required for testing,
   such as test users or roles, can be created.
#. After the necessary resources are created, the :py:func:`move_to_test_org`
   function can be used to move the AWS account back to the
   `RHEDcloudStandardAccountOrg` OU.
#. Finally, the :py:func:`test_answer` test function is executed.
#. When the test function finishes, :py:func:`move_to_setup_org` may be invoked
   to move the AWS account back into the `RHEDcloudAccountAdministrationOrg` OU.
#. Once the AWS account is back in the `RHEDcloudAccountAdministrationOrg` OU,
   the resources previously setup for the test can be cleaned up.
#. After the necessary resources are cleaned up, additional resources may be
   setup for the next test function (resuming at step 3). If no other tests
   need to be executed, control returns to the :py:func:`in_setup_org`
   session-wide pytest fixture.
#. The :py:func:`in_setup_org` fixture calls :py:func:`move_to_test_org` to put
   the AWS account back where it belongs.
#. ``pytest`` exits.

.. attention::

    Notice that the two circles in the diagram above both happen within the
    `RHEDcloudAccountAdministrationOrg` OU, where there are fewer SCP
    restrictions.

"""

from contextlib import ContextDecorator
from functools import partial
import os
import time

import boto3

from .decorators import aws_client
from .utils import debug


# only enable this fixture when the `RHEDCLOUD_AUTOMOVE_ACCOUNT` environment
# variable is set
AUTOMOVE_ACCOUNT = bool(os.getenv('RHEDCLOUD_AUTOMOVE_ACCOUNT'))
ACCOUNT_NAME = os.getenv('RHEDCLOUD_ACCOUNT_NAME', 'Serviceforge 110')

OU_SWITCH_PAUSE = 5
try:
    OU_SWITCH_PAUSE = int(os.getenv('RHEDCLOUD_OU_SWITCH_PAUSE'))
except Exception:
    pass

SETUP_PROFILE = os.getenv('RHEDCLOUD_SETUP_PROFILE') or os.getenv("AWS_PROFILE") or 'rhedcloud-serviceforge-100'
TEST_PROFILE = os.getenv('RHEDCLOUD_TEST_PROFILE', 'rhedcloud-serviceforge-110')

SETUP_ORG = os.getenv('RHEDCLOUD_SETUP_ORG', 'RHEDcloudAccountAdministrationOrg')
TEST_ORG = os.getenv('RHEDCLOUD_TEST_ORG', 'RHEDcloudStandardAccountOrg')


class in_org(ContextDecorator):
    """This context manager temporarily moves the AWS account from one AWS
    Organizational Unit into another, executes the block, then moves the AWS
    account back into the original AWS Organizational Unit.

    Usage:

    .. code-block:: python

        @in_org("RHEDcloudAccountAdministrationOrg")
        def test_something():
            print("this is running in the account's RHEDcloudAccountAdministrationOrg org unit")

        def test_something():
            with in_setup_org("RHEDcloudAccountAdministrationOrg"):
                print("this is running in the account's RHEDcloudAccountAdministrationOrg org unit")

    :param str to_org:
        Name of the target Organizational Unit.
    :param str from_org: (optional)
        Name of the original Organizational Unit. If omitted, the account's
        current OU will be used.
    :param str account: (optional)
        Name or ID of the specific AWS account to move.
    :param str in_reason: (optional)
        The reason the account must be moved to the setup OU.
    :param str out_reason: (optional)
        The reason the account must be moved to the test OU.
    :param bool detect: (optional)
        Whether to detect if the account is already in the setup OU. If so, it
        will remain in the setup OU rather than forcibly being moved to the
        test OU. By default, this is disabled to have minimal impact on
        existing tests.
    :param bool one_way: (optional)
        Whether to bother moving the account back to the test OU.

    """

    def __init__(
        self,
        to_org,
        from_org=None,
        account=None,
        in_reason='setup',
        out_reason='cleanup',
        detect=False,
        one_way=False,
        *,
        organizations
    ):
        self.to_org = to_org
        self.from_org = from_org
        self.account = account or ACCOUNT_NAME

        self.in_reason = in_reason
        self.out_reason = out_reason
        self.detect = detect or not from_org
        self.one_way = one_way

        self.client = organizations

    def should_move(self):
        """Determine whether the account should move to the original OU upon
        exiting the context block.

        :returns:
            A boolean.

        """

        move = not self.one_way
        if move and self.detect and self.from_org == self.to_org:
            move = False

        return move

    def __enter__(self):
        if not self.client:
            self.client = _get_organizations_client()

        if self.detect:
            self.from_org = get_current_org_unit(self.account, organizations=self.client)

        move_to_org(
            self.account,
            self.to_org,
            self.from_org,
            reason=self.in_reason,
            organizations=self.client,
        )

        return self

    def __exit__(self, *exc):
        if self.should_move():
            move_to_org(
                self.account,
                self.from_org,
                self.to_org,
                reason=self.out_reason,
                organizations=self.client,
            )

        return False


class in_setup_org(ContextDecorator):
    """This context manager temporarily moves the AWS account into the
    ``SETUP_ORG`` AWS Organizational Unit, executes the block, then moves the
    AWS account back into the ``TEST_ORG`` AWS Organizational Unit. This relies
    on the following environment variables to determine which Organizational
    Units to switch between:

    * :envvar:`RHEDCLOUD_TEST_ORG`: the AWS Organizational Unit where tests
      will be executed, possibly with additional restrictions.
    * :envvar:`RHEDCLOUD_SETUP_ORG`: the AWS Organizational Unit with fewer
      restrictions, allowing for tests to be properly setup and tore down.

    Usage:

    .. code-block:: python

        @in_setup_org()
        def test_something():
            print("this is running in the account's setup org unit")

        def test_something():
            with in_setup_org():
                print("this is running in the account's setup org unit")

    :param str in_reason: (optional)
        The reason the account must be moved to the setup OU.
    :param str out_reason: (optional)
        The reason the account must be moved to the test OU.
    :param bool detect: (optional)
        Whether to detect if the account is already in the setup OU. If so, it
        will remain in the setup OU rather than forcibly being moved to the
        test OU. By default, this is disabled to have minimal impact on
        existing tests.
    :param bool one_way: (optional)
        Whether to bother moving the account back to the test OU.
    :param str setup_org: (optional)
        Name of the AWS Organizational Unit to use for administrative purposes.
    :param str test_org: (optional)
        Name of the AWS Organizational Unit to use for testing purposes.

    """

    def __init__(
        self,
        in_reason='setup',
        out_reason='testing',
        detect=False,
        one_way=False,
        setup_org=SETUP_ORG,
        test_org=TEST_ORG,
    ):
        self.in_reason = in_reason
        self.out_reason = out_reason
        self.detect = detect
        self.one_way = one_way
        self._entry_ou = None

        self.setup_org = setup_org
        self.test_org = test_org

    def should_move(self):
        """Determine whether the account should move to the test OU upon
        exiting the context block.

        :returns:
            A boolean.

        """

        move = not self.one_way
        if move and self.detect and self._entry_ou == self.setup_org:
            move = False

        return move

    def __enter__(self):
        if self.detect:
            client = _get_organizations_client()
            self._entry_ou = get_current_org_unit(organizations=client)

        move_to_setup_org(reason=self.in_reason)
        return self

    def __exit__(self, *exc):
        if self.should_move():
            move_to_test_org(reason=self.out_reason)

        return False


class in_test_org(ContextDecorator):
    """This context manager temporarily moves the AWS account into the
    ``TEST_ORG`` AWS Organizational Unit, executes the block, then moves the
    AWS account back into the ``SETUP_ORG`` AWS Organizational Unit. This relies
    on the following environment variables to determine which Organizational
    Units to switch between:

    * :envvar:`RHEDCLOUD_TEST_ORG`: the AWS Organizational Unit where tests
      will be executed, possibly with additional restrictions.
    * :envvar:`RHEDCLOUD_SETUP_ORG`: the AWS Organizational Unit with fewer
      restrictions, allowing for tests to be properly setup and tore down.

    Usage:

    .. code-block:: python

        @in_test_org()
        def test_something():
            print("this is running in the account's normal org unit")

        def test_something():
            with in_test_org():
                print("this is running in the account's normal org unit")

    :param str in_reason: (optional)
        The reason the account must be moved to the test OU.
    :param str out_reason: (optional)
        The reason the account must be moved to the setup OU.
    :param bool detect: (optional)
        Whether to detect if the account is already in the test OU. If so, it
        will remain in the test OU rather than forcibly being moved to the
        setup OU. By default, this is disabled to have minimal impact on
        existing tests.
    :param bool one_way: (optional)
        Whether to bother moving the account back to the setup OU.
    :param str setup_org: (optional)
        Name of the AWS Organizational Unit to use for administrative purposes.
    :param str test_org: (optional)
        Name of the AWS Organizational Unit to use for testing purposes.

    """

    def __init__(
        self,
        in_reason='testing',
        out_reason='teardown',
        detect=False,
        one_way=False,
        setup_org=SETUP_ORG,
        test_org=TEST_ORG,
    ):
        self.in_reason = in_reason
        self.out_reason = out_reason
        self.detect = detect
        self.one_way = one_way
        self._entry_ou = None

        self.setup_org = setup_org
        self.test_org = test_org

    def should_move(self):
        """Determine whether the account should move to the setup OU upon
        exiting the context block.

        :returns:
            A boolean.

        """

        move = not self.one_way
        if move and self.detect and self._entry_ou == self.setup_org:
            move = False

        return move

    def __enter__(self):
        if self.detect:
            client = _get_organizations_client()
            self._entry_ou = get_current_org_unit(organizations=client)

        move_to_test_org(reason=self.in_reason)
        return self

    def __exit__(self, *exc):
        if self.should_move():
            move_to_setup_org(reason=self.out_reason)

        return False


class aws_profile(ContextDecorator):
    """Temporarily switch to another AWS profile.

    :param str other:
        Name of the AWS profile to switch to.
    :param str current: (optional)
        Name of the AWS profile to switch back to.

    """

    def __init__(self, other, current=None):
        self.other = other

        if current is None:
            current = os.getenv('AWS_PROFILE')
        self.current = current

    def __enter__(self):
        switch_profile(self.other)

    def __exit__(self, *exc):
        switch_profile(self.current)
        return False


def switch_profile(profile_name):
    """Switch to a different AWS Profile.

    :param str profile_name:
        Name of the AWS profile to use. This should already exist in the
        `$HOME/.aws/credentials` file.

    """

    if not hasattr(switch_profile, '_cache'):
        switch_profile._cache = {}

    if os.environ.get('AWS_PROFILE') == profile_name:
        return

    if not profile_name:
        profile_name = "default"

    debug('Switching to profile: {}'.format(profile_name))
    os.environ['AWS_PROFILE'] = profile_name

    session = switch_profile._cache.get(profile_name)
    if session is None:
        # no cached session
        boto3.setup_default_session()

        # cache the new session
        switch_profile._cache[profile_name] = boto3.DEFAULT_SESSION
    else:
        # reuse cached session
        boto3.DEFAULT_SESSION = session


@aws_client('organizations')
def lookup_account(account, *, organizations):
    """Lookup the specifed account by name or ID.

    :param str account:
        Name or ID of an AWS Account.

    :raises ValueError:
        When no account can be found with the specified name or ID.

    :returns:
        Details about the specified account.

    """

    for acct in list_accounts(organizations=organizations):
        if account in (acct['Id'], acct['Name']):
            return acct

    raise ValueError('cannot find account "{}"'.format(account))


@aws_client('organizations')
def lookup_ou(name, *, organizations):
    """Lookup the specifed AWS Organizational Unit by name or ID.

    :param str name:
        Name or ID of an AWS Organizational Unit.

    :raises ValueError:
        When no Organizational Unit can be found with the specified name or ID.

    :returns:
        Details about the specified Organizational Unit.

    """

    parents = [
        root["Id"]
        for root in organizations.list_roots()["Roots"]
    ]

    for parent in parents:
        if parent == name:
            return parent

        account_list = organizations.list_accounts_for_parent(
            ParentId=parent,
        )
        for acct in account_list["Accounts"]:
            if name in (acct["Id"], acct["Name"]):
                return parent

        org_units = organizations.list_organizational_units_for_parent(
            ParentId=parent,
        )
        for ou in org_units["OrganizationalUnits"]:
            if name in (ou["Id"], ou["Name"]):
                return ou

            parents.append(ou["Id"])

    raise ValueError('cannot find OU "{}"'.format(name))


def _get_organizations_client(setup_profile=SETUP_PROFILE, test_profile=TEST_PROFILE):
    """Return a handle to the AWS Organizations API using an account that has
    access to that API.

    :param str setup_profile: (optional)
        Name of the AWS profile to use for administrative purposes.
    :param str test_profile: (optional)
        Name of the AWS profile to use for testing purposes.

    :returns:
        An Organizations.Client object.

    """

    if not hasattr(_get_organizations_client, '_client'):
        # switch to the profile that gives us access to the setup account
        switch_profile(setup_profile)

        # create a client using the new profile
        _get_organizations_client._client = boto3.client('organizations')

        # switch back to the test profile
        switch_profile(test_profile)

    return _get_organizations_client._client


def move_to_org(
    account, dest_name, src_name, reason, setup_profile=SETUP_PROFILE, test_profile=TEST_PROFILE, *, organizations=None
):
    """Move the specified account from one AWS Organizational Unit to another.

    :param str account:
        Name or ID of the AWS Account to move.
    :param str dest_name:
        Name or ID of the AWS Organizational Unit to move the account into.
    :param str src_name:
        Name or ID of the AWS Organizational Unit to move the account from.
    :param str reason:
        Reason for moving the account to another Organizational Unit, to help
        testers follow test output.
    :param str setup_profile: (optional)
        Name of the AWS profile to use for administrative purposes.
    :param str test_profile: (optional)
        Name of the AWS profile to use for testing purposes.

    .. note::

        **Do not** decorate this function with `@aws_client('organizations')`--the
        client needs to be created *after* the profile switch in order to work
        correctly.

    """

    if not organizations:
        # use the cached client
        organizations = _get_organizations_client(
            setup_profile=setup_profile,
            test_profile=test_profile,
        )

    # short circuit if we're already in the correct OU
    current = get_current_org_unit(account, organizations=organizations)
    if current == dest_name:
        return

    # lookup account and OUs
    target = lookup_account(account, organizations=organizations)
    dest_ou = lookup_ou(dest_name, organizations=organizations)
    src_ou = lookup_ou(src_name, organizations=organizations)

    if dest_ou['Id'] != src_ou['Id']:
        debug('Moving account "{}" from OU "{}" to "{}" for {}'.format(
            target['Name'],
            src_ou['Name'],
            dest_ou['Name'],
            reason))

        try:
            organizations.move_account(
                AccountId=target['Id'],
                SourceParentId=src_ou['Id'],
                DestinationParentId=dest_ou['Id'],
            )

            # pause briefly for the change to take effect
            time.sleep(OU_SWITCH_PAUSE)
        except Exception as ex:
            debug('Unexpected error moving account: {}'.format(ex))


@aws_client('organizations')
def get_current_org_unit(account_name=ACCOUNT_NAME, *, organizations):
    """Query the current AWS Organizational Unit of the test account.

    :param str account_name: (optional)
        The name of the AWS Account to look for. Defaults to the value of the
        :envvar:`RHEDCLOUD_ACCOUNT_NAME` variable.

    :raises ValueError:
        When no account information can be found.

    :returns:
        A string.

    """

    debug("Looking for current OU for account: {}".format(account_name))
    for account in list_accounts(organizations=organizations):
        # skip accounts with a different name than the one configured for
        # the test account
        account_id = account.get('Id')
        debug("Inspecting account: {} ({})".format(account.get("Name"), account_id))
        if not account_id or account_name not in (account_id, account.get('Name')):
            continue

        debug("Found account: {}".format(account))
        return get_account_org_unit(account_id, organizations=organizations)

    raise ValueError('Unknown AWS Organizational Unit')


@aws_client("organizations")
def get_parents(account_id, valid_types=("ORGANIZATIONAL_UNIT",), *, organizations):
    """Iterate over all parents for the specified account, yielding information
    about each parent of specific types.

    :param str account_id:
        The ID of an AWS Account.
    :param tuple(str) valid_types: (optional)
        One or more types of organizational unit to filter results.

    :yields:
        A dictionary.

    """

    # root OUs have no parent
    if account_id.startswith("r-"):
        return

    parents_res = organizations.list_parents(ChildId=account_id)
    for parent in parents_res.get('Parents', []):
        if parent["Type"] in valid_types:
            yield parent

        for ancestor in get_parents(parent["Id"], valid_types, organizations=organizations):
            yield ancestor


@aws_client("organizations")
def get_root_ou(account_id, *, organizations):
    """Get information about the root OU for the specified account.

    :param str account_id:
        The ID of an AWS Account.

    :returns:
        A dictionary.

    """

    return next(get_parents(
        account_id,
        valid_types=("ROOT",),
        organizations=organizations,
    ))


@aws_client('organizations')
def get_account_org_unit(account_id, *, organizations):
    """Query the current AWS Organizational Unit of the test account.

    :param str account_id:
        The ID of an AWS Account.

    :raises ValueError:
        When no account information can be found.

    :returns:
        A string.

    """

    for parent in get_parents(account_id, organizations=organizations):
        res = organizations.describe_organizational_unit(
            OrganizationalUnitId=parent['Id'],
        )

        ou = res.get('OrganizationalUnit')
        if not ou or 'Name' not in ou:
            continue

        return ou['Name']


@aws_client('organizations')
def list_accounts(*, organizations):
    """Iterate over all sub-accounts.

    :yields:
        A dictionary.

    """

    kwargs = {}

    while True:
        res = organizations.list_accounts(**kwargs)
        for acct in res['Accounts']:
            yield acct

        next_token = res.get("NextToken")
        if not (next_token and res["Accounts"]):
            break

        kwargs["NextToken"] = next_token


move_to_test_org = partial(move_to_org, ACCOUNT_NAME, TEST_ORG, SETUP_ORG)
move_to_setup_org = partial(move_to_org, ACCOUNT_NAME, SETUP_ORG, TEST_ORG)
