"""
==========
AWS Cloud9
==========

Utilities for AWS Cloud9 Gateway.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import debug, has_status, unexpected


@contextmanager
@aws_client('cloud9')
def new_cloud9_environment(prefix, *, cloud9):
    """Context manager to create a Cloud9 Environment and remove it
    when it is no longer used

    :param str prefix:
        Prefix to give the Cloud9 environment when generating a name

    :yields:
        A Cloud9 Environment ID

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    env_id = None
    try:
        res = cloud9.create_environment_ec2(name=name, instanceType='t2.micro', automaticStopTimeMinutes=5)
        env_id = res['environmentId']
        assert has_status(res, 200), unexpected(res)

        debug('Created Cloud9 Environment: {}'.format(name))

        yield env_id
    finally:
        if res is not None and env_id is not None:
            debug('Removing Cloud9 Environment: {}'.format(name))
            res = cloud9.delete_environment(environmentId=env_id)
            assert has_status(res, 200), unexpected(res)
