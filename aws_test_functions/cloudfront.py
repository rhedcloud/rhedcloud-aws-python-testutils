"""
==============
AWS CloudFront
==============

Utilities for AWS CloudFront.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import debug, has_status, retry, unexpected


@contextmanager
@aws_client("cloudfront")
def new_origin_access_identity(prefix, *, cloudfront):
    """Context manager to create an origin access identity for AWS
    CloudFront and remove it when it is no longer used

    :param str prefix:
        Prefix to give the OAID when generating a name

    :yields:
        A CloudFront Origin Access Identity ID

    """

    name = "{}-{}".format(prefix, get_uuid())
    res = None
    cf_id = None
    e_tag = None
    try:
        res = cloudfront.create_cloud_front_origin_access_identity(
            CloudFrontOriginAccessIdentityConfig={
                "CallerReference": name,
                "Comment": "Test Identity Access",
            }
        )
        assert has_status(res, 201), unexpected(res)
        debug("Created CloudFront Origin Access Identity: {}".format(name))

        cf_id = res["CloudFrontOriginAccessIdentity"]["Id"]
        e_tag = res["ETag"]
        yield cf_id
    finally:
        if res is not None:
            debug("Removing CloudFront Origin Access Identity: {}".format(name))
            res = cloudfront.delete_cloud_front_origin_access_identity(
                Id=cf_id,
                IfMatch=e_tag,
            )
            assert has_status(res, 204), unexpected(res)


@contextmanager
@aws_client("cloudfront")
def new_distribution(prefix, cf_oaid, bucket, *, cloudfront):
    """Context manager to create a new web distribution and remove it when
    it is no longer used

    :param str prefix:
        Prefix to give the distribution when generating a name
    :param str cf_oaid:
        The Cloudfront Origin Access Identity to use
    :param S3.Bucket bucket:
        An S3 bucket with TestCloudFront.txt in the root directory

    :yields:
        A CloudFront Distribution ID

    """

    name = "{}-{}".format(prefix, get_uuid())
    res = None
    dist_id = None
    e_tag = None
    try:
        res = cloudfront.create_distribution(
            DistributionConfig={
                "CallerReference": name,
                "Origins": {
                    "Quantity": 1,
                    "Items": [
                        {
                            "Id": "TestDistributionItemID",
                            "DomainName": bucket.name + ".s3.amazonaws.com",
                            "S3OriginConfig": {
                                "OriginAccessIdentity": "origin-access-identity/cloudfront/" + cf_oaid
                            },
                        }
                    ],
                },
                "DefaultCacheBehavior": {
                    "TargetOriginId": "TestDistributionItemID",
                    "ForwardedValues": {
                        "QueryString": False,
                        "Cookies": {"Forward": "none"},
                    },
                    "TrustedSigners": {"Enabled": False, "Quantity": 0},
                    "ViewerProtocolPolicy": "redirect-to-https",
                    "MinTTL": 0,
                },
                "Comment": "Test Distribution",
                "PriceClass": "PriceClass_All",
                "Enabled": True,
            }
        )
        assert has_status(res, 201), unexpected(res)
        debug("Created CloudFront Distribution: {}".format(name))
        dist_id = res["Distribution"]["Id"]
        e_tag = res["ETag"]
        cloudfront.get_waiter("distribution_deployed").wait(Id=dist_id)
        yield dist_id
    finally:
        if res is not None:
            debug("Removing CloudFront Distribution: {}".format(name))
            dist_config = cloudfront.get_distribution(Id=dist_id)["Distribution"]["DistributionConfig"]
            dist_config["Enabled"] = False
            res = cloudfront.update_distribution(DistributionConfig=dist_config, Id=dist_id, IfMatch=e_tag)
            status = retry(
                cloudfront.get_distribution,
                kwargs=dict(Id=dist_id),
                msg="Waiting for CloudFront distribution {} to be disabled...".format(dist_id),
                until=lambda s: (
                    s["Distribution"]["DistributionConfig"]["Enabled"] is False
                    and s["Distribution"]["Status"] == "Deployed"
                ),
                delay=60,
                max_attempts=60,
            )

            res = cloudfront.delete_distribution(Id=dist_id, IfMatch=status["ETag"])
            assert has_status(res, 204), unexpected(res)
