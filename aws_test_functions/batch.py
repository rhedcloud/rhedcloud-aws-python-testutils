"""
=========
AWS Batch
=========

Utilities for AWS Batch.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_uuid
from .utils import debug, has_status, unexpected


@contextmanager
@aws_client('batch')
@aws_client('ec2')
def new_batch_compute_environment(
    prefix, subnets, security_group_ids, service_role, ecs_instance_profile, *, batch, ec2
):
    """Context manager to create a new compute environment for Batch and remove
    it when it's no longer used.

    :param str prefix:
        Prefix to give the Compute Environment when generating a random name.
    :param list[str] subnets:
        The list of subnet IDs where the compute environment will be deployed.
    :param list[str] security_group_ids:
        A list of SecurityGroup Ids for the compute environment instances.
    :param boto3.IAM.Role service_role:
        A Role for the compute environment service.
    :param boto3.IAM.InstanceProfile ecs_instance_profile:
        An InstanceProfile for the compute environment instances.

    :yields:
        A Batch Compute Environment Name

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    key_name = None
    try:
        ec2.create_key_pair(KeyName=name + '_key')
        key_name = name + '_key'
        res = batch.create_compute_environment(
            computeEnvironmentName=name,
            type='MANAGED',
            state='ENABLED',
            computeResources={
                'type': 'EC2',
                'minvCpus': 2,
                'maxvCpus': 256,
                'instanceTypes': ['m4.large'],
                'subnets': subnets,
                'securityGroupIds': security_group_ids,
                'ec2KeyPair': key_name,
                'instanceRole': ecs_instance_profile.arn,
            },
            serviceRole=service_role.arn,
        )
        assert has_status(res, 200), unexpected(res)
        debug('Created Batch Compute Environment: {}'.format(name))
        yield name
    finally:
        if res is not None:
            debug('Removing Batch Compute Environment: {}'.format(name))
            batch.update_compute_environment(computeEnvironment=name, state='DISABLED')

            wait_response = batch.describe_compute_environments(computeEnvironments=[name])
            assert len(wait_response['computeEnvironments']) > 0, 'Failed to retrieve Compute Environment status'
            while wait_response['computeEnvironments'][0]['status'] == 'UPDATING':
                wait_response = batch.describe_compute_environments(computeEnvironments=[name])

            wait_response = batch.describe_compute_environments(computeEnvironments=[name])
            res = batch.delete_compute_environment(computeEnvironment=name)
            assert len(wait_response['computeEnvironments']) > 0, 'Failed to retrieve Compute Environment status'
            while len(wait_response['computeEnvironments']) > 0:
                wait_response = batch.describe_compute_environments(computeEnvironments=[name])

            if key_name is not None:
                ec2.delete_key_pair(KeyName=key_name)

            assert has_status(res, 200), unexpected(res)


@contextmanager
@aws_client('batch')
def new_batch_job_queue(prefix, compute_environment_name, *, batch):
    """Context manager to create a new batch job queue and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to give the Compute Environment when generating a random name.
    :param str compute_environment_name:
        The name of the compute environment the queue will use

    :yields:
        A Job Queue Name

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        debug("Creating Queue on Environment " + compute_environment_name)

        res = batch.create_job_queue(
            jobQueueName=name,
            state='ENABLED',
            priority=1,
            computeEnvironmentOrder=[{
                'order': 1,
                'computeEnvironment': compute_environment_name,
            }],
        )

        assert has_status(res, 200), unexpected(res)
        debug('Created Batch Job Queue: {}'.format(name))
        yield name
    finally:
        if res is not None:
            debug('Removing Job Queue: {}'.format(name))

            batch.update_job_queue(jobQueue=name, state='DISABLED')
            wait_response = batch.describe_job_queues(jobQueues=[name])
            assert len(wait_response['jobQueues']) > 0, 'Failed to retrieve Job Queue status'
            while wait_response['jobQueues'][0]['status'] == 'UPDATING':
                wait_response = batch.describe_job_queues(jobQueues=[name])

            wait_response = batch.describe_job_queues(jobQueues=[name])
            res = batch.delete_job_queue(jobQueue=name)
            assert len(wait_response['jobQueues']) > 0, 'Failed to retrieve Job Queue status'
            while len(wait_response['jobQueues']) > 0:
                wait_response = batch.describe_job_queues(jobQueues=[name])

            assert has_status(res, 200), unexpected(res)


@contextmanager
@aws_client('batch')
def new_batch_job_definition(prefix, repo_name, job_role, *, batch):
    """Context manager to create a new batch job definition and remove it when
    it's no longer used.

    :param str prefix:
        Prefix to give the Compute Environment when generating a random name.
    :param str repo_name:
        The name of the ECR repo containing the fetch_and_run.sh entrypoint,
        defined within the AWS Batch tutorial.

    :yields:
        A Job Definition Name with appended version tag ':1'

    """

    name = '{}-{}'.format(prefix, get_uuid())
    res = None
    try:
        res = batch.register_job_definition(
            jobDefinitionName=name,
            type='container',
            containerProperties={
                'image': repo_name,
                'vcpus': 1,
                'memory': 500,
                'jobRoleArn': job_role.arn,
            },
        )
        assert has_status(res, 200), unexpected(res)
        versioned_name = name + ":1"
        debug('Created Batch Job Definition: {}'.format(versioned_name))
        yield versioned_name
    finally:
        if res is not None:
            debug('Removing Batch Job Definition: {}'.format(versioned_name))
            res = batch.deregister_job_definition(jobDefinition=versioned_name)
            assert has_status(res, 200), unexpected(res)
