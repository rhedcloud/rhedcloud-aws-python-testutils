"""
===============
Amazon GameLift
===============

"""

from .decorators import (
    aws_client,
    ignore_errors,
)


@ignore_errors
@aws_client('gamelift')
def cleanup(*, gamelift):
    """Remove unused resources."""

    cleanup_aliases(gamelift=gamelift)
    cleanup_fleets(gamelift=gamelift)
    cleanup_builds(gamelift=gamelift)


@aws_client('gamelift')
def delete_alias(alias_id, *, gamelift):
    """Delete a alias.

    :param str alias_id:
        ID of the alias to delete.

    """

    print('Deleting alias: {}'.format(alias_id))
    return gamelift.delete_alias(AliasId=alias_id)


@aws_client('gamelift')
def cleanup_aliases(*, gamelift):
    """Remove any unused aliases."""

    res = gamelift.list_aliases()
    for alias in res['Aliases']:
        ignore_errors(delete_alias)(alias['AliasId'], gamelift=gamelift)


@aws_client('gamelift')
def delete_build(build_id, *, gamelift):
    """Delete a build.

    :param str build_id:
        ID of the build to delete.

    """

    print('Deleting build: {}'.format(build_id))
    return gamelift.delete_build(BuildId=build_id)


@aws_client('gamelift')
def cleanup_builds(*, gamelift):
    """Remove any unused builds."""

    res = gamelift.list_builds()
    for build in res['Builds']:
        ignore_errors(delete_build)(build['BuildId'], gamelift=gamelift)


@aws_client('gamelift')
def delete_fleet(fleet_id, *, gamelift):
    """Delete a fleet.

    :param str fleet_id:
        ID of the fleet to delete.

    """

    print('Setting capacity to zero for fleet: {}'.format(fleet_id))
    ignore_errors(gamelift.update_fleet_capacity)(
        FleetId=fleet_id,
        DesiredInstances=0,
    )

    print('Deleting fleet: {}'.format(fleet_id))
    return gamelift.delete_fleet(FleetId=fleet_id)


@aws_client('gamelift')
def cleanup_fleets(*, gamelift):
    """Remove any unused fleets."""

    res = gamelift.list_fleets()
    for fleet_id in res['FleetIds']:
        ignore_errors(delete_fleet)(fleet_id, gamelift=gamelift)
