"""
==========================
AWS Simple Systems Manager
==========================

Utilities to interact with SSM.

"""

from collections import UserDict
from contextlib import ExitStack, contextmanager

from .decorators import aws_client
from .lib import get_uuid, wait_for_managed_instance
from .utils import dict_to_filters, has_status, retry
from .vpc import create_service_endpoint, service_endpoint, delete_service_endpoint

DONE_STATES = ('Success', 'Cancelled', 'Failed')
ERROR_STATES = ('TimedOut',)


class CommandRunner:
    """Helper to execute commands inside an EC2 instance managed with SSM.

    :param EC2.Instance instance:
        The EC2 Instance resource to use when running commands.

    """

    @aws_client('ssm')
    def __init__(self, instance, *, ssm):
        self.instance = instance
        self.client = ssm

    def _sanitize_commands(self, commands):
        """Ensure that the specified commands are in the correct format for use
        with SSM.

        :param mixed commands:
            One or more commands to sanitize.

        :returns:
            A list of strings.

        """

        if isinstance(commands, bytes):
            commands = commands.decode()

        # make sure the input commands are a list
        if isinstance(commands, str):
            commands = [commands]

        return commands

    def _send_commands(self, commands, timeout=60):
        """Send a request to run the specified commands on the target EC2
        instance.

        :param mixed commands:
            One or more commands to execute.
        :param int timeout: (optional)
            Maximum number of seconds to wait for the command to finish
            running.

        :returns:
            A dictionary containing information about the issued request.

        """

        commands = self._sanitize_commands(commands)

        # make sure the instance is registered with SSM
        wait_for_managed_instance(self.instance.instance_id, timeout=timeout * 10)

        if len(commands) == 1:
            print('Executing command: {}'.format(commands[0]))

        req = self.client.send_command(
            InstanceIds=[self.instance.instance_id],
            DocumentName='AWS-RunShellScript',
            Parameters={
                'commands': commands,
                'executionTimeout': ['{}'.format(timeout)],
            },
        )

        # print('SSM Request Response:\n{}'.format(req))
        assert has_status(req, 200), 'command failed: {}'.format(req)

        return req

    def _get_output(self, command_id):
        """Retrieve the output from a command executed on an EC2 instance using
        SSM.

        :param str command_id:
            ID of the SSM request to run a command.

        :returns:
            An SSMResult.

        """

        try:
            res = self.client.get_command_invocation(
                CommandId=command_id,
                InstanceId=self.instance.instance_id,
            )
        except Exception as ex:
            if 'InvocationDoesNotExist' not in str(ex):
                raise

        status = res['Status']
        output = SSMResult(res)

        assert status not in ERROR_STATES, 'Command {}: {}'.format(status, output)

        if status not in DONE_STATES:
            raise ValueError(status)

        return output

    def __call__(self, commands, timeout=60, delay=5, max_attempts=60):
        """Issue a request to run the specified commands and wait for the
        output to be ready.

        :param str/list(str) commands:
            One or more commands to execute.
        :param int timeout: (optional)
            Maximum number of seconds to wait for the command to finish
            running.
        :param int delay: (optional)
            Number of seconds to wait between polls for command output.
        :param int max_attempts: (optional)
            Maximum number of times to check for command output before failing.

        """

        req = self._send_commands(commands, timeout=timeout)

        return retry(
            self._get_output,
            args=(req['Command']['CommandId'],),
            msg='Waiting for command to complete',
            delay=delay,
            max_attempts=max_attempts,
        )


class SSMResult(UserDict):
    """A simple wrapper around the result of a command executed using SSM.

    It allows the result of an SSM command to act like a string of the actual
    command output in most cases, but it still provides access to information
    about the SSM command. That information is accessible when treating an
    SSMResult as a dictionary.

    """

    OUTPUT_DIVIDER = '\n\n====> STDERR <====\n\n'

    def __getattr__(self, name):
        """Make the result act like a string whenever possible."""

        if callable(getattr(str, name, None)):
            return getattr(str(self), name)

        return getattr(self.data, name)

    def __contains__(self, needle):
        """For ``'needle' in output``.

        :param str needle:
            The value to search for in the command's output.

        :returns:
            A boolean.

        """

        return needle in str(self)

    def __eq__(self, value):
        """Override equality operator, treating the command output as a string
        rather than the SSM result dictionary.

        :param str value:
            The value to compare with the output of the SSM command.

        :returns:
            A boolean.

        """

        return str(self) == value

    def __len__(self):
        """Check the length of the command's output, not the size of the
        SSM result dictionary.

        :returns:
            An integer.

        """

        return len(str(self))

    def __str__(self):
        """Return the output from the command itself, rather than a string
        version of the SSM result dictionary.

        :returns:
            A string.

        """

        output = self.data['StandardOutputContent']

        stderr = self.data['StandardErrorContent']
        if stderr:
            output = ''.join([output, self.OUTPUT_DIVIDER, stderr])

        return output


@contextmanager
@aws_client('ec2')
def ssm_endpoint(vpc_id, *, ec2):
    """Setup endpoints required for SSM to work in the specified (non-default) VPC.

    :param str vpc_id:
        Internal VPC ID.

    """

    with ExitStack() as stack:
        # both of these endpoints are required to use SSM outside of the
        # default VPC
        stack.enter_context(service_endpoint('ssm', vpc_id))
        stack.enter_context(service_endpoint('ec2messages', vpc_id))

        yield


@aws_client('ssm')
def build_command_runner(instance, *, ssm):
    """Return a helper that executes commands on the specified ``instance`` using SSM,
    returning the standard output and standard error.

    :param EC2.Instance instance:
        The EC2 Instance resource to use when running commands.

    :returns:
        A CommandRunner.

    """

    return CommandRunner(instance, ssm=ssm)


@aws_client('ec2')
def setup_ssm_service_endpoints(vpc_id, *, ec2):
    """Setup the correct AWS Endpoints to be able to use SSM.

    :param str vpc_id:
        ID of an RHEDcloud VPC.

    """

    create_service_endpoint('ssm', vpc_id, ec2=ec2)
    create_service_endpoint('ec2messages', vpc_id, ec2=ec2)


@aws_client('ec2')
def delete_ssm_service_endpoints(vpc_id, *, ec2):
    """Delete any service endpoints that were setup for SSM.

    :param str vpc_id:
        ID of an RHEDcloud VPC.

    """

    filters = dict_to_filters({
        'vpc-id': vpc_id,
    })

    res = ec2.describe_vpc_endpoints(Filters=filters)
    endpoint_ids = [
        endpoint['VpcEndpointId']
        for endpoint in res['VpcEndpoints']
        if 'ssm' in endpoint['ServiceName'] or 'ec2messages' in endpoint['ServiceName']
    ]

    print('Deleting SSM endpoints (VPC: {})'.format(vpc_id))
    delete_service_endpoint(*endpoint_ids, ec2=ec2)


@contextmanager
@aws_client('ssm')
def new_ssm_parameter(prefix, value, param_type, *, ssm):
    """Context manager to create a MediaLive input and remove it when it's
    no longer used.

    :param str prefix:
        Prefix to use with the input's name.
    :param str input_sg_id:
        The input security group ID to use for this input.
    :param str hls_url_primary:
        The location of a hosted m3u8 file.
    :param str hls_url_backup:
        A secondary location of the hosted m3u8 file.
    :param bool delete:
        Whether or not to delete the input when finished.

    :yields:
        A MediaLive input id

    """

    res = None
    name = '{}-{}'.format(prefix, get_uuid())
    try:
        res = ssm.put_parameter(Name=name, Value=value, Type=param_type)
        assert has_status(res, 200)
        yield name
    finally:
        if res is not None:
            res = ssm.delete_parameter(Name=name)
            assert has_status(res, 200)
