"""
==========
Decorators
==========

A collection of decorators to help reuse logic.

"""

from functools import wraps

import boto3
import botocore


def catch(*exceptions, reraise=False, return_value=None, return_type=None, msg=None):
    """Decorator to handle the specified `exceptions` raised by the decorated
    function.

    :param Exception exceptions:
        One or more Exceptions that are anticipated in the body of the
        decorated function.
    :param bool reraise: (optional)
        Whether to re-raise the original exception.
    :param mixed return_value: (optional)
        A value to return when one of the specified exceptions is caught. This
        has no effect when `reraise` is True.
    :param callable return_type: (optional)
        The type of value to return when one of the specified exceptions is
        caught. This takes precedence over ``return_value`` This has no effect
        when ``reraise`` is True.
    :param str msg: (optional)
        A message to print when one of the specified exceptions is caught.

    Usage:

    >>> @catch(ValueError, msg='Caught: {ex}', return_value=10)
    ... def my_function():
    ...     raise ValueError('oops')
    >>> r1 = my_function()
    Caught: oops
    >>> r1
    10

    >>> @catch(ValueError, msg='Caught and Raised: {ex}', return_value=11, reraise=True)
    ... def similar_function():
    ...     raise ValueError('oops')
    >>> r2 = similar_function()
    Traceback (most recent call last):
        ...
    ValueError: oops
    >>> r2
    Traceback (most recent call last):
        ...
    NameError: name 'r2' is not defined

    >>> @catch(ValueError, msg='Uncaught: {ex}', return_value=12)
    ... def other_function():
    ...     raise TypeError('oops')
    >>> r3 = other_function()
    Traceback (most recent call last):
        ...
    TypeError: oops
    >>> r3
    Traceback (most recent call last):
        ...
    NameError: name 'r3' is not defined

    >>> @catch(ValueError, TypeError, msg='Multiple exception types: {ex}')
    ... def other_function():
    ...     raise TypeError('oops')
    >>> r4 = other_function()
    Multiple exception types: oops
    >>> r4 is None
    True

    >>> @catch(ValueError, msg='Uncaught: {ex}', return_type=list)
    ... def other_function():
    ...     raise ValueError('oops')
    >>> r5 = other_function()
    Uncaught: oops
    >>> r5
    []
    >>> r6 = other_function()
    Uncaught: oops
    >>> r6
    []
    >>> r5 is r6
    False

    >>> @catch(ValueError, msg='Uncaught: {ex}', return_value=[])
    ... def other_function():
    ...     raise ValueError('oops')
    >>> r7 = other_function()
    Uncaught: oops
    >>> r7
    []
    >>> r8 = other_function()
    Uncaught: oops
    >>> r8
    []
    >>> r7 is r8
    True

    """

    def outer(f):
        @wraps(f)
        def inner(*args, **kwargs):
            result = None

            try:
                result = f(*args, **kwargs)
            except exceptions as ex:
                if msg is not None:
                    print(msg.format(ex=ex), flush=True)

                if reraise:
                    raise

                if return_type is not None:
                    result = return_type()

                if return_value is not None:
                    result = return_value

            return result

        return inner

    return outer


def aws_client(name, client_name=None):
    """Decorator to automatically populate the keyword argument having the
    specified `name` with an AWS Client (if it's not already set).

    :param str name:
        The name of the keyword argument to which the AWS client will be
        assigned.

    :param str client_name:
        The name of the AWS client type, if not equal to the keyword
        argument name. By default, this will be set to the name parameter.

    """

    if client_name is None:
        client_name = name

    def outer(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            existing = kwargs.get(name, None)
            if existing is None:
                kwargs[name] = boto3.client(client_name)
            elif isinstance(existing, boto3.resources.base.ServiceResource):
                kwargs[name] = existing.meta.client
            elif not isinstance(existing, botocore.client.BaseClient):
                raise TypeError('expected AWS Client for parameter "{}"; got {}'.format(client_name, type(existing)))

            return f(*args, **kwargs)

        return wrapped

    return outer


def aws_resource(name):
    """Decorator to automatically populate the keyword argument having the
    specified `name` with an AWS Service Resource (if it's not already set).

    :param str name:
        The name of the keyword argument to which the AWS Service Resource will
        be assigned.

    """

    def outer(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            existing = kwargs.get(name, None)
            if existing is None:
                kwargs[name] = boto3.resource(name)
            elif not isinstance(existing, boto3.resources.base.ServiceResource):
                raise TypeError('expected AWS Service Resource for parameter "{}"; got {}'.format(name, type(existing)))

            return f(*args, **kwargs)

        return wrapped

    return outer


ignore_errors = catch(Exception, msg='Ignoring error: {ex}')
