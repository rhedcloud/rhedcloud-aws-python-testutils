"""
=========================
AWS Elastic Compute Cloud
=========================

Utilities for AWS EC2.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import (
    ClientError,
    get_uuid,
)
from .utils import client_resource, dict_to_filters, retry


@aws_client('ec2')
def find_ec2_instances(name, subnet_id=None, *, valid_states=None, ec2):
    """Find all EC2 instances with the specified name.

    :param str name:
        Name of the EC2 instance to find.
    :param str subnet_id: (optional)
        ID of the subnet where the instance should live.
    :param list valid_states: (optional)
        Require the instance to have one of the specified states. By default,
        the valid states are ``pending`` and ``running``.

    :yields:
        EC2 Instance resources.

    """

    if valid_states is None:
        valid_states = ('pending', 'running')

    filters = {
        'tag:Name': name,
    }

    if subnet_id:
        filters['network-interface.subnet-id'] = subnet_id

    res = retry(
        ec2.describe_instances,
        kwargs=dict(
            Filters=dict_to_filters(filters),
        ),
        msg="Describing EC2 instances",
        until=lambda r: "Reservations" in r,
        delay=10,
        show=True,
    )
    for reservation in res['Reservations']:
        for instance in reservation['Instances']:
            if instance['State']['Name'] not in valid_states:
                continue

            yield client_resource(ec2).Instance(instance['InstanceId'])


@aws_client('ec2')
def find_ec2_instance(name, subnet_id=None, *, valid_states=None, ec2):
    """Find the first EC2 instance with the specified name.

    :param str name:
        Name of the EC2 instance to find.
    :param str subnet_id: (optional)
        ID of the subnet where the instance should live.
    :param list valid_states: (optional)
        Require the instance to have one of the specified states. By default,
        the valid states are ``pending`` and ``running``.

    :raises ValueError:
        When no matching EC2 instance can be found.

    :returns:
        An EC2 Instance resource.

    """

    matches = list(find_ec2_instances(
        name,
        subnet_id,
        valid_states=valid_states,
        ec2=ec2,
    ))

    if not len(matches):
        raise ValueError('No matching instances')

    return matches[0]


@contextmanager
@aws_client('ec2')
def new_security_group(vpc_id, prefix, *, ec2):
    """Context manager to create a new security group and remove it when it's
    no longer used.

    :param str vpc_id:
        ID of the VPC where the security group should be created.
    :param str prefix:
        Prefix to use when generating the name of the security group to create.

    :yields:
        A SecurityGroup resource.

    """

    sg = None
    name = '{}-{}'.format(prefix, get_uuid())

    try:
        sg = create_security_group(vpc_id, name, ec2=ec2)
        yield sg
    finally:
        if sg is not None:
            retry(
                sg.delete,
                msg='Deleting security group: {}'.format(sg.group_name),
                exceptions=ClientError,
                pred=lambda ex: 'DependencyViolation' in str(ex),
                backoff=1,
            )


@aws_client('ec2')
def create_security_group(vpc_id, name, description=None, *, ec2):
    """Create a new security group to control access to EC2 instances.

    :param str vpc_id:
        ID of the VPC where the security group should be created.
    :param str name:
        Name of the security group to create.
    :param str description: (optional)
        Description for the security group.

    :returns:
        A SecurityGroup resource.

    """

    res = ec2.create_security_group(
        GroupName=name,
        Description=description or 'Description for {}'.format(name),
        VpcId=vpc_id,
    )

    assert 'GroupId' in res, 'Unexpected response: {}'.format(res)
    sg = client_resource(ec2).SecurityGroup(res['GroupId'])
    print('Created security group: {}'.format(sg.group_name))

    return sg


@contextmanager
@aws_client('ec2')
def new_internet_gateway(vpc_id, *, dry_run=False, ec2):
    """Context manager to create and cleanup a new temporary Internet Gateway.

    :param str vpc_id:
        VPC to attach to the new Internet Gateway.

    :yields:
        An InternetGateway resource.

    """

    attached = ec2.describe_internet_gateways(
        Filters=dict_to_filters({
            'attachment.vpc-id': vpc_id,
        })
    )
    if len(attached['InternetGateways']):
        igw = attached['InternetGateways'][0]
        print('Found Internet Gateway attached to VPC {}: {}'.format(vpc_id, igw['InternetGatewayId']))
        yield client_resource(ec2).InternetGateway(igw['InternetGatewayId'])
    else:
        igw = None
        try:
            igw = create_internet_gateway(dry_run=dry_run, ec2=ec2)
            print('Created Internet Gateway: {}'.format(igw.id))
            igw.attach_to_vpc(VpcId=vpc_id)

            yield igw
        finally:
            if igw is not None:
                igw.detach_from_vpc(VpcId=vpc_id)

                print('Deleting Internet Gateway: {}'.format(igw.id))
                igw.delete()


@aws_client('ec2')
def create_internet_gateway(*, dry_run=False, ec2):
    """Create a new Internet Gateway.

    :returns:
        An InternetGateway resource.

    """

    res = ec2.create_internet_gateway(DryRun=dry_run)
    assert 'InternetGateway' in res, 'Unexpected response: {}'.format(res)
    assert 'InternetGatewayId' in res['InternetGateway'], 'Unexpected response: {}'.format(res)

    return client_resource(ec2).InternetGateway(res['InternetGateway']['InternetGatewayId'])


@contextmanager
@aws_client('ec2')
def new_key_pair(prefix, *, ec2):
    """Context manager to create a new Key Pair and remove it when it's no
    longer necessary.

    :param str prefix:
        Prefix to use when generating the name of the key pair.

    :returns:
        A dictionary with information about the Key Pair.

    """

    name = '{}-{}'.format(prefix, get_uuid())
    created = False

    try:
        res = ec2.create_key_pair(KeyName=name)
        assert 'KeyName' in res, 'Unexpected response: {}'.format(res)
        created = True
        print('Created Key Pair: {}'.format(name))

        yield res
    finally:
        if created:
            print('Deleting Key Pair: {}'.format(name))
            ec2.delete_key_pair(KeyName=name)
