"""
====================================
AWS Key Management Service Utilities
====================================

This module contains helpers for the AWS KMS.

"""

from .decorators import aws_client


@aws_client('kms')
def list_all_kms_keys(limit: int = 1000, *, kms):
    """Generator to return all KMS keys.

    :param int limit: (optional)
        Maximum number of keys to retrieve in each request.

    :yields:
        A dictionary.

    """

    kwargs = {
        'Limit': limit,
    }

    while True:
        print('Retrieving next {} KMS keys...'.format(limit))
        res = kms.list_keys(**kwargs)
        for key in res['Keys']:
            yield key

        if res.get('Truncated') or 'NextMarker' not in res:
            print('All KMS keys retrieved')
            break

        kwargs['Marker'] = res['NextMarker']


@aws_client('kms')
def get_or_create_kms_key_arn(*, kms):
    """Return the ARN for the default KMS key.

    Currently, encryption keys require deletion to be scheduled for 7-30 days,
    so we try to reuse any existing keys. If no key already exists, a new key
    will be created.

    :returns:
        A string.

    """

    key_arn = None
    for key in list_all_kms_keys(kms=kms):
        desc = kms.describe_key(KeyId=key['KeyArn'])['KeyMetadata']
        if not desc['Enabled'] or desc['KeyManager'] == 'AWS':
            continue

        key_arn = key['KeyArn']
        print('Found KMS key: {}'.format(key_arn))
        break

    if key_arn is None:
        key = kms.create_key()
        key_arn = key['KeyMetadata']['Arn']
        print('Created KMS key: {}'.format(key_arn))

    assert key_arn, 'failed to get or create KMS key'
    return key_arn
