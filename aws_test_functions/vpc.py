"""
=========================
AWS Virtual Private Cloud
=========================

Utilities for interacting with VPCs.

"""

from contextlib import contextmanager

from .decorators import aws_client
from .lib import get_subnet_id
from .utils import dict_to_filters
from . import env


@contextmanager
@aws_client('ec2')
def service_endpoint(name, vpc_id, *, ec2, **kwargs):
    """Context manager to create a VPC endpoint for a certain service and remove the endpoint
    when it's no longer necessary.

    :param str name:
        Name of the service.
    :param str vpc_id:
        ID of the VPC to create the endpoint for.

    """

    vpc_endpoint_id = None
    existed = False

    try:
        vpc_endpoint_id, existed = create_service_endpoint(name, vpc_id, ec2=ec2, **kwargs)
        yield
    finally:
        if vpc_endpoint_id is not None and not existed:
            delete_service_endpoint(vpc_endpoint_id, ec2=ec2)


def get_full_service_name(name):
    """Return the full service name for the specified service.

    :param str name:
        Short name of a service.

    :returns:
        A string.

    """

    return 'com.amazonaws.us-east-1.{}'.format(name)


@aws_client('ec2')
def create_service_endpoint(name, vpc_id, subnets=None, *, ec2, **kwargs):
    """Create a VPC endpoint for a certain service.

    If the service endpoint is already present, simply return the ID for the
    existing endpoint.

    :param str name:
        Name of the service.
    :param str vpc_id:
        ID of the VPC to create the endpoint for.
    :param iter(str) subnets: (optional)
        Name of one or more subnets in the specified VPC. This is only used for
        Interface type endpoints.

    :returns:
        The VPC endpoint ID.

    """

    gateway_endpoint_types = ('s3', 'dynamodb')
    service_name = get_full_service_name(name)
    vpc_endpoint_id = None
    existed = False

    # check to see if there's already an endpoint for the service
    filters = dict_to_filters({'vpc-id': vpc_id})
    existing = ec2.describe_vpc_endpoints(Filters=filters)
    for endpoint in existing['VpcEndpoints']:
        if endpoint['ServiceName'] == service_name:
            vpc_endpoint_id = endpoint['VpcEndpointId']
            existed = True
            break

    if not vpc_endpoint_id:
        endpoint_type = 'Gateway' if name in gateway_endpoint_types else 'Interface'
        if endpoint_type == 'Interface':
            if subnets is None:
                subnets = env.RHEDCLOUD_DEFAULT_VPC_ENDPOINT_SUBNETS

            kwargs['SubnetIds'] = [
                subnet if subnet.startswith('subnet-') else get_subnet_id(vpc_id, subnet, ec2=ec2)
                for subnet in subnets
            ]
        else:
            kwargs['RouteTableIds'] = [rt['RouteTableId'] for rt in get_vpc_route_tables(vpc_id)]

        kwargs.update({
            'ServiceName': service_name,
            'VpcEndpointType': endpoint_type,
            'VpcId': vpc_id,
        })

        res = ec2.create_vpc_endpoint(**kwargs)
        vpc_endpoint_id = res['VpcEndpoint']['VpcEndpointId']
        print('Created VPC endpoint for {} (VPC: {})'.format(name, vpc_id))

    return vpc_endpoint_id, existed


@aws_client('ec2')
def get_endpoint_for_service(name, vpc_id, *, ec2):
    """Retrieve information about an Endpoint for the specified service in the
    specified VPC.

    :param str name:
        Name of the service.
    :param str vpc_id:
        A VPC ID.

    :returns:
        A dictionary.

    """

    filters = dict_to_filters({
        'service-name': get_full_service_name(name),
        'vpc-id': vpc_id,
    })
    res = ec2.describe_vpc_endpoints(Filters=filters)
    assert 'VpcEndpoints' in res and len(res['VpcEndpoints']), 'Unexpected response: {}'.format(res)

    return res['VpcEndpoints'][0]


@aws_client('ec2')
def delete_service_endpoint(*vpc_endpoint_ids, ec2, **kwargs):
    """Delete the VPC endpoints with the specified IDs.

    :param vpc_endpoint_ids:
        One or more VPC endpoint IDs.

    """

    if not len(vpc_endpoint_ids):
        return

    kwargs['VpcEndpointIds'] = vpc_endpoint_ids

    print('Deleting VPC endpoints: {}'.format(', '.join(vpc_endpoint_ids)))
    ec2.delete_vpc_endpoints(**kwargs)


@aws_client('ec2')
def get_vpc_subnets(vpc_id, *, ec2):
    """Retrieve a list of all subnets for the specified ``vpc_id``.

    :param str vpc_id:
        ID of a VPC.

    :returns:
        A list.

    """

    res = ec2.describe_subnets(
        Filters=dict_to_filters({
            'vpc-id': vpc_id,
        }),
    )
    assert 'Subnets' in res, 'Unexpected response: {}'.format(res)

    return res['Subnets']


@aws_client('ec2')
def get_vpc_route_tables(vpc_id, *, ec2):
    """Return information about all route tables within the specified VPC.

    :param str vpc_id:
        AWS VPC ID.

    :returns:
        A list of dictionaries.

    """

    filters = dict_to_filters({
        'vpc-id': vpc_id,
    })
    res = ec2.describe_route_tables(Filters=filters)
    assert 'RouteTables' in res, 'Unexpected response: {}'.format(res)

    return res['RouteTables']


@aws_client('ec2')
def get_vpc_subnet_ids(vpc_id, *, ec2):
    """Retrieve a list of all subnet IDs for the specified ``vpc_id``.

    :param str vpc_id:
        ID of a VPC.

    :returns:
        A list.

    """

    return [subnet['SubnetId'] for subnet in get_vpc_subnets(vpc_id, ec2=ec2)]


@aws_client('ec2')
def get_vpc_security_groups(vpc_id, *, ec2):
    """Retrieve a list of all security groups for the specified ``vpc_id``.

    :param str vpc_id:
        ID of a VPC.

    :returns:
        A list.

    """

    res = ec2.describe_security_groups(
        Filters=dict_to_filters({
            'vpc-id': vpc_id,
        }),
    )
    assert 'SecurityGroups' in res, 'Unexpected response: {}'.format(res)

    return res['SecurityGroups']
