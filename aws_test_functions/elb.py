"""
=========================
AWS Elastic Load Balancer
=========================

Utilities for AWS Elastic Load Balancers.

"""

from contextlib import ExitStack, contextmanager
import time

import boto3

from .decorators import aws_client


@contextmanager
def new_elb(elb_type, scheme, instances, session=None, subnets=None):
    """Context manager to create a new load balancer and clean it up when it's
    no longer needed.

    :param str elb_type:
        The type of load balancer to create. Must be either ``application`` or
        ``network``.
    :param str scheme:
        Load balancer scheme. Must be either ``internet-facing`` or ``internal``.
    :param list instances:
        List of EC2 instances to target with the new load balancer.
    :param boto3.session.Session session: (optional)
        A custom user session.
    :param list subnets: (optional)
        A list of two or more subnet IDs. If omitted, the subnets IDs are
        determined based on the specified instances.

    :yields:
        A dictionary with information about the created load balancer.

    """

    if session is None:
        session = boto3

    lb = None
    name = 'test-elb-{}-{}'.format(elb_type, scheme)[:32]
    proto = 'TCP' if elb_type == 'network' else 'HTTP'

    if subnets is None:
        subnets = [inst.subnet_id for inst in instances]

    try:
        if elb_type == 'classic':
            elb = session.client('elb')
            lb = create_classic_load_balancer(name, scheme, subnets, proto, elb=elb)

            with instances_in_classic_load_balancer(lb, instances, elb=elb):
                yield lb
        else:
            elbv2 = session.client('elbv2')
            lb = create_load_balancer(name, elb_type, scheme, subnets, elbv2=elbv2)

            with ExitStack() as stack:
                tg = stack.enter_context(new_target_group(lb, instances, proto, elbv2=elbv2))
                stack.enter_context(new_listener(lb, tg, proto, elbv2=elbv2))

                yield lb
    finally:
        if lb is not None:
            lb['_cleanup']()


@aws_client('elb')
def create_classic_load_balancer(name, scheme, subnets, proto, port=80, *, elb):
    name = name[:32]
    lb = elb.create_load_balancer(
        LoadBalancerName=name,
        Listeners=[{
            'Protocol': proto,
            'LoadBalancerPort': port,
            'InstancePort': port,
        }],
        Subnets=subnets,
        Scheme=scheme,
    )

    elb.configure_health_check(
        LoadBalancerName=name,
        HealthCheck=dict(
            Target='TCP:{}'.format(port),
            Interval=5,
            Timeout=4,
            UnhealthyThreshold=2,
            HealthyThreshold=2,
        ),
    )

    def cleanup():
        print('Deleting load balancer: {}'.format(lb['DNSName']))
        elb.delete_load_balancer(LoadBalancerName=name)

    lb['_cleanup'] = cleanup

    # try to make things a bit more consistent with ELBv2
    lb.update(dict(
        LoadBalancerName=name,
        Type='classic',
        Scheme=scheme,
    ))

    return lb


@contextmanager
@aws_client('elb')
def instances_in_classic_load_balancer(lb, instances, *, elb):
    res = None

    params = dict(
        LoadBalancerName=lb['LoadBalancerName'],
        Instances=[{
            'InstanceId': inst.instance_id,
        } for inst in instances],
    )

    try:
        res = elb.register_instances_with_load_balancer(**params)

        yield
    finally:
        if res is not None:
            elb.deregister_instances_from_load_balancer(**params)


@aws_client('elbv2')
def create_load_balancer(name, elb_type, scheme, subnets, *, elbv2):
    """Create a new load balancer.

    :param str name:
        Name for the new load balancer.
    :param str elb_type:
        The type of load balancer to create. Must be either ``application`` or
        ``network``.
    :param str scheme:
        Load balancer scheme. Must be either ``internet-facing`` or ``internal``.
    :param list subnets:
        List of subnet IDs for the load balancer.

    :returns:
        A dictionary with information about the created load balancer.

    """

    assert elb_type in ('application', 'network')
    assert scheme in ('internet-facing', 'internal')

    res = elbv2.create_load_balancer(
        Name=name[:32],
        Type=elb_type,
        Subnets=subnets,
        Scheme=scheme,
        IpAddressType='ipv4',
    )

    assert 'LoadBalancers' in res, 'unexpected response: {}'.format(res)
    assert len(res['LoadBalancers']), 'incomplete response: {}'.format(res)

    lb = res['LoadBalancers'][0]
    domain = lb['DNSName']
    print('Created load balancer: {}'.format(domain))

    def cleanup():
        print('Deleting load balancer: {}'.format(domain))
        elbv2.delete_load_balancer(LoadBalancerArn=lb['LoadBalancerArn'])

    lb['_cleanup'] = cleanup

    return lb


@contextmanager
@aws_client('elbv2')
def new_target_group(lb, instances, proto, port=80, *, elbv2):
    """Context manager to create and remove a target group for a load balancer.

    :param dict lb:
        Information about a load balancer.
    :param list instances:
        A list of EC2 Instance resources.
    :param str proto:
        Protocol to use for the target group, such as ``HTTP`` or ``TCP``.
    :param int port: (optional)
        Port to forward.

    :yields:
        A dictionary containing information about the target group.

    """

    tg = arn = None
    name = 'tg-{}-{}'.format(lb['Type'], lb['Scheme'])
    vpc_id = instances[0].vpc_id

    params = dict(
        Name=name,
        Protocol=proto,
        Port=port,
        VpcId=vpc_id,
        TargetType='instance',
    )

    if lb['Type'] == 'application':
        params.update(dict(
            HealthCheckIntervalSeconds=5,
            HealthCheckTimeoutSeconds=2,
        ))
    else:
        params.update(dict(
            HealthCheckIntervalSeconds=10,
        ))

    try:
        res = elbv2.create_target_group(**params)

        assert 'TargetGroups' in res, 'Unexpected response: {}'.format(res)
        assert len(res['TargetGroups']), 'Invalid response: {}'.format(res)
        tg = res['TargetGroups'][0]
        arn = tg['TargetGroupArn']

        print('Created target group: {}'.format(name))
        with register_targets(arn, instances, elbv2=elbv2):
            yield tg
    finally:
        if tg is not None:
            print('Deleting target group: {}'.format(name))
            elbv2.delete_target_group(
                TargetGroupArn=arn,
            )


@contextmanager
@aws_client('elbv2')
def register_targets(group_arn, instances, *, elbv2):
    """Context manager to register and unregister targets in a target group.

    :param str group_arn:
        ARN of the target group to register the instances with.
    :param list instances:
        A list of EC2 Instance resources.

    """

    params = dict(
        TargetGroupArn=group_arn,
        Targets=[{
            'Id': inst.instance_id,
        } for inst in instances],
    )

    try:
        print('Registering instances...')
        elbv2.register_targets(**params)
        yield
    finally:
        print('Deregistering instances...')
        elbv2.deregister_targets(**params)


@contextmanager
@aws_client('elbv2')
def new_listener(lb, target_group, proto, port=80, *, elbv2):
    """Context manager to create and remove a new listener on a load balancer.

    :param dict lb:
        Information about a load balancer.
    :param dict target_group:
        Information about the target group.
    :param str proto:
        Protocol to use for the target group, such as ``HTTP`` or ``TCP``.
    :param int port: (optional)
        Port to forward.

    :yields:
        A dictionary with information about the load balancer.

    """

    res = listener_arn = None

    try:
        res = elbv2.create_listener(
            LoadBalancerArn=lb['LoadBalancerArn'],
            Protocol=proto,
            Port=port,
            DefaultActions=[{
                'Type': 'forward',
                'TargetGroupArn': target_group['TargetGroupArn'],
            }],
        )

        assert 'Listeners' in res, 'Unexpected response: {}'.format(res)
        assert len(res['Listeners']), 'Unexpected response: {}'.format(res)
        listener_arn = res['Listeners'][0]['ListenerArn']
        print('Created listener: {}'.format(listener_arn))

        yield lb
    finally:
        if res is not None:
            print('Deleting listener: {}'.format(listener_arn))
            elbv2.delete_listener(ListenerArn=listener_arn)


def wait_for_classic_elb_state(target_lb, desired_state, delay=10, max_attempts=15, *, session=None):
    """Wait for a classic load balancer to be in a specific state.

    Classic load balancers don't have a "state" associated with them directly,
    so this simply checks to see if all EC2 instances attached to the load
    balancer are in a specific state. Normally the specified ``desired_state``
    will be used, but ``active`` is a special case which equates to
    ``InService`` here.

    :param dict target_lb:
        Information about a load balancer.
    :param str desired_state:
        The state to wait for.
    :param int delay: (optional)
        Number of seconds to wait between state checks.
    :param int max_attempts: (optional)
        Maximum number of times to check the load balancer's state before
        giving up.

    """

    if desired_state == 'active':
        desired_state = 'InService'

    lb_name = target_lb['LoadBalancerName']
    elb = session.client('elb')
    for attempt in range(max_attempts):
        res = elb.describe_instance_health(
            LoadBalancerName=lb_name,
        )
        states = [s['State'] for s in res['InstanceStates']]

        print('Current classic ELB {} instance states: {}'.format(lb_name, states))
        if all(s == desired_state for s in states):
            break

        time.sleep(delay)


def wait_for_elb_state(target_lb, desired_state, delay=10, max_attempts=15, *, session=None):
    """Wait for a load balancer to be in a specific state.

    :param dict target_lb:
        Information about a load balancer.
    :param str desired_state:
        The state to wait for.
    :param int delay: (optional)
        Number of seconds to wait between state checks.
    :param int max_attempts: (optional)
        Maximum number of times to check the load balancer's state before
        giving up.

    """

    if session is None:
        session = boto3

    if target_lb['Type'] == 'classic':
        wait_for_classic_elb_state(target_lb, desired_state, delay, max_attempts, session=session)
        return

    elbv2 = session.client('elbv2')
    for attempt in range(max_attempts):
        res = elbv2.describe_load_balancers(
            LoadBalancerArns=[target_lb['LoadBalancerArn']],
        )

        assert 'LoadBalancers' in res, 'Unexpected response: {}'.format(res)
        assert len(res['LoadBalancers']), 'Unexpected response: {}'.format(res)
        lb = res['LoadBalancers'][0]
        current_state = lb['State']['Code']

        print('Current {} ELB state: {}'.format(target_lb['DNSName'], current_state))
        if current_state == desired_state:
            break

        time.sleep(delay)


@aws_client('elb')
def get_classic_target_health(load_balancer_name, instances, *, elb):
    """Return the health status of each specified instance in the target group.

    :param str target_group_arn:
        ARN of the target group.
    :param list instances:
        A list of EC2 Instance resources.

    :returns:
        A dictionary, keyed on instance ID.

    """

    res = elb.describe_instance_health(
        LoadBalancerName=load_balancer_name,
        Instances=[{'InstanceId': inst.instance_id} for inst in instances],
    )

    return {
        state['InstanceId']: state['State']
        for state in res['InstanceStates']
    }


@aws_client('elbv2')
def get_target_health(target_group_arn, instances, *, elbv2):
    """Return the health status of each specified instance in the target group.

    :param str target_group_arn:
        ARN of the target group.
    :param list instances:
        A list of EC2 Instance resources.

    :returns:
        A dictionary, keyed on instance ID.

    """

    res = elbv2.describe_target_health(
        TargetGroupArn=target_group_arn,
        Targets=[{'Id': inst.instance_id} for inst in instances],
    )

    return {
        thd['Target']['Id']: thd['TargetHealth']['State']
        for thd in res['TargetHealthDescriptions']
    }


def create_classic_health_checker(lb, instances):
    """Create a function to easily check the health of the specified instances
    attached to the specified classic load balancer.

    :param dict lb:
        A dictionary with information about a classic load balancer.
    :param list instances:
        A list of EC2 Instance resources.

    :returns:
        A function.

    """

    def wrapped(*expected):
        assert len(expected) == len(instances)

        lb_name = lb['LoadBalancerName']
        print('Checking health for classic load balancer: {}'.format(lb_name))
        health = get_classic_target_health(lb_name, instances)
        for idx, inst in enumerate(instances):
            state = health[inst.instance_id]
            want = expected[idx]
            assert state == want, 'unexpected state for instance {}: expected {}, got {}'.format(
                inst.instance_id, want, state
            )

    return wrapped


def create_health_checker(target_group, instances):
    """Create a function to easily check the health of the specified instances in the specified
    target group.

    :param dict target_group:
        A dictionary with information about a target group.
    :param list instances:
        A list of EC2 Instance resources.

    :returns:
        A function.

    """

    def wrapped(*expected):
        assert len(expected) == len(instances)

        print('Checking health for target group: {}'.format(target_group['TargetGroupName']))
        health = get_target_health(target_group['TargetGroupArn'], instances)
        for idx, inst in enumerate(instances):
            state = health[inst.instance_id]
            want = expected[idx]
            assert state == want, 'unexpected state for instance {}: expected {}, got {}'.format(
                inst.instance_id, want, state
            )

    return wrapped
