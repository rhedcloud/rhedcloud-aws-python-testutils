import pytest


def pytest_runtest_makereport(item, call):
    if "incremental" in item.keywords:
        if call.excinfo is not None:
            parent = item.parent
            parent._prev_failed = item


def pytest_runtest_setup(item):
    if "incremental" in item.keywords:
        prev_failed = getattr(item.parent, '_prev_failed', None)
        if prev_failed is not None:
            pytest.xfail('previous test failed ({})'.format(prev_failed.name))
