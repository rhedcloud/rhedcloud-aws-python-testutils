# rhedcloud-aws-python-testutils

This is the source of functions 1) common to more than one test and 2) used for reporting.

## Copyright

This software was created at Emory University. Please see NOTICE.txt for Emory's copyright and disclaimer notifications. This software was extended and modified by the RHEDcloud Foundation and is copyright 2019 by the RHEDcloud Foundation.

## License

This software is licensed under the Apache 2.0 license included in this distribution as LICENSE-2.0.txt
