test:
	pytest

build:
	python setup.py sdist bdist_wheel

docs:
	$(MAKE) -C ./docs clean html epub
	cd docs/build/html ; zip -r ../../../rhedcloud-aws-python-testutils-docs.zip .

clean:
	rm -rf ./build/ ./dist/

.PHONY: test build docs clean
