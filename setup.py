from setuptools import setup


setup(
    name='rhedcloud-aws-python-testutils',
    version='0.1.0-dev',
    description='RHEDcloud utility functions for testing AWS CloudFormation templates',
    author='RHEDcloud AWS IT Arch',
    author_email='aws-itarch@mscloud.emory.net',
    url='https://bitbucket.org/itarch/emory-aws-python-testutils',
    packages=['aws_test_functions'],
)
