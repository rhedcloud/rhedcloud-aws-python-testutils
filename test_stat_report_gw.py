import os
pathDir=os.environ.get('IT_ARCH_HOME')+'/'
print ("pathDir=",pathDir)
repoNames = ['rhedcloud-aws-rs-account-cfn', 'rhedcloud-aws-vpc-type1-cfn', 'rhedcloud-aws-org-standard-scp','rhedcloud-aws-vpc-type2-cfn']
repos = [
    {'name':repoNames[0], 'path':pathDir+repoNames[0]},
    {'name':repoNames[1], 'path':pathDir+repoNames[1]},
    {'name':repoNames[2], 'path':pathDir+repoNames[2]},
    {'name':repoNames[3], 'path':pathDir+repoNames[3]}
    ]
print (repos)

import os
import json
import re
import subprocess
import datetime
import pprint

def count_source_lines_and_scan_for_testcount(repo,type,file_name):
    test_count = None
    p = None
    if type=='structural' or type=='simulation':
        test_count = 1
    elif type=='functional':
        p = re.compile('\\$\\{testcount:.*}')
    else:
        raise Exception('bad test type', type)
    
    fname = repo['path']+'/tests/'+type+'/'+file_name
    with open(fname) as f:
        for i, l in enumerate(f):
            if not(test_count):
                s = p.search(l)
                #print(l)
                if s:
                    try:
                        test_count  = int(s.group().split(':')[1].split('}')[0]) 
                    except ValueError:
                        test_count = '#NAN'
                          
    return {'lines':i + 1, 'tests':test_count}

def get_file_stats_from_git(repo,type,file_name):
    pretty = '--pretty=format:{"name":"%an","timestamp":"%at"}'
    git_dir = repo['path']+'/.git'
    work_tree = repo['path']
    file_path = repo['path']+"/tests/"+type+"/"+file_name
    c = subprocess.run(["git", '--git-dir='+git_dir, '--work-tree='+work_tree, "log", pretty, file_path], stdout=subprocess.PIPE)
    d = c.stdout.decode('utf-8') #d is a string
    e = d.replace('\n',',')
    history_list = json.loads('['+e+']')
    if len(history_list)==0:
        authors=set({"#filehistory_not_found"})
        commits = None
    else:
        authors = set()
        commits = {'first':None,'last':None}
        for event in history_list:
            authors.add(event['name'])
            if not(commits['first']) or event['timestamp']<commits['first']:
                commits['first']=event['timestamp']
            if not(commits['last']) or event['timestamp']>commits['last']:
                commits['last']=event['timestamp']
           
    counts=count_source_lines_and_scan_for_testcount(repo,type,file_name)

    return {
        'authors':list(authors),
        'type':type,
        'name':file_name,
        'count':counts['tests'],
        'linecount':counts['lines'],
        'commits':commits,
        'template':repo['name'],
        'sortkey':commits['first']
        }

def main():
    global repos
    
    file_stats_list = []
    for repo in repos:
        if 'ignore' in repo and repo['ignore']:
            print("Ignoring repo '",repo['name'],"'")
            continue
        
        print(repo['name']+': structural:')
        try:
            structural_list = os.listdir(repo['path']+"/tests/structural")
            for file_name in structural_list:
                if (file_name.startswith('test_') or file_name.startswith('notest_')) and file_name.endswith('.py') and os.stat(repo['path']+"/tests/structural/"+file_name).st_size>0:
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo,'structural',file_name))
        except FileNotFoundError as err:
            print('No functional tests in ',repo['name'])        
            
        print(repo['name']+': functional:')
        try:
            functional_list = os.listdir(repo['path']+"/tests/functional")
            for file_name in functional_list:
                if (file_name.startswith('test_') or file_name.startswith('notest_')) and file_name.endswith('.py') and os.stat(repo['path']+"/tests/functional/"+file_name).st_size>0 :
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo,'functional',file_name))
        except FileNotFoundError as err:
            print('No functional tests in ',repo['name'])
    
        print(repo['name']+': simulation:')
        try:
            simulation_list = os.listdir(repo['path']+"/tests/simulation")
            for file_name in simulation_list:
                if (file_name.startswith('test_') or file_name.startswith('notest_')) and file_name.endswith('.py') and os.stat(repo['path']+"/tests/simulation/"+file_name).st_size>0:
                    print(file_name)
                    file_stats_list.append(get_file_stats_from_git(repo,'simulation',file_name))
        except FileNotFoundError as err:
            print('No simulation tests in ',repo['name'])
        
    
    #sort list of events
    file_stats_list = sorted(file_stats_list, key=lambda file_stat: file_stat['sortkey'])
        
    #create comma separated list and count lines
    total_lines = 0
    test_count = 0
    testers = {}
    "CSV-------------"
    #csv_lines='"Repo","Type","Name","Count","Lines","First Commit","Last Commit","Author(s)","Running Total Tests","Running Total Lines","RT Lines / 100"\n'
    last_valid_date_str='';
    test_csv_lines='"Dates","Total Tests"\n'
    line_csv_lines='"Dates","Total  Lines"\n'
    for fs in file_stats_list:
        tcsv_line = '"'
        lcsv_line = '"'
        if fs['commits']:
            dtlast = datetime.datetime.fromtimestamp(int(fs['commits']['first'])).date()
            last_valid_date_str=str(dtlast)
            tcsv_line+=str(dtlast)+'",'
            lcsv_line+=str(dtlast)+'",'
        else:
            tcsv_line+=last_valid_date_str+'",'
            lcsv_line+=last_valid_date_str+'",' 
        total_lines+=fs['linecount']
        if type(fs['count']) is int:
            test_count+=fs['count']
        tcsv_line+=str(test_count)+"\n"
        lcsv_line+=str(total_lines)+"\n"
        test_csv_lines+=tcsv_line
        line_csv_lines+=lcsv_line
    "CSV-END-------------"
    # write csv file
    f = open('TestCount.csv','w')    
    f.write(test_csv_lines)
    f = open('LineCount.csv','w')    
    f.write(line_csv_lines)
    os.rename("TestCount.csv", "/opt/openeai/dev/feedfile/AwsTestStat/TestCount.csv")
    os.rename("LineCount.csv", "/opt/openeai/dev/feedfile/AwsTestStat/LineCount.csv")
    # write json file
    file_stats_list_string = json.dumps(file_stats_list)
    f = open('file_stats.json','w')
    f.write(file_stats_list_string)

    print("Testers:")
    pprint.pprint(testers)
        
    print("Total tests: {:,.0f}".format(test_count))
    print ("Lines of code: {:,.0f}".format(total_lines))
        
if __name__ == "__main__":
    main()
