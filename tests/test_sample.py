def func(x):
    return x + 1


def test_answer():
    print("test_answer():")
    assert func(4) == 5
    return func(4) == 5


def main():
    print("test_answer() results: ", test_answer())


if __name__ == "__main__":
    main()
