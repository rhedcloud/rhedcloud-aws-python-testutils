import boto3
import uuid
from botocore.exceptions import ClientError

from aws_test_functions import deleteBucket

def test_answer():
    print("test_answer(): Start")
    #1 - create a bucket
    s3r = boto3.resource("s3")
    bucket = s3r.Bucket("testbucket"+str(uuid.uuid4()))
    bucket.create(ACL='public-read-write')
    bucket.wait_until_exists()
    #2 - put 10 objects in the bucket
    for x in range(0, 10):
        key_name = "upload_file_"+str(x)+".rtf"
        bucket.upload_file('upload_file.rtf', key_name)
        o = boto3.resource('s3').Object(bucket.name,key_name)
        o.wait_until_exists()
    objectList = boto3.client('s3').list_objects(Bucket=bucket.name)['Contents']
    if len(objectList) == 10:
        objectSet = set()
        for object in objectList:
            objectSet.add(object['Key'])
        for x in range(0, 10):
            key_name = "upload_file_"+str(x)+".rtf"
            if not(key_name in objectSet):
                print('FAILED: key_name, '+key_name+' not found in bucket')
                assert False
                return 'FAILED: key_name, '+key_name+' not found in bucket'
                
    #3 - call deleteBucket
        deleteBucket(bucket.name)
        
    #4 - verify that the bucket doesn't exist
        try:
            boto3.client('s3').list_objects(Bucket=bucket.name)
            print('FAILED to delete bucket '+bucket.name)
            assert False
            return 'FAILED to delete bucket '+bucket.name
        except ClientError as e:
            if e.response['ResponseMetadata']['HTTPStatusCode'] == 404:
                assert True
                return 'SUCCESS: bucket deleted'
            else:
                print ("Unexpected error: %s" % e)
                raise e                    
    else:
        print('FAILED: expected 10 objects in bucket but found '+str(len(objectList)))
        assert False
        return 'FAILED: expected 10 objects in bucket but found '+str(len(objectList))
 

def main():
    print("test_answer() results: ",test_answer())

if __name__ == "__main__":
    main()
