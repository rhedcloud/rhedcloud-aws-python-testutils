import pytest

from aws_test_functions import env


@pytest.mark.parametrize('value,expected', (
    (None, 5),
    ('', 5),
    ('6', 6),
    ('100', 100),
    ('-6', 5),
    ('Magic', 5),
))
def test_iam_delay(mocker, value, expected):
    getenv = mocker.patch.object(env.os, 'getenv')
    getenv.return_value = value

    env._load_iam_delay()

    assert env.IAM_DELAY == expected
