import pytest

from aws_test_functions import get_account_number, get_policy_arn


@pytest.mark.parametrize('policy,expected', ((
    'arn:aws:iam::99999:policy/RHEDcloudManagementSubnetPolicy',
    'arn:aws:iam::99999:policy/RHEDcloudManagementSubnetPolicy'
), (
    'AmazonEC2FullAccess',
    'arn:aws:iam::aws:policy/AmazonEC2FullAccess'
), (
    'RHEDcloudManagementSubnetPolicy',
    'arn:aws:iam::{}:policy/rhedcloud/RHEDcloudManagementSubnetPolicy'.format(get_account_number())
)))
def test_get_policy_arn(policy, expected):
    assert get_policy_arn(policy) == expected
