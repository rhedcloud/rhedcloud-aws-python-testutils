from aws_test_functions import get_account_info


def test_answer():
    account_info = get_account_info()
    assert account_info
    return account_info


def main():
    print("test_answer() results: ", test_answer())


if __name__ == "__main__":
    main()
