import boto3

from aws_test_functions import (
    ClientError,
    attach_policy_to_user,
    create_session_for_test_user,
    create_test_policy_using_client,
    create_test_user,
    delete_test_user,
)

"""
    There be 6 tests in this here script:

    1. test_createTestUser(): unit test of aws_test_functions.create_test_user()
    2. test_createSessionForTestUser(): unit test of aws_test_functions.create_session_for_test_user()
    3. test_createTestPolicyUsingClient(): unit test of aws_test_functions.create_test_policy_using_client()
    4. test_attachPolicyToUser(): unit test of aws_test_functions.attach_policy_to_user()
    5. test_deleteTestPolicyUsingClient(): unit test of aws_test_functions.deleteTestPolicyUsingClient()
    6. test_deleteTestUser(): unit test of aws_test_functions.delete_test_user()

"""

user = None
session = None
policy = None


def test_createTestUser():  # 1 ------------
    global user
    try:
        user = create_test_user("test_dependent_functions")
        assert user
        return user
    except ClientError as e:
        print("Unexpected error: %s" % e)
        assert False
        return e


def test_createSessionForTestUser():  # 2 ------------
    global user, session
    if user:
        try:
            session = create_session_for_test_user(user["UserName"])
            print("session=", session)
            assert session
            return session
        except ClientError as e:
            print("Unexpected error: %s" % e)
            assert False
            return e
    else:
        assert False
        return "No user"


def test_createTestPolicyUsingClient():  # 3 ------------
    global policy
    try:
        policy = create_test_policy_using_client(boto3.client("iam"))["Policy"]
        print("policy = ", policy)
        assert policy
        return policy
    except ClientError as e:
        print("Unexpected error: %s" % e)
        assert False
        return e


def test_attachPolicyToUser():  # 4 ------------
    global user, policy
    if user and policy:
        try:
            attach_policy_to_user(user["UserName"], policy["Arn"])
            assert True
            return policy["Arn"] + " attached to " + user["UserName"]
        except ClientError as e:
            print("Unexpected error: %s" % e)
            assert False
            return e
    else:
        assert False
        return "No user or policy"


def test_deleteTestPolicyUsingClient():  # 5 ------------
    global session, policy
    if policy and session:
        try:
            policy = create_test_policy_using_client(boto3.client("iam"))
            print("policy = ", policy)
            assert policy
            return policy
        except ClientError as e:
            print("Unexpected error: %s" % e)
            assert False
            return e
    else:
        assert False
        return "No policy or session"


def test_deleteTestUser():  # 6 ------------
    global user
    if user:
        try:
            delete_test_user(user["UserName"])
            print(user["UserName"] + " deleted")
            assert True
            return user["UserName"] + " deleted"
        except ClientError as e:
            print("Unexpected error: %s" % e)
            assert False
            return e
    else:
        assert False
        return "No user"


def main():
    print("#1: test_createTestUser() results: ", test_createTestUser())
    print(
        "#2: test_createSessionForTestUser() results: ", test_createSessionForTestUser()
    )
    print(
        "#3: test_createTestPolicyUsingClient() results: ",
        test_createTestPolicyUsingClient(),
    )
    print("#4: test_attachPolicyToUser() results: ", test_attachPolicyToUser())
    print(
        "#5: test_deleteTestPolicyUsingClient() results: ",
        test_deleteTestPolicyUsingClient(),
    )
    print("#6: test_deleteTestUser() results: ", test_deleteTestUser())


if __name__ == "__main__":
    main()
