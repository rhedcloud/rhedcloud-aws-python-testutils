from copy import copy

import pytest

from aws_test_functions import snowball as mod


def _with_id(addr_id, addr=None):
    """Helper to inject the specified ``addr_id`` into an address dictionary.

    :param str addr_id:
        Fake ID for the Address.
    :param dict addr: (optional)
        Address record to inject ID into.

    :returns:
        A dictionary.

    """

    if addr is None:
        addr = mod._SAMPLE_ADDRESS

    addr = copy(addr)
    addr['AddressId'] = addr_id

    return addr


@pytest.fixture(scope='function')
def snowball(mocker):
    snowball = mocker.Mock()
    client = mocker.patch('boto3.client')
    client.return_value = snowball

    return snowball


@pytest.mark.parametrize('expect_create,addrs,expected', (
    (True, {'Addresses': []}, _with_id('456')),
    (False, {'Addresses': [_with_id('123'), _with_id('789')]}, _with_id('123')),
))
def test_get_or_create_address(mocker, snowball, expect_create, addrs, expected):
    snowball.describe_addresses.return_value = addrs
    snowball.create_address.side_effect = lambda **kw: _with_id('456', kw['Address'])

    res = mod.get_or_create_address()

    snowball.describe_addresses.assert_called_once_with()
    if expect_create:
        snowball.create_address.assert_called_once_with(Address=mod._SAMPLE_ADDRESS)

    assert res == expected, 'unexpected address'


def test_new_snowball_job(mocker):
    role = mocker.Mock()

    create_snowball_job = mocker.patch.object(mod, 'create_snowball_job')
    create_snowball_job.return_value = '555'

    cancel_snowball_job = mocker.patch.object(mod, 'cancel_snowball_job')

    with mod.new_snowball_job(role, 'a_bucket') as job_id:
        assert job_id == '555'

    create_snowball_job.assert_called_once_with(role, 'a_bucket', snowball=mocker.ANY)
    cancel_snowball_job.assert_called_once_with('555', snowball=mocker.ANY)


@pytest.mark.parametrize('snowball_type,expected_type', (
    (None, 'STANDARD'),
    ('STANDARD', 'STANDARD'),
    ('EDGE', 'EDGE'),
    pytest.param('INVALID', None, marks=pytest.mark.xfail),
))
def test_create_snowball_job(mocker, snowball, snowball_type, expected_type):
    role = mocker.Mock(arn='000')

    get_or_create_address = mocker.patch.object(mod, 'get_or_create_address')
    get_or_create_address.return_value = {'AddressId': '123'}

    get_or_create_kms_key_arn = mocker.patch.object(mod, 'get_or_create_kms_key_arn')
    get_or_create_kms_key_arn.return_value = '456'

    snowball.create_job.return_value = {'JobId': '999'}

    res = mod.create_snowball_job(role, 'a_bucket', snowball_type=snowball_type)

    expected_params = dict(
        AddressId='123',
        KmsKeyARN='456',
        RoleARN='000',
        Description='Test Snowball',
        JobType='EXPORT',
        Resources={
            'S3Resources': [{
                'BucketArn': 'arn:aws:s3:::a_bucket',
            }],
        },
        ShippingOption='SECOND_DAY',
        SnowballType=expected_type,
    )

    if snowball_type == 'EDGE':
        expected_params['SnowballCapacityPreference'] = 'T100'

    snowball.create_job.assert_called_once_with(**expected_params)

    assert res == '999'


def test_cancel_snowball_job(mocker, snowball):
    snowball.cancel_job.return_value = {}
    mod.cancel_snowball_job('1234jobid')
    snowball.cancel_job.assert_called_once_with(JobId='1234jobid')
