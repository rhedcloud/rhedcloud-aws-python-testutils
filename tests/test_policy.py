import boto3
import pytest

from aws_test_functions import (
    attach_policy,
    find_policy_for_stack,
    new_test_user,
)


def test_attach_policy():
    with new_test_user('test_policy') as user:
        assert len(list(user.attached_policies.all())) == 0

        with attach_policy(user, 'AmazonEC2FullAccess', 'AmazonS3FullAccess'):
            assert len(list(user.attached_policies.all())) == 2

        assert len(list(user.attached_policies.all())) == 0


def test_attach_policy_exception():
    with pytest.raises(ValueError):
        with new_test_user('test_policy') as user:
            assert len(list(user.attached_policies.all())) == 0

            with attach_policy(user, 'AmazonEC2FullAccess', 'AmazonS3FullAccess'):
                assert len(list(user.attached_policies.all())) == 2
                raise ValueError


@pytest.fixture
def fake_iam(mocker):
    policy1 = mocker.Mock(arn='arn:aws:iam::9999:policy/stack-name-PolicyName')
    policy2 = mocker.Mock(arn='arn:aws:iam::9999:policy/rhedcloud/stack-name-OtherPolicyName')
    policy3 = mocker.Mock(arn='arn:aws:iam::aws:policy/AmazonEC2FullAccess')

    iam = mocker.MagicMock(boto3.resource('iam'))
    iam.policies.all.return_value = [policy1, policy2, policy3]

    return iam


@pytest.mark.parametrize('policy,expected', ((
    'OtherPolicyName', 'arn:aws:iam::9999:policy/rhedcloud/stack-name-OtherPolicyName',
), (
    'AmazonEC2FullAccess', 'arn:aws:iam::aws:policy/AmazonEC2FullAccess',
), (
    'DoesntExist', 'DoesntExist',
)))
def test_find_policy_for_stack(fake_iam, policy, expected):
    assert find_policy_for_stack('stack-name', policy, iam=fake_iam) == expected
