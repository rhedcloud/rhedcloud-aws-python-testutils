import boto3
import pytest

from aws_test_functions import aws_client


def test_aws_client_iam_implicit(capsys):
    @aws_client('iam')
    def demofunc(iam=None):
        print(type(iam))

    demofunc()

    out, err = capsys.readouterr()
    assert 'botocore.client.IAM' in out


def test_aws_client_iam_explicit(capsys):
    c = boto3.client('iam')

    @aws_client('iam')
    def demofunc(iam=None):
        print(type(iam))

    demofunc(iam=c)

    out, err = capsys.readouterr()
    assert 'botocore.client.IAM' in out


def test_aws_client_iam_service_resource(capsys):
    r = boto3.resource('iam')

    @aws_client('iam')
    def demofunc(iam=None):
        print(type(iam))

    demofunc(iam=r)

    out, err = capsys.readouterr()
    assert 'botocore.client.IAM' in out


def test_aws_client_iam_invalid_type():
    r = dict()

    @aws_client('iam')
    def demofunc(iam=None):
        print(type(iam))

    with pytest.raises(TypeError) as exc:
        demofunc(iam=r)

    assert 'expected AWS Client for parameter "iam"' in str(exc)


def test_aws_client_lambda_explicit(capsys):
    c = boto3.client('lambda')

    @aws_client('lambda_client', 'lambda')
    def demofunc(lambda_client=None):
        print(type(lambda_client))

    demofunc(lambda_client=c)

    out, err = capsys.readouterr()
    assert 'botocore.client.Lambda' in out
