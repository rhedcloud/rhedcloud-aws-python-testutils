import boto3
import uuid
from aws_test_functions import bucketExists, waitForBucketExists, waitForBucketNotExists

bucketName = None

def test_setup_create_bucket():
    global bucketName
    bucketName = "testbucket-"+str(uuid.uuid4())
    boto3.client('s3').create_bucket(Bucket=bucketName)
    #s3 waiters: ['bucket_exists', 'bucket_not_exists', 'object_exists', 'object_not_exists']
    assert True
    return bucketName
    
def test_waitForBucketExists():
    bucketName
    waitForBucketExists(bucketName)
    assert True
    return True
    
def test_one():
    answer = bucketExists(bucketName)
    assert answer
    return answer
    
def test_setup_delete_bucket():
    global bucketName
    if bucketName:
        boto3.client('s3').delete_bucket(Bucket=bucketName)
        assert True
        return bucketName+' deleted'
        
def test_waitForBucketNotExists():
    global bucketName
    waitForBucketNotExists(bucketName)
    assert True
    return True        
        
def test_two():
    global bucketName
    answer = bucketExists(bucketName)
    assert not(answer)
    return not(answer)


def main():
    print("test_setup_create_bucket() results: ",test_setup_create_bucket())
    print("test_waitForBucketExists() results: ",test_waitForBucketExists())
    print("test_one() results: ",test_one())
    print("test_test_setup_delete_bucket() results: ",test_setup_delete_bucket())
    print("test_waitForBucketNotExists() results: ",test_waitForBucketNotExists())
    print("test_two() results: ",test_two())

if __name__ == "__main__":
    main()
