"""Tests to verify S3 bucket-related functions.

Tests include:

* creating buckets
* adding objects to buckets
* verifying buckets ARE NOT empty
* verifying that uploaded objects exist in buckets
* emptying buckets
* verifying buckets ARE empty
* deleting buckets

This test creates a bucket and objects that must be removed
"""

import pytest

from aws_test_functions.lib import (
    create_bucket,
    delete_bucket,
    empty_bucket,
    find_objects_in_bucket,
    get_uuid,
    is_bucket_empty,
    new_bucket,
)

TEST_KEYS = ('upload_file.rtf', 'upload_file-2.rtf')


@pytest.fixture(scope='module')
def bucket():
    with new_bucket('test_buckets') as bucket:
        yield bucket


@pytest.mark.incremental
class TestBuckets:

    def test_0_bucket_empty(self, bucket):
        assert is_bucket_empty(bucket.name)

    def test_1_upload_files(self, bucket):
        for key in TEST_KEYS:
            bucket.upload_file('./tests/upload_file.rtf', key)
        assert not is_bucket_empty(bucket.name)

    def test_2_find_files(self, bucket):
        found = find_objects_in_bucket(bucket.name, *TEST_KEYS)

        for key in TEST_KEYS:
            assert found.get(key) is True, '{} not found'.format(key)

    def test_3_empty_bucket(self, bucket):
        empty_bucket(bucket.name)
        assert is_bucket_empty(bucket.name)

    def test_4_find_files(self, bucket):
        found = find_objects_in_bucket(bucket.name, *TEST_KEYS)

        for key in TEST_KEYS:
            assert not found.get(key) is True, '{} found'.format(key)


def test_create_and_delete():
    name = 'test-bucket-{}'.format(get_uuid())[:63]
    try:
        bucket = create_bucket(name)
        assert bucket is not None
    finally:
        delete_bucket(name)
