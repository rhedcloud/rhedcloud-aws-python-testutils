from datetime import datetime

import boto3
import pytest

from aws_test_functions import kms as mod


KEYS = [{
    'AWSAccountId': '462544485824',
    'KeyId': '253f5215-ce57-441c-9e02-adbec2f9c5bf',
    'Arn': 'arn:aws:kms:us-east-1:462544485824:key/253f5215-ce57-441c-9e02-adbec2f9c5bf',
    'CreationDate': datetime(2017, 12, 19, 22, 3, 13, 142000),
    'Enabled': False,
    'Description': 'test key created: 2017-12-20 03:03:12.897910',
    'KeyUsage': 'ENCRYPT_DECRYPT',
    'KeyState': 'PendingDeletion',
    'DeletionDate': datetime(2017, 12, 27, 19, 0),
    'Origin': 'AWS_KMS',
    'KeyManager': 'CUSTOMER'
}, {
    'AWSAccountId': '462544485824',
    'KeyId': '6c549e34-8ee6-4640-88b0-386bf986ef97',
    'Arn': 'arn:aws:kms:us-east-1:462544485824:key/6c549e34-8ee6-4640-88b0-386bf986ef97',
    'CreationDate': datetime(2017, 12, 21, 14, 55, 21, 337000),
    'Enabled': True,
    'Description': 'Default master key that protects my EBS volumes when no other key is defined',
    'KeyUsage': 'ENCRYPT_DECRYPT',
    'KeyState': 'Enabled',
    'Origin': 'AWS_KMS',
    'KeyManager': 'AWS',
}, {
    'AWSAccountId': '462544485824',
    'KeyId': '7cb19bb1-415a-4da1-ac4d-842ca34ef19d',
    'Arn': 'arn:aws:kms:us-east-1:462544485824:key/7cb19bb1-415a-4da1-ac4d-842ca34ef19d',
    'CreationDate': datetime(2017, 12, 7, 12, 47, 2, 643000),
    'Enabled': True,
    'Description': '',
    'KeyUsage': 'ENCRYPT_DECRYPT',
    'KeyState': 'Enabled',
    'Origin': 'AWS_KMS',
    'KeyManager': 'CUSTOMER',
}]

LIST_KEYS_KEYS = [
    {'KeyId': k['KeyId'], 'KeyArn': k['Arn']}
    for k in KEYS
]


@pytest.fixture
def kms(mocker):
    return mocker.MagicMock(boto3.client('kms'))


def test_list_all_kms_keys(mocker, kms):
    kms.list_keys.side_effect = [{
        'Keys': LIST_KEYS_KEYS[:2],
        'Truncated': False,
        'NextMarker': 'something',
    }, {
        'Keys': LIST_KEYS_KEYS[2:],
        'Truncated': True,
        'NextMarker': None,
    }]

    gen = mod.list_all_kms_keys(limit=2, kms=kms)
    key = next(gen)
    assert key['KeyArn'] == 'arn:aws:kms:us-east-1:462544485824:key/253f5215-ce57-441c-9e02-adbec2f9c5bf'

    key = next(gen)
    assert key['KeyArn'] == 'arn:aws:kms:us-east-1:462544485824:key/6c549e34-8ee6-4640-88b0-386bf986ef97'

    key = next(gen)
    assert key['KeyArn'] == 'arn:aws:kms:us-east-1:462544485824:key/7cb19bb1-415a-4da1-ac4d-842ca34ef19d'

    with pytest.raises(StopIteration):
        next(gen)

    assert kms.list_keys.call_count == 2
    kms.list_keys.assert_any_call(Limit=2)
    kms.list_keys.assert_any_call(Limit=2, Marker='something')


def test_get_key_arn(mocker, kms):
    kms.describe_key.side_effect = [
        {'KeyMetadata': k}
        for k in KEYS
    ]

    list_all_kms_keys = mocker.patch.object(mod, 'list_all_kms_keys')
    list_all_kms_keys.return_value = LIST_KEYS_KEYS

    arn = mod.get_or_create_kms_key_arn(kms=kms)

    assert arn == 'arn:aws:kms:us-east-1:462544485824:key/7cb19bb1-415a-4da1-ac4d-842ca34ef19d'


def test_create_key(mocker, kms):
    kms.create_key.return_value = {'KeyMetadata': {'Arn': 'fake'}}

    list_all_kms_keys = mocker.patch.object(mod, 'list_all_kms_keys')
    list_all_kms_keys.return_value = []

    arn = mod.get_or_create_kms_key_arn(kms=kms)

    kms.describe_key.assert_not_called()
    kms.create_key.assert_called_once_with()
    assert arn == 'fake'
