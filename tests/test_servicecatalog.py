from datetime import datetime

import boto3
import pytest

from aws_test_functions import servicecatalog as mod


@pytest.fixture
def client(
    mocker,
    create_portfolio_response,
    create_product_response,
    create_constraint_response,
    create_provisioned_product_plan_response,
):
    c = mocker.MagicMock(boto3.client('servicecatalog'))
    mocker.patch('time.sleep')

    c.create_portfolio.return_value = create_portfolio_response
    c.create_product.return_value = create_product_response
    c.associate_product_with_portfolio.return_value = {}
    c.create_constraint.return_value = create_constraint_response
    c.associate_principal_with_portfolio.return_value = {}
    c.create_provisioned_product_plan.return_value = create_provisioned_product_plan_response

    return c


@pytest.fixture
def portfolio_id():
    return 'port-1234567abcdef'


@pytest.fixture
def product_id():
    return 'prod-1234567abcdef'


@pytest.fixture
def role(mocker):
    c = mocker.MagicMock(boto3.resource('iam'))
    r = c.Role('test')
    r.arn = 'aaarrrrnnn'

    return r


@pytest.fixture
def constraint_id():
    return 'cons-1234567abcdef'


@pytest.fixture
def artifact_id():
    return 'pa-1234567abcdef'


@pytest.fixture
def plan_id():
    return 'pl-1234567abcdef'


@pytest.fixture
def create_portfolio_response(portfolio_id):
    return {
        'PortfolioDetail': {
            'Id': portfolio_id,
            'ARN': 'string',
            'DisplayName': 'string',
            'Description': 'string',
            'CreatedTime': datetime(2015, 1, 1),
            'ProviderName': 'string'
        },
        'Tags': [{
            'Key': 'string',
            'Value': 'string'
        }],
    }


@pytest.fixture
def create_product_response(product_id):
    return {
        'ProductViewDetail': {
            'ProductViewSummary': {
                'Id': 'string',
                'ProductId': product_id,
                'Name': 'string',
                'Owner': 'string',
                'ShortDescription': 'string',
                'Type': 'CLOUD_FORMATION_TEMPLATE',
                'Distributor': 'string',
                'HasDefaultPath': False,
                'SupportEmail': 'string',
                'SupportDescription': 'string',
                'SupportUrl': 'string'
            },
            'Status': 'CREATING',
            'ProductARN': 'string',
            'CreatedTime': datetime(2015, 1, 1)
        },
        'ProvisioningArtifactDetail': {
            'Id': 'string',
            'Name': 'string',
            'Description': 'string',
            'Type': 'CLOUD_FORMATION_TEMPLATE',
            'CreatedTime': datetime(2015, 1, 1),
            'Active': True
        },
        'Tags': [{
            'Key': 'string',
            'Value': 'string'
        }]
    }


@pytest.fixture
def create_constraint_response(constraint_id):
    return {
        'ConstraintDetail': {
            'ConstraintId': constraint_id,
            'Type': 'string',
            'Description': 'string',
            'Owner': 'string'
        },
        'ConstraintParameters': 'string',
        'Status': 'CREATING'
    }


@pytest.fixture
def create_provisioned_product_plan_response(plan_id, product_id, artifact_id):
    return {
        'PlanName': 'string',
        'PlanId': plan_id,
        'ProvisionProductId': product_id,
        'ProvisionedProductName': 'string',
        'ProvisioningArtifactId': artifact_id,
    }


def test_new_portfolio(client, mocker, portfolio_id):
    with mod.new_portfolio('foo', servicecatalog=client) as p:
        assert 'Id' in p
        assert p['Id'] == portfolio_id

    client.create_portfolio.assert_called_once_with(
        DisplayName=mocker.ANY,
        ProviderName=mocker.ANY,
        IdempotencyToken=mocker.ANY,
    )

    client.delete_portfolio.assert_called_once_with(
        Id=portfolio_id,
    )


def test_new_product(client, mocker, product_id):
    url = 'http://some.local/file.json'
    with mod.new_product('foo', url, servicecatalog=client) as p:
        assert 'ProductViewDetail' in p
        assert 'ProductViewSummary' in p['ProductViewDetail']
        assert 'ProductId' in p['ProductViewDetail']['ProductViewSummary']
        assert p['ProductViewDetail']['ProductViewSummary']['ProductId'] == product_id

    client.create_product.assert_called_once_with(
        Name=mocker.ANY,
        Owner='rhedcloud',
        ProductType='CLOUD_FORMATION_TEMPLATE',
        ProvisioningArtifactParameters=dict(
            Info=dict(
                LoadTemplateFromURL=url,
            ),
            Type='CLOUD_FORMATION_TEMPLATE',
        ),
        IdempotencyToken=mocker.ANY,
    )

    client.delete_product.assert_called_once_with(
        Id=product_id,
    )


def test_new_portfolio_product(client, mocker, portfolio_id, product_id):
    with mod.new_portfolio_product(
        portfolio_id,
        product_id,
        servicecatalog=client,
    ):
        pass

    client.associate_product_with_portfolio.assert_called_once_with(
        ProductId=product_id,
        PortfolioId=portfolio_id,
    )

    client.disassociate_product_from_portfolio.assert_called_once_with(
        ProductId=product_id,
        PortfolioId=portfolio_id,
    )


def test_new_launch_constraint(client, mocker, portfolio_id, product_id, role, constraint_id):
    with mod.new_launch_constraint(
        portfolio_id,
        product_id,
        role,
        servicecatalog=client,
    ) as c:
        assert 'ConstraintDetail' in c
        assert 'ConstraintId' in c['ConstraintDetail']
        assert c['ConstraintDetail']['ConstraintId'] == constraint_id

    client.create_constraint.assert_called_once_with(
        PortfolioId=portfolio_id,
        ProductId=product_id,
        Parameters='{"RoleArn": "aaarrrrnnn"}',
        Type='LAUNCH',
    )

    client.delete_constraint.assert_called_once_with(
        Id=constraint_id,
    )


def test_new_portfolio_principal(client, mocker, portfolio_id, role):
    with mod.new_portfolio_principal(
        portfolio_id,
        role.arn,
        servicecatalog=client,
    ):
        pass

    client.associate_principal_with_portfolio.assert_called_once_with(
        PrincipalType='IAM',
        PortfolioId=portfolio_id,
        PrincipalARN=role.arn,
    )

    client.disassociate_principal_from_portfolio.assert_called_once_with(
        PortfolioId=portfolio_id,
        PrincipalARN=role.arn,
    )


def test_new_product_plan(client, mocker, product_id, artifact_id, plan_id):
    with mod.new_product_plan(
        'foo',
        product_id,
        artifact_id,
        servicecatalog=client,
    ) as pid:
        assert pid == plan_id

    client.create_provisioned_product_plan.assert_called_once_with(
        PlanName=mocker.ANY,
        PlanType='CLOUDFORMATION',
        ProductId=product_id,
        ProvisionedProductName=mocker.ANY,
        ProvisioningArtifactId=artifact_id,
    )

    client.delete_provisioned_product_plan.assert_called_once_with(
        PlanId=plan_id,
    )
