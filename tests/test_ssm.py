import pytest

from aws_test_functions import SSMResult


@pytest.fixture(scope='module')
def stdout():
    return 'sample output\nmore output\n'


@pytest.fixture(scope='module')
def stderr():
    return 'errors happened\n'


@pytest.fixture(scope='module')
def res(stdout):
    return SSMResult({
        'StandardOutputContent': stdout,
        'StandardErrorContent': '',
        'Status': 'Success',
    })


@pytest.fixture(scope='module')
def failedres(stdout, stderr):
    return SSMResult({
        'StandardOutputContent': stdout,
        'StandardErrorContent': stderr,
        'Status': 'Fail',
    })


def test_result_acts_like_string(res, stdout):
    assert res == stdout
    assert 'ample' in res
    assert 'PUT' not in res
    assert res.capitalize().startswith('Sample output')
    assert res.title().startswith('Sample Output')
    assert res.count('u') == 4
    assert res.strip() == stdout[:-1]
    assert len(res) == len(stdout)


def test_result_acts_like_dict(res):
    assert len(list(res.keys())) == 3
    assert len(list(res.values())) == 3
    assert res['Status'] == 'Success'

    with pytest.raises(KeyError):
        assert res['BadKey']


def test_failed_result(failedres, stdout, stderr):
    assert SSMResult.OUTPUT_DIVIDER in failedres
    assert failedres.startswith(stdout)
    assert failedres.endswith(stderr)
