from datetime import datetime
import boto3
import pytest

from aws_test_functions import ClientError, efs


@pytest.fixture
def fake_efs(
    mocker,
    create_fs_response,
    create_mt_response,
    describe_file_systems_response,
    describe_mount_targets_response,
):
    client = mocker.MagicMock(boto3.client('efs'))
    mocker.patch('time.sleep')

    client.create_file_system.return_value = create_fs_response
    client.create_mount_target.return_value = create_mt_response
    client.describe_file_systems.return_value = describe_file_systems_response
    client.describe_mount_targets.return_value = describe_mount_targets_response

    return client


@pytest.fixture
def fs_id():
    return 'fs-1234abcd'


@pytest.fixture
def subnet_id():
    return 'subnet-1234abcd'


@pytest.fixture
def target_id():
    return 'fsmt-1234abcd'


@pytest.fixture
def create_fs_response(fs_id):
    return {
        'OwnerId': 'string',
        'CreationToken': 'string',
        'FileSystemId': fs_id,
        'CreationTime': datetime(2015, 1, 1),
        'LifeCycleState': 'creating',
        'Name': 'string',
        'NumberOfMountTargets': 123,
        'SizeInBytes': {
            'Value': 123,
            'Timestamp': datetime(2015, 1, 1)
        },
        'PerformanceMode': 'generalPurpose',
        'Encrypted': None,
        'KmsKeyId': 'string'
    }


@pytest.fixture
def create_mt_response(fs_id, subnet_id, target_id):
    return {
        'OwnerId': 'string',
        'MountTargetId': target_id,
        'FileSystemId': fs_id,
        'SubnetId': subnet_id,
        'LifeCycleState': 'creating',
        'IpAddress': 'string',
        'NetworkInterfaceId': 'string'
    }


@pytest.fixture
def describe_file_systems_response(fs_id):
    return {
        'Marker': 'string',
        'FileSystems': [{
            'OwnerId': 'string',
            'CreationToken': 'string',
            'FileSystemId': fs_id,
            'CreationTime': datetime(2015, 1, 1),
            'LifeCycleState': 'creating',
            'Name': 'string',
            'NumberOfMountTargets': 123,
            'SizeInBytes': {
                'Value': 123,
                'Timestamp': datetime(2015, 1, 1)
            },
            'PerformanceMode': 'generalPurpose',
            'Encrypted': True,
            'KmsKeyId': 'string'
        }],
        'NextMarker': 'string'
    }


@pytest.fixture
def describe_mount_targets_response(fs_id, subnet_id, target_id):
    return {
        'Marker': 'string',
        'MountTargets': [{
            'OwnerId': 'string',
            'MountTargetId': target_id,
            'FileSystemId': fs_id,
            'SubnetId': subnet_id,
            'LifeCycleState': 'creating',
            'IpAddress': 'string',
            'NetworkInterfaceId': 'string'
        }],
        'NextMarker': 'string'
    }


@pytest.mark.parametrize('encrypted', (False, True))
def test_new_filesystem(fake_efs, encrypted, mocker):
    with efs.new_filesystem(encrypted, efs=fake_efs) as fs_id:
        pass

    fake_efs.create_file_system.assert_called_once_with(
        CreationToken=mocker.ANY,
        Encrypted=encrypted,
    )
    fake_efs.delete_file_system.assert_called_once_with(
        FileSystemId=fs_id,
    )


@pytest.mark.parametrize('encrypted', (False, True))
def test_create_file_system(fake_efs, encrypted, mocker):
    efs.create_file_system(encrypted, efs=fake_efs)

    fake_efs.create_file_system.assert_called_once_with(
        CreationToken=mocker.ANY,
        Encrypted=encrypted,
    )


def test_delete_file_system(fake_efs, fs_id, target_id):
    efs.delete_file_system(fs_id, efs=fake_efs)

    fake_efs.delete_mount_target.assert_called_once_with(
        MountTargetId=target_id,
    )

    fake_efs.delete_file_system.assert_called_once_with(
        FileSystemId=fs_id,
    )


def test_delete_file_system_retry(fake_efs, fs_id):
    fake_efs.delete_file_system.side_effect = [
        ClientError({
            'Error': {
                'Code': 'FileSystemInUse',
                'Message': 'yadda yadda',
            },
        }, 'delete_file_system'),
        {}
    ]

    efs.delete_file_system(fs_id, efs=fake_efs)

    assert fake_efs.delete_file_system.call_count == 2
    fake_efs.delete_file_system.assert_called_with(
        FileSystemId=fs_id,
    )

    fake_efs.describe_mount_targets.assert_called_once_with(
        FileSystemId=fs_id,
    )


def test_delete_file_system_raise_is_caught(fake_efs, fs_id):
    fake_efs.delete_file_system.side_effect = [
        ClientError({
            'Error': {
                'Code': 'AccessDeniedException',
                'Message': 'yadda yadda',
            },
        }, 'delete_file_system'),
        {}
    ]

    efs.delete_file_system(fs_id, efs=fake_efs)

    assert fake_efs.delete_file_system.call_count == 1
    fake_efs.delete_file_system.assert_called_once_with(
        FileSystemId=fs_id,
    )


def test_cleanup_filesystems(fake_efs, mocker, fs_id):
    delete = mocker.patch.object(efs, 'delete_file_system')

    efs.cleanup_file_systems(efs=fake_efs)

    delete.assert_called_once_with(fs_id, efs=fake_efs)


def test_new_mount_target(fake_efs, fs_id, subnet_id):
    with efs.new_mount_target(fs_id, subnet_id, efs=fake_efs) as target_id:
        pass

    fake_efs.create_mount_target.assert_called_once_with(
        FileSystemId=fs_id,
        SubnetId=subnet_id,
    )
    fake_efs.delete_mount_target.assert_called_once_with(
        MountTargetId=target_id,
    )


def test_create_mount_target(fake_efs, fs_id, subnet_id):
    efs.create_mount_target(fs_id, subnet_id, efs=fake_efs)

    fake_efs.create_mount_target.assert_called_once_with(
        FileSystemId=fs_id,
        SubnetId=subnet_id,
    )


def test_delete_mount_target(fake_efs, target_id):
    efs.delete_mount_target(target_id, efs=fake_efs)

    fake_efs.delete_mount_target.assert_called_once_with(
        MountTargetId=target_id,
    )
