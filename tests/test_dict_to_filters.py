import pytest

from aws_test_functions import dict_to_filters


@pytest.mark.parametrize('kv,expected', ((
    {'tag:Name': 'test'},
    [{'Name': 'tag:Name', 'Values': ['test']}]
), (
    {'tag:Name': 'test', 'something': 'else'},
    [
        {'Name': 'something', 'Values': ['else']},
        {'Name': 'tag:Name', 'Values': ['test']},
    ]
), (
    {'tag:Name': ['test1', 'test2']},
    [{'Name': 'tag:Name', 'Values': ['test1', 'test2']}]
)))
def test_dict_to_filters(kv, expected):
    assert sorted(dict_to_filters(kv), key=lambda i: i['Name']) == expected
