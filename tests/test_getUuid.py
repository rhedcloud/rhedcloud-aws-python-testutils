from aws_test_functions import get_uuid


def test_answer():
    uuid = get_uuid()
    assert uuid
    return uuid


def main():
    print("test_answer() results: ", test_answer())


if __name__ == "__main__":
    main()
