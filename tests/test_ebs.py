import boto3
import pytest

from aws_test_functions import ebs as mod


@pytest.mark.parametrize('encrypted', (
    True,
    False,
    pytest.param(None, marks=pytest.mark.xfail),
))
def test_new_ebs_volume(mocker, encrypted):
    create_ebs_volume = mocker.patch.object(mod, 'create_ebs_volume')

    with mod.new_ebs_volume('prefix', 'an-az', encrypted=encrypted) as vol:
        assert vol is not None
        create_ebs_volume.assert_called_once_with(
            mocker.ANY,
            'an-az',
            10,
            encrypted,
            ec2=mocker.ANY,
        )
        vol.delete.assert_not_called()

    vol.delete.assert_called_once_with()


@pytest.mark.parametrize('encrypted', (
    True,
    False,
    pytest.param(None, marks=pytest.mark.xfail),
))
def test_create_ebs_volume(mocker, encrypted):
    ec2_client = mocker.MagicMock(boto3.client('ec2'))
    ec2_client.create_volume.return_value = {
        'VolumeId': 'an-id',
    }

    ec2_res = mocker.patch.object(mod.boto3, 'resource')

    vol = mod.create_ebs_volume('foo', 'bar', 10, encrypted, ec2=ec2_client)
    assert vol is not None

    ec2_client.create_volume.assert_called_once_with(
        AvailabilityZone='bar',
        Encrypted=encrypted,
        Size=10,
        TagSpecifications=[{
            'ResourceType': 'volume',
            'Tags': [{
                'Key': 'Name',
                'Value': 'foo',
            }],
        }],
    )

    ec2_res.assert_called_once_with('ec2')
    ec2_res().Volume.assert_called_once_with('an-id')
