import boto3
import pytest

from aws_test_functions import aws_resource


def test_aws_resource_iam_implicit(capsys):
    @aws_resource('iam')
    def demofunc(iam=None):
        print(type(iam))

    demofunc()

    out, err = capsys.readouterr()
    assert 'iam.ServiceResource' in out


def test_aws_resource_iam_explicit(capsys):
    c = boto3.resource('iam')

    @aws_resource('iam')
    def demofunc(iam=None):
        print(type(iam))

    demofunc(iam=c)

    out, err = capsys.readouterr()
    assert 'iam.ServiceResource' in out


def test_aws_resource_iam_invalid_type():
    c = boto3.client('iam')

    @aws_resource('iam')
    def demofunc(iam=None):
        print(type(iam))

    with pytest.raises(TypeError) as exc:
        demofunc(iam=c)

    assert 'expected AWS Service Resource for parameter "iam"' in str(exc)
