import boto3
import pytest

from aws_test_functions import ClientError, new_test_user


def test_new_test_user():
    username = None

    with pytest.raises(ValueError):
        with new_test_user('test_user') as user:
            username = user.user_name
            assert username.startswith('test_user-')
            raise ValueError

    assert username is not None

    with pytest.raises(ClientError):
        iam = boto3.client('iam')
        user = iam.get_user(UserName=username)

        assert False, 'user was not deleted'
