from aws_test_functions import get_account_number


def test_answer():
    account_number = get_account_number()
    print(account_number)
    assert account_number
    return account_number


def main():
    print("test_answer() results: ", test_answer())


if __name__ == "__main__":
    main()
