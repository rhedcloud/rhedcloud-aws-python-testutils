import boto3
import pytest

from aws_test_functions import sts as mod


@pytest.fixture
def client(mocker):
    mocker.patch("botocore.session.Session")
    mocker.patch.object(mod, "has_status")

    return mocker.patch("boto3.client")()


@pytest.fixture
def lookup_account(mocker):
    m = mocker.patch("aws_test_functions.orgs.lookup_account")
    m.return_value = {
        "Id": "123",
        "Name": "Demo",
    }

    return m


def test_target_account_access(client, lookup_account):
    before = boto3.DEFAULT_SESSION
    target_account = mod.target_account_access("123", "cfn-tool")

    assert not client.assume_role.called

    with target_account:
        assert boto3.DEFAULT_SESSION != before

    assert client.assume_role.called
    assert boto3.DEFAULT_SESSION == before

    with target_account:
        assert boto3.DEFAULT_SESSION != before

    assert client.assume_role.call_count == 1
    assert boto3.DEFAULT_SESSION == before
    assert not lookup_account.called


def test_force_account_id(mocker, client, lookup_account):
    mod.get_target_account_session("Account Name", "Session")

    lookup_account.assert_called_once_with("Account Name", organizations=mocker.ANY)
    client.assume_role.assert_called_once_with(
        RoleArn="arn:aws:iam::123:role/rhedcloud/RHEDcloudMaintenanceOperatorRole",
        RoleSessionName=mocker.ANY,
    )
