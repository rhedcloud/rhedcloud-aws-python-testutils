import boto3
import uuid
from botocore.exceptions import ClientError

from aws_test_functions import deleteBucket

def test_answer():
    print("test_answer(): Start")
    #1 - create a bucket
    s3r = boto3.resource("s3")
    bucket = s3r.Bucket("testbucket"+str(uuid.uuid4()))
    bucket.create(ACL='public-read-write')
    bucket.wait_until_exists()
    #2 - verify that the bucket exists
    print(boto3.client('s3').list_objects(Bucket=bucket.name)) #side effect print, will raise ClientError if the bucket doesn't exist
                
    #3 - call deleteBucket
    deleteBucket(bucket.name)
        
    #4 - verify that the bucket doesn't exist
    try:
        print(boto3.client('s3').list_objects(Bucket=bucket.name)) #side effect print
        print('FAILED to delete bucket '+bucket.name)
        assert False
        return 'FAILED to delete bucket '+bucket.name
    except ClientError as e:
        if e.response['ResponseMetadata']['HTTPStatusCode'] == 404:
            assert True
            return 'SUCCESS: bucket deleted'
        else:
            print ("Unexpected error: %s" % e)
            raise e                    

def main():
    print("test_answer() results: ",test_answer())

if __name__ == "__main__":
    main()
