import pytest

from aws_test_functions import catch


def test_catch_and_return(capsys):
    @catch(ValueError, msg='Caught: {ex}', return_value=10)
    def my_function():
        raise ValueError('oops')

    assert my_function() == 10

    out, err = capsys.readouterr()
    assert 'Caught: oops' in out


def test_catch_and_raise(capsys):
    @catch(ValueError, msg='Caught and Raised: {ex}', return_value=11, reraise=True)
    def similar_function():
        raise ValueError('oops')

    with pytest.raises(ValueError) as ex:
        similar_function()

    assert 'ValueError: oops' in str(ex)

    out, err = capsys.readouterr()
    assert 'Caught and Raised: oops' in out


def test_uncaught(capsys):
    @catch(ValueError, msg='Uncaught: {ex}', return_value=12)
    def other_function():
        raise TypeError('oops')

    with pytest.raises(TypeError) as ex:
        other_function()

    assert 'TypeError: oops' in str(ex)

    out, err = capsys.readouterr()
    assert 'Uncaught: oops' not in out


def test_multiple_exceptions(capsys):
    @catch(ValueError, TypeError, msg='Multiple exception types: {ex}')
    def another_function():
        raise TypeError('oops')

    another_function()

    out, err = capsys.readouterr()
    assert 'Multiple exception types: oops' in out
